package dk.kvalitetsit.cdadocumentviewer.gui;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import dk.kvalitetsit.cdadocumentviewer.dto.DocumentMetadata;
import dk.kvalitetsit.cdadocumentviewer.dto.LoggableResult;
import dk.kvalitetsit.cdadocumentviewer.dto.MetadataInput;
import dk.kvalitetsit.cdadocumentviewer.dto.UploadFileRequest;
import dk.kvalitetsit.cdadocumentviewer.dto.UploadFileSession;
import dk.kvalitetsit.cdadocumentviewer.dto.UploadResponseInfo;
import dk.kvalitetsit.cdadocumentviewer.service.ErrorConverterUtil;
import dk.kvalitetsit.cdadocumentviewer.service.ReferenceDataService;
import dk.kvalitetsit.cdadocumentviewer.service.RepositoryProfileIti41Service;
import dk.kvalitetsit.cdadocumentviewer.service.UploadService;

@Controller
@SessionAttributes({"rememberedFileContent", "uploadFileSession"})
public class UploadController {

	private static Logger LOGGER = LoggerFactory.getLogger(UploadController.class);

	@Autowired
	UploadService uploadService;
	
	@Autowired
	RepositoryProfileIti41Service repositoryProfileIti41Service;
	
	@Autowired
	ReferenceDataService referenceDataService;
	
	@ModelAttribute("uploadFileSession")
	private  UploadFileSession uploadFileSession() {
	    return new UploadFileSession();
	}

	@RequestMapping("/upload")
	public String upload(Model model,
			@ModelAttribute("externalDocumentId") String externalDocumentId, 
			@ModelAttribute("entryUuid") String entryUuid,
			@ModelAttribute("repositoryId") String repositoryId, 
			@ModelAttribute("uploadFileSession") UploadFileSession uploadFileSession) {
		//Purpose: The user select document to upload and repository (repository is given if its an update)  
		
		List<String> errorList = uploadService.prepareUpload(externalDocumentId, entryUuid, repositoryId, uploadFileSession);
		if (errorList.size() > 0) {
			model.addAttribute("errorList", errorList);
			return returnUpload(model, uploadFileSession);
		}
		
		if (uploadFileSession.isReplaceOperationActive()) {
			model.addAttribute("selectedRepositoryName", uploadFileSession.getReplaceRepository().getName());
			model.addAttribute("uploadOperation",  "Ret af " + uploadFileSession.getReplaceExternalDocumentId());
		} else {
			model.addAttribute("repositoryList", repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues());
			model.addAttribute("uploadOperation",  "Opret");
		}
		
		model.addAttribute("request",  new UploadFileRequest());
		return "upload";  
	}
	
	@RequestMapping(value = "/showUpload", method = RequestMethod.POST)
	public String showUpload(Model model, @ModelAttribute("uploadRequest") UploadFileRequest request,
			@ModelAttribute("uploadFileSession") UploadFileSession uploadFileSession) {
		//Purpose: The user can look at the metadata and document. And can select optional metadata
		try {
			
			List<String> errorList = uploadService.validateShowUpload(request, uploadFileSession);
			if (errorList.size() > 0) {
				model.addAttribute("errorList", errorList);
				return returnUpload(model, uploadFileSession);
			}
			
			DocumentMetadata documentMetadata = uploadService.getMetaDataFromUploadFile(request);
			byte[] fileContent = uploadService.getBytesFromUploadFile(request);
			
			addTooltipToModel(model);

			model.addAttribute("metadataInput",  new MetadataInput());
			model.addAttribute("selectedNA", "N/A");
			model.addAttribute("valueRepository", uploadFileSession.getRepository().getName());
			model.addAttribute("valueHomecommunityid", uploadFileSession.getRepository().getHomeCommunityId());

			//add all dropwdownlist with values
			model.addAttribute("availabilityStatusList", referenceDataService.getAvailabilityStatusList());
			model.addAttribute("healthcareFacilityTypeCodeList", referenceDataService.getHealthcareFacilityTypeCodeList(null));
			model.addAttribute("objectTypeList", referenceDataService.getObjectTypeList());
			model.addAttribute("practiceSettingCodeList", referenceDataService.getPracticeSettingCodeList(null));
			if (documentMetadata.getFormatCode() == null) { //if not included, allow the user to select one
				model.addAttribute("formatCodeList", referenceDataService.getFormatCodeList(null));
			}

			if (uploadFileSession.isReplaceOperationActive()) {
				model.addAttribute("uploadOperation",  "Ret af " + uploadFileSession.getReplaceExternalDocumentId());
			} else {
				model.addAttribute("uploadOperation",  "Opret");
			}
			model.addAttribute("fileContent", new String(fileContent));
			model.addAttribute("fileMetadata", documentMetadata);
			model.addAttribute("rememberedFileContent", new String(fileContent));

		} catch (Exception e) {
			LOGGER.error("Template upload failed",e);
			List<String> errorList = Arrays.asList("Fejl ved håndtering af dokument eller dets metadata");
			model.addAttribute("errorList", errorList);
			return returnUpload(model, uploadFileSession);
		}

		return "showUploadContent";   	
	}
	
	private String returnUpload(Model model, UploadFileSession uploadFileSession) {
		if (uploadFileSession.isReplaceOperationActive()) {
			return upload(model, uploadFileSession.getReplaceExternalDocumentId(), uploadFileSession.getReplaceEntryUuid(), 
					uploadFileSession.getReplaceRepository().getRepositoryuniqueid(), uploadFileSession);
		} else {
			return upload(model, null, null, null, uploadFileSession );
		}
	}
	

	@RequestMapping(value = "/uploadDocument", method = RequestMethod.POST)
	public String uploadDocument(Model model, @ModelAttribute("metadataInput") MetadataInput request, 
			@ModelAttribute("rememberedFileContent") String fileContent, 
			@ModelAttribute("uploadFileSession") UploadFileSession uploadFileSession) {
		//Purpose: The document is uploaded
		
		try {
			LoggableResult<UploadResponseInfo> result = uploadService.doUpload(fileContent, request, uploadFileSession);
			if (result != null) {
				UploadResponseInfo uploadResponseInfo = result.getResult();
				if (uploadResponseInfo.allOk()) {
					model.addAttribute("notification", "Dokument uploadet (" + uploadResponseInfo.getExternalDocumentId() + ")");
					model.addAttribute("uploadResponseInfo", uploadResponseInfo);
					if (!uploadResponseInfo.getErrorList().isEmpty()) {
						model.addAttribute("errorList", ErrorConverterUtil.addFirstError(uploadResponseInfo.getErrorList(), "Advarsler ved upload af dokument. Advarslerne er:"));
					}
				} else {
					model.addAttribute("errorList", ErrorConverterUtil.addFirstError(uploadResponseInfo.getErrorList(), "Fejl ved upload af dokument. Fejlene er:"));
				}
				addRequestResponseModel(model, result.getRequestId(), result.getResponseId());
			}

		} catch (Exception e) {
			LOGGER.error("Template upload failed",e);
			model.addAttribute("errorList", ErrorConverterUtil.addFirstError(Arrays.asList(e.getMessage()), "Fejl ved upload af dokument. Fejlen er:"));
		}

		return "showUploadResult";   	
	}
	
	private void addTooltipToModel(Model model) {
		String tooltipPrefix = "Der søges på kodeværdien indenfor codeScheme: ";
		model.addAttribute("tooltipClassCode", tooltipPrefix+referenceDataService.getClassCodeScheme());
		model.addAttribute("tooltipFormatCode", tooltipPrefix+referenceDataService.getFormatCodeScheme());
		model.addAttribute("tooltipHealthcareFacilityTypeCode", tooltipPrefix+referenceDataService.getHealthcareFacilityTypeCodeScheme());
		model.addAttribute("tooltipPracticeSettingCode", tooltipPrefix+referenceDataService.getPracticeSettingCodeScheme());
	}
	
	private void addRequestResponseModel(Model model, String requestId, String responseId) {
		model.addAttribute("requestId", requestId);
		model.addAttribute("responseId", responseId);
	}

}
