package dk.kvalitetsit.cdadocumentviewer.gui;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.activation.DataHandler;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import dk.kvalitetsit.cdadocumentviewer.dto.*;
import dk.kvalitetsit.cdadocumentviewer.service.*;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.responses.QueryResponse;
import org.openehealth.ipf.commons.ihe.xds.core.responses.RetrievedDocumentSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonProcessingException;

import dk.kvalitetsit.cdadocumentviewer.log.LogEntry;
import dk.kvalitetsit.cdadocumentviewer.log.LogEntryService;
import dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata.DocumentEntryConverter;
import dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata.DocumentMetadata;


@Controller
@SessionAttributes({"searchResult"})
public class ViewController {

	private static Logger LOGGER = LoggerFactory.getLogger(ViewController.class);
	
	@Autowired
	CdaDocumentService documentService;	
	
	@Autowired
	SearchProfileService searchProfileService;

	@Autowired
	LogEntryService logEntryService;
	
	private String stsUrl = "Testbibliotekets STS: http://test1.ekstern-test.nspop.dk:8080/sts/services/NewSecurityTokenService";

	@Autowired
	ReferenceDataService referenceDataService;
	
	@Autowired
	RepositoryProfileIti41Service repositoryProfileIti41Service;

	@Autowired
	RegistryProfileIti18Service registryProfileIti18Service;
	
	private DocumentEntryConverter documentEntryConverter = new DocumentEntryConverter();
	
	public ViewController() {

	}

	@RequestMapping("/")
	public String frontPage(Model model) {
		SearchRequest request = new SearchRequest();
		model.addAttribute("searchResult", new ArrayList<>());
		model.addAttribute("searchRequest",  new SearchRequest());
		String backendCode = null;
		if (registryProfileIti18Service.getRegistryAsDropDownListForSearch().getDropDownListValues().size() > 0) {
			backendCode = registryProfileIti18Service.getRegistryAsDropDownListForSearch().getDropDownListValues().get(0).getCode();
		}
		addSearchPageModel(model, backendCode, true, request);
		return "search";
	}

	private void addSearchPageModel(Model model, String backendCode, boolean useDefaultForDocType, SearchRequest request) {
		model.addAttribute("tooltipUniqueId", "Unique ID er et filter, som udføres efter selve søgningen. Det indgår derfor ikke i requestet");
		String tooltipPrefix = "Der søges på kodeværdien indenfor codeScheme: ";
		model.addAttribute("tooltipFormatcode", tooltipPrefix+referenceDataService.getFormatCodeScheme());
		model.addAttribute("tooltipHealthcarefacilitytypecode", tooltipPrefix+referenceDataService.getHealthcareFacilityTypeCodeScheme());
		model.addAttribute("tooltipPracticesettingtypeCodeScheme", tooltipPrefix+referenceDataService.getPracticeSettingCodeScheme());
		
		model.addAttribute("tooltipDocumentType", "Hvis hverken stable eller on-demand vælges, søges der default på stable");

		// for at undgå at nulstille dokument type tilbage til default i forbindelse med søgning, håndteres dette her.
		if (useDefaultForDocType) {
			model.addAttribute("typeStableDefaultValue", registryProfileIti18Service.getRegistryFromBackend(backendCode).isDefaultTypeStable());
			model.addAttribute("typeOnDemandDefaultValue", registryProfileIti18Service.getRegistryFromBackend(backendCode).isDefaultTypeOnDemand());
		} else {
			model.addAttribute("typeStableDefaultValue", request.isTypeStable());
			model.addAttribute("typeOnDemandDefaultValue", request.isTypeOnDemand());
		}

		model.addAttribute("searchRegistries", registryProfileIti18Service.getRegistryAsDropDownListForSearch().getDropDownListValues());
		model.addAttribute("searchProfiles", searchProfileService.getSearchProfiles());
		
		//add all dropwdownlist with values			
		model.addAttribute("formatCodeList", referenceDataService.getFormatCodeList(backendCode));
		model.addAttribute("healthcareFacilityTypeCodeList", referenceDataService.getHealthcareFacilityTypeCodeList(backendCode));
		model.addAttribute("practiceSettingCodeList", referenceDataService.getPracticeSettingCodeList(backendCode));
		model.addAttribute("eventCodeSchemeList", referenceDataService.getEventCodeSchemeList(backendCode));
		model.addAttribute("typeCodeList", referenceDataService.getTypeCodeList(backendCode));

		String scheme = null;
		if (request != null && request.getTypeCode() != null && !request.getTypeCode().isEmpty()) {
			scheme = referenceDataService.getRelatedTypeCodeScheme(backendCode, request.getTypeCode());
		}
		model.addAttribute("typeCodeScheme", (scheme != null && !scheme.isEmpty()) ? scheme : "N/A" ); //empty og null ignoreres af jsp siden


	}
	
	private void addRequestResponseModel(Model model, String requestId, String responseId) {
		model.addAttribute("requestId", requestId);
		model.addAttribute("responseId", responseId);
	}


	@RequestMapping("/newsearch")
	public String newSearch(Model model) { 
		return frontPage(model);
	}

	@RequestMapping("/about")
	public String about(Model model) {    

		model.addAttribute("stsendpoint", stsUrl);
		
		List<RepositoryProfileIti41Dto> repoList = repositoryProfileIti41Service.getRepositoryProfileList();
		model.addAttribute("repoList", repoList);

		List<RegistryProfileIti18Dto> backendList = registryProfileIti18Service.getRegistryProfileList();
		model.addAttribute("backendList", backendList);

		return "about";  
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public String search(Model model, @ModelAttribute("searchRequest") SearchRequest request, @RequestParam(value = "submitAction", required = false) String submitAction) {

		searchProfileService.setSearchProfileFromId(request.getProfile());
		if ("filter".equals(submitAction)) {
			addSearchPageModel(model, request.getBackendCode(), true, request);
			return "search";
		}
		if ("chgDropDown".equals(submitAction)) {
			addSearchPageModel(model, request.getBackendCode(), false, request);
			return "search";
		}


		LoggableResult<QueryResponse> result = null;

		List<String> errorMessagesList = documentService.validateSearchRequest(request);//also list to pick up all errors in the flow before finally adding them to model at the end

		try {
			result = documentService.findDocuments(request);
		} catch (SOAPFaultException e) {
			errorMessagesList.add(e.getMessage());
		}
		
		if (request.getPatientID() == null || request.getPatientID().isEmpty()) {
			errorMessagesList.add("Patient ID skal indtastes ");
		}
		
		if ((request.getEventCode() != null && !request.getEventCode().isEmpty()) && (request.getEventCodeScheme() == null || request.getEventCodeScheme().isEmpty() ) ) {
			errorMessagesList.add("Når eventcode anvendes skal både code og scheme indtastes. Kriteriet er udeladt i søgningen");
		}

		if (result != null) {
			QueryResponse queryResponse = result.getResult();
			if (queryResponse.getErrors() != null && queryResponse.getErrors().size() > 0) {
				errorMessagesList.addAll(ErrorConverterUtil.convertErrors(queryResponse.getErrors()));
			}

			model.addAttribute("searchResult", convert(queryResponse.getDocumentEntries(), (request.getRegistryProfileIti18Dto() != null ? request.getRegistryProfileIti18Dto().getBackendCode() : null)));
			model.addAttribute("searchRequest", request);
			addRequestResponseModel(model, result.getRequestId(), result.getResponseId());

			if (queryResponse.getDocumentEntries() == null || queryResponse.getDocumentEntries().size() == 0) {
				if (request.filterUniqueID()) {
					model.addAttribute("notification", "Der blev ikke fundet nogen dokumenter med det angivne uniqueId. Eventuelt øvrige dokumenter er filtreret væk.");
				} else {
					model.addAttribute("notification", "Der blev ikke fundet nogen dokumenter ");
				}
			}
			if (!errorMessagesList.isEmpty()) {
				if (request.filterUniqueID()) {
					model.addAttribute("searchErrorsTop", "Der er advarsler og/eller fejl i svaret. Se nedenfor.");
				} else {
					model.addAttribute("searchErrorsTop", ErrorConverterUtil.convertErrorsString(errorMessagesList));
				}
				model.addAttribute("searchErrorsBottom", ErrorConverterUtil.convertErrorsString(errorMessagesList));
			}

		}

		addSearchPageModel(model, request.getBackendCode(), false, request);

		return "search";   	
	}

	@RequestMapping(value = "/showDocument", method = RequestMethod.POST)
	public String showDocument(Model model, 
			@ModelAttribute("externalDocumentId") String externalDocumentId, 
			@ModelAttribute("patientId") String patientId, 
			@ModelAttribute("repositoryId") String repositoryId,
			@ModelAttribute("backendCode") String backendCode,
			@RequestParam(value="homecommunityid", required=false) String homecommunityid,
			@ModelAttribute("encodedMetadata") String encodedMetadata) throws XdsException {
		LOGGER.info("Henter eksternt dokument med id: " + externalDocumentId);    	    	


		DocumentMetadata documentMetadata = documentEntryConverter.getShowDocumentMetadataFromJsonBase64Encoded(encodedMetadata);
		if (documentMetadata != null) {
			model.addAttribute("docEntry", documentMetadata);
		}		

		LoggableResult<RetrievedDocumentSet> loggableDocumentResponse = documentService.fetchDocument(externalDocumentId, patientId, repositoryId, homecommunityid, true, backendCode);
		RetrievedDocumentSet documentResponse = loggableDocumentResponse.getResult();
		model.addAttribute("documentFetchRequestId", loggableDocumentResponse.getRequestId());
		model.addAttribute("documentFetchResponseId", loggableDocumentResponse.getResponseId());
		
		List<String> documentErrors = null;
		if (documentResponse != null && documentResponse.getDocuments() != null && documentResponse.getDocuments().size() > 0) {
			try {
				model.addAttribute("document", formatXML(documentResponse.getDocuments().get(0).getDataHandler()));
			} catch (XPathExpressionException | TransformerException | SAXException | IOException | ParserConfigurationException e) {
				documentErrors = new LinkedList<>();
				documentErrors.add("Fejl under formattering af XML dokument: "+e.getMessage());
			}	 
		}
		if (documentErrors != null || (documentResponse != null && documentResponse.getErrors() != null && documentResponse.getErrors().size() > 0)) {
			List<String> errors = ErrorConverterUtil.convertErrors(documentResponse.getErrors());
			if (documentErrors != null) {
				errors.addAll(documentErrors);
			}
			model.addAttribute("documentFetchErrors", errors);
		}
		model.addAttribute("externalDocumentId", externalDocumentId);
		model.addAttribute("patientId", patientId);
		model.addAttribute("repositoryId", repositoryId);
		model.addAttribute("backendCode", backendCode);
		if (homecommunityid != null) {
			model.addAttribute("homecommunityid", homecommunityid);
		}
		
		model.addAttribute("lastpage","showDocument");    	
		return "showDocument";
	}

	@RequestMapping("/getLogEntry")
	public ResponseEntity<?> getLogEntry(@RequestParam("logEntryId") String logEntryId) {
		HttpHeaders headers = new HttpHeaders();
		String filename = "Log-"+logEntryId+".txt";
		headers.setContentDispositionFormData(filename, filename);
		LogEntry logContent = logEntryService.getLogFromId(logEntryId);
		ResponseEntity<String> response = new ResponseEntity<String>((logContent == null ? "" : logContent.getPayload()), headers, HttpStatus.OK);
		return response;
	}

	@RequestMapping("/getDocument")
	public ResponseEntity<?>  getDocument(Model model,
										  @RequestParam("externalDocumentId") String externalDocumentId,
										  @RequestParam("patientId") String patientId,
										  @RequestParam("repositoryUniqueId") String repositoryUniqueId,
										  @RequestParam("backendCode") String backendCode,
										  @RequestParam(value="homecommunityid", required=false) String homecommunityid) throws XdsException, XPathExpressionException, TransformerException, SAXException, IOException, ParserConfigurationException {
		return getDocumentFronRepository(model, externalDocumentId, patientId, repositoryUniqueId, homecommunityid, true, backendCode);
	}
	
	@RequestMapping("/getDocumentRaw")
	public ResponseEntity<?>  getDocumentRaw(Model model,
											 @RequestParam("externalDocumentId") String externalDocumentId,
											 @RequestParam("patientId") String patientId,
											 @RequestParam("repositoryUniqueId") String repositoryUniqueId,
											 @RequestParam("backendCode") String backendCode,
											 @RequestParam(value="homecommunityid", required=false) String homecommunityid) throws XdsException, XPathExpressionException, TransformerException, SAXException, IOException, ParserConfigurationException {
		return getDocumentFronRepository(model, externalDocumentId, patientId, repositoryUniqueId, homecommunityid, false, backendCode);
	}
	
	
	public ResponseEntity<?>  getDocumentFronRepository(Model model, String externalDocumentId, String patientId, String repositoryUniqueId, String homecommunityid, boolean transform, String backendCode) throws XdsException, XPathExpressionException, TransformerException, SAXException, IOException, ParserConfigurationException {
		LoggableResult<RetrievedDocumentSet> loggableDocumentResponse = documentService.fetchDocument(externalDocumentId,patientId, repositoryUniqueId, homecommunityid, true, backendCode);
		RetrievedDocumentSet documentResponse = loggableDocumentResponse.getResult();
		HttpHeaders headers = new HttpHeaders();
		
		if ((documentResponse != null && documentResponse.getErrors() != null && documentResponse.getErrors().size() > 0)) {
			ResponseEntity<List<String>> response = new ResponseEntity<List<String>>(ErrorConverterUtil.convertErrors(documentResponse.getErrors()), headers, HttpStatus.INTERNAL_SERVER_ERROR);
			return response;
		}
		
		String filename = "Document-"+externalDocumentId+".xml";
		headers.setContentDispositionFormData(filename, filename);
		
		ResponseEntity<String> response;
		if (transform) {
			response = new ResponseEntity<String>(formatXML(documentResponse.getDocuments().get(0).getDataHandler()), headers, HttpStatus.OK);
		} else {
			response = new ResponseEntity<String>(rawXml(documentResponse.getDocuments().get(0).getDataHandler()), headers, HttpStatus.OK);
		}
		
		return response;
	}

	private String formatXML(DataHandler dataHandler) throws TransformerException, SAXException, IOException, ParserConfigurationException, XPathExpressionException {   
			Document xmlDocument = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder()
					.parse(new InputSource(dataHandler.getInputStream()));

			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",xmlDocument,XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); ++i) {
				Node node = nodeList.item(i);
				node.getParentNode().removeChild(node);
			}

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			StringWriter stringWriter = new StringWriter();
			StreamResult streamResult = new StreamResult(stringWriter);

			transformer.transform(new DOMSource(xmlDocument), streamResult);
			return stringWriter.toString();

	}
	private String rawXml(DataHandler dataHandler) throws IOException {
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		dataHandler.writeTo(outputStream);
        byte[] documentDataByte = outputStream.toByteArray();
        String documentDataString = new String(documentDataByte);
        return documentDataString;

	}

	private List<DocumentEntryTO> convert(List<DocumentEntry> list, String backendCode) {
		List<DocumentEntryTO> rv = new ArrayList<>(); 
		if (list != null) {
			for (DocumentEntry de : list) {
				boolean allowUpdate =  repositoryProfileIti41Service.isUpdateAllowedForRepositoryUniqueId(de.getRepositoryUniqueId());
				boolean allowDeprecate =  repositoryProfileIti41Service.isDeprecateAllowedForRepositoryUniqueId(de.getRepositoryUniqueId());
				
				String docAsJson;
				try {
					docAsJson = documentEntryConverter.getJsonBase64EncodedFromDocumentEntry(de);
				} catch (JsonProcessingException e) {
					LOGGER.error("Fejl ved model til json konvertering", e);
					docAsJson = null; //metadata tabellen udfyldes ikke på siden
				}
				
				rv.add(new DocumentEntryTO(de, allowUpdate, allowDeprecate, docAsJson, backendCode)); //allowed might be overruled by doc status here
			}
		}
		return rv;
	}

}
