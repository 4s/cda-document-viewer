package dk.kvalitetsit.cdadocumentviewer.gui;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import dk.kvalitetsit.cdadocumentviewer.dto.DeprecateResponseInfo;
import dk.kvalitetsit.cdadocumentviewer.dto.LoggableResult;
import dk.kvalitetsit.cdadocumentviewer.service.DeprecateService;
import dk.kvalitetsit.cdadocumentviewer.service.ErrorConverterUtil;

@Controller
public class DeprecateController {

	private static Logger LOGGER = LoggerFactory.getLogger(DeprecateController.class);

	@Autowired
	DeprecateService deprecateService;

	@RequestMapping("/deprecate")
	public String deprecate(Model model, 
			@ModelAttribute("externalDocumentId") String externalDocumentId, 
			@ModelAttribute("patientId") String patientId, 
			@ModelAttribute("repositoryId") String repositoryId) {

		try {
			LoggableResult<DeprecateResponseInfo> result = deprecateService.doDeprecate(externalDocumentId, repositoryId, patientId);
			if (result != null) {
				DeprecateResponseInfo deprecateResponseInfo = result.getResult();
				if (deprecateResponseInfo.allOk()) {
					model.addAttribute("notification", "Dokumentet er deprecated (" + externalDocumentId + ")");
				} else {
					model.addAttribute("deprecateErrors", deprecateResponseInfo.getErrorList());
				}
				addRequestResponseModel(model, result.getRequestId(), result.getResponseId());
			}
		} catch (Exception e) {
			LOGGER.error("Deprecated failed",e);
			List<String> errorList = new ArrayList<String>();
			errorList.add("Fejl ved deprecate af dokument");
			errorList.add("Backend fejl: " + e.getMessage());
			model.addAttribute("deprecateErrors", ErrorConverterUtil.convertErrorsString(errorList));
		}
		return "showDeprecateResult";  
	}
	
	private void addRequestResponseModel(Model model, String requestId, String responseId) {
		model.addAttribute("requestId", requestId);
		model.addAttribute("responseId", responseId);
	}

}
