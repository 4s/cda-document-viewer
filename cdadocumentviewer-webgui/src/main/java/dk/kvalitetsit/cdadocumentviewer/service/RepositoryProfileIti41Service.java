package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.configuration.RepositoryProfileIti41Configuration;
import dk.kvalitetsit.cdadocumentviewer.configuration.RepositoryProfileIti41Configuration.Repo;
import dk.kvalitetsit.cdadocumentviewer.dto.RepositoryProfileIti41Dto;

public class RepositoryProfileIti41Service {
	
	private static Logger LOGGER = LoggerFactory.getLogger(RepositoryProfileIti41Service.class);
	
	@Autowired
	private BeanFactory beanFactory;
	
	@Autowired
    private RepositoryProfileIti41Configuration repositoryProfileIti41Configuration;

	private List<RepositoryProfileIti41Dto> repositoryProfileList;
	private DropDownListService repositoryAsDropDownListForUpload;
	
	public RepositoryProfileIti41Service(BeanFactory beanFactory,  RepositoryProfileIti41Configuration repositoryProfileIti41Configuration) {
		this.beanFactory = beanFactory;
		this.repositoryProfileIti41Configuration = repositoryProfileIti41Configuration;
		init();
		
	}
	public RepositoryProfileIti41Service() {
			
	}
	
	@PostConstruct
	public void init() {
		repositoryProfileList = new ArrayList<RepositoryProfileIti41Dto>();
		StringBuilder endpoints = new StringBuilder();
		StringBuilder names = new StringBuilder();
		
		for (Repo repo: repositoryProfileIti41Configuration.getRepo()) {

			// bagudkompatibel: anvende de gamle properties hvis nye ikke er sat op
			if (repo.getBackendCode() == null || repo.getBackendCode().isEmpty()) {
				throw new RuntimeException("backend has not been configured for repository configuration");
			}

			RepositoryProfileIti41Dto repositoryProfileDto = new RepositoryProfileIti41Dto();
			repositoryProfileDto.setDgws(repo.isDgwsenabled());
			repositoryProfileDto.setDgwsIti57(repo.isDgwsenablediti57());
			repositoryProfileDto.setRepositoryuniqueid(repo.getRepositoryuniqueid());
			repositoryProfileDto.setHomeCommunityId (repo.getHomeCommunityId ());
			repositoryProfileDto.setUploadallowed(repo.isUploadallowed());
			repositoryProfileDto.setUpdateallowed(repo.isUpdateallowed());
			repositoryProfileDto.setDeprecateallowed(repo.isDeprecateallowed());
			repositoryProfileDto.setEndpoint(repo.getEndpoint());
			repositoryProfileDto.setEndpointIti57(repo.getEndpointiti57());
			repositoryProfileDto.setBackendCode(repo.getBackendCode());
			repositoryProfileDto.setName(repo.getName());
			Iti41PortType iti41PortType = beanFactory.getBean(Iti41PortType.class, repositoryProfileDto.getEndpoint(), repositoryProfileDto.isDgws());
			repositoryProfileDto.setIti41PortType(iti41PortType);
			if (repositoryProfileDto.getEndpointIti57() != null && !repositoryProfileDto.getEndpointIti57().isEmpty()) {
				Iti57PortType iti57PortType = beanFactory.getBean(Iti57PortType.class, repositoryProfileDto.getEndpointIti57(), repositoryProfileDto.isDgwsIti57());
				repositoryProfileDto.setIti57PortType(iti57PortType);				
			}

			for (String docType: repo.getValiddoctypes()) {
				if (!docType.isEmpty()) {
					repositoryProfileDto.addDocType(docType);
				}
			}
			
			repositoryProfileList.add(repositoryProfileDto);
			LOGGER.info("ITI41 Configuration loaded is: " + repositoryProfileDto.toString());
			
			if (repositoryProfileDto.isUploadallowed()) {
				if (endpoints.length() > 0) {
					endpoints.append(";");
					names.append(";");
				}
				endpoints.append(repositoryProfileDto.getEndpoint());
				names.append(repositoryProfileDto.getName());
			}
			
		}
		repositoryAsDropDownListForUpload = new DropDownListService(endpoints.toString(), names.toString(), null);
		
	}
	
	public List<RepositoryProfileIti41Dto> getRepositoryProfileList() {
		return repositoryProfileList;
	}
	
	public DropDownListService getRepositoryAsDropDownListForUpload() {
		return repositoryAsDropDownListForUpload;
	}

	public boolean isUpdateAllowedForRepositoryUniqueId(String repositoryUniqueId) {
		if (repositoryUniqueId == null) {
			return false;
		}
		for (RepositoryProfileIti41Dto repositoryProfileIti41Dto: repositoryProfileList) {
			if (repositoryProfileIti41Dto != null && repositoryProfileIti41Dto.getRepositoryuniqueid().equals(repositoryUniqueId) && repositoryProfileIti41Dto.isUpdateallowed()) {
				LOGGER.debug("Finding repository by id: " + repositoryUniqueId + " Found is: " + repositoryProfileIti41Dto.getName());
				LOGGER.debug("Repository has update allowed");
				return true;
			}
		}
		return false;

	}
	
	public boolean isDeprecateAllowedForRepositoryUniqueId(String repositoryUniqueId) {
		if (repositoryUniqueId == null) {
			return false;
		}
		for (RepositoryProfileIti41Dto repositoryProfileIti41Dto: repositoryProfileList) {
			if (repositoryProfileIti41Dto != null && repositoryProfileIti41Dto.getRepositoryuniqueid().equals(repositoryUniqueId) && repositoryProfileIti41Dto.isdeprecateallowed() &&
					repositoryProfileIti41Dto.getIti57PortType() != null) {
				LOGGER.debug("Finding repository by id: " + repositoryUniqueId + " Found is: " + repositoryProfileIti41Dto.getName());
				LOGGER.debug("Repository has deprecate allowed");
				return true;
			}
		}
		return false;
	}
	
	public RepositoryProfileIti41Dto getUpdateRepositoryFromUniqueId(String repositoryId) {
		for (RepositoryProfileIti41Dto repositoryProfileIti41Dto: repositoryProfileList) {
			if (repositoryProfileIti41Dto != null &&  repositoryProfileIti41Dto.getRepositoryuniqueid().equals(repositoryId) && repositoryProfileIti41Dto.isUpdateallowed()) {
				LOGGER.debug("Finding update repository by id: " + repositoryId + " Found is: " + repositoryProfileIti41Dto.getName());
				return repositoryProfileIti41Dto;
			}
		}
		return null;
	}

	public RepositoryProfileIti41Dto getUploadRepositoryFromEndpoint(String endpoint) {
		for (RepositoryProfileIti41Dto repositoryProfileIti41Dto: repositoryProfileList) {
			if (repositoryProfileIti41Dto != null && repositoryProfileIti41Dto.getEndpoint().equals(endpoint) && repositoryProfileIti41Dto.isUploadallowed()) {
				LOGGER.debug("Finding upload repository by id: " + endpoint + " Found is: " + repositoryProfileIti41Dto.getName());
					return repositoryProfileIti41Dto;
			}
		}
		return null;
	}
	
	public RepositoryProfileIti41Dto getDeprecateRepositoryFromUniqueId(String repositoryId) {
		for (RepositoryProfileIti41Dto repositoryProfileIti41Dto: repositoryProfileList) {
			if (repositoryProfileIti41Dto != null &&  repositoryProfileIti41Dto.getRepositoryuniqueid().equals(repositoryId) && repositoryProfileIti41Dto.isdeprecateallowed()) {
				LOGGER.debug("Finding deprecate repository by id: " + repositoryId + " Found is: " + repositoryProfileIti41Dto.getName());
				return repositoryProfileIti41Dto;
			}
		}
		return null;
	}

}
