package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.openehealth.ipf.commons.ihe.xds.core.responses.ErrorInfo;

public class ErrorConverterUtil {
	
	public static List<String> convertErrorsString(List<String> errors) {
		List<String> errorMessages = new LinkedList<>();

		if (errors != null) {
			for (String error : errors) {
				errorMessages.add(error);
			}
		}
		return errorMessages;
	}
	
	public static List<String> convertErrors(List<ErrorInfo> errors) {
		List<String> errorMessages = new LinkedList<>();

		if (errors != null) {
			for (ErrorInfo errorInfo : errors) {
				String errorMessage = (errorInfo.getSeverity() != null ? errorInfo.getSeverity().name()+" " : "") + (errorInfo.getErrorCode() != null ? errorInfo.getErrorCode().getOpcode() + ": " : "") + (errorInfo.getCustomErrorCode() != null ? errorInfo.getCustomErrorCode() + " " : "") + (errorInfo.getCodeContext() != null ? errorInfo.getCodeContext() : "");
				errorMessages.add(errorMessage);
			}
		}
		return errorMessages;
	}
	
	public static List<String> addFirstError(List<String> originalErrorList, String firstErrorText) {
		List<String> errorList = new ArrayList<String>();
		errorList.add(firstErrorText);
		errorList.addAll(originalErrorList);
		return errorList;
	}

}
