package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.dgws.AbstractSosiUser;
import dk.kvalitetsit.cdadocumentviewer.dgws.MocesUser;
import dk.kvalitetsit.cdadocumentviewer.dgws.MocesUserFactory;
import dk.kvalitetsit.cdadocumentviewer.dto.SearchProfileDto;

public class SearchProfileService {

	@Autowired
	SearchProfile searchProfile;
	
	@Autowired
	MocesUserFactory mocesUserFactory;
	
	Map<String, AbstractSosiUser> profileMap;
	
	@PostConstruct
	public void init() {
		profileMap = new LinkedHashMap<>();
		
		for(MocesUser mocesUser : mocesUserFactory.getAllMocesUsers()) {
			profileMap.put(mocesUser.getIdForList(), mocesUser);
		}
	}
	
	public void setSearchProfileFromId(String id) {
		AbstractSosiUser user = profileMap.get(id);
		if (user == null) {
			user = mocesUserFactory.getDefaultMocesUser();//defaultMocesUser;
		}		
		searchProfile.setCurrent(user);
	}
	
	public List<SearchProfileDto> getSearchProfiles() {
		List<SearchProfileDto> profiles = new LinkedList<>();
		for (String key : profileMap.keySet()) {
			profiles.add(new SearchProfileDto(key, key));
		}
		return profiles;
	}
	
	public SearchProfile getSearchProfile() {
		return searchProfile;
	}
}
