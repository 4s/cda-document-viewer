package dk.kvalitetsit.cdadocumentviewer.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import dk.kvalitetsit.cdadocumentviewer.dto.CdaMetadata;
import dk.kvalitetsit.cdadocumentviewer.dto.Code;
import dk.kvalitetsit.cdadocumentviewer.dto.CreateAndRegisterCdaRequest;
import dk.kvalitetsit.cdadocumentviewer.dto.DocumentMetadata;
import dk.kvalitetsit.cdadocumentviewer.dto.LoggableResult;
import dk.kvalitetsit.cdadocumentviewer.dto.MetadataInput;
import dk.kvalitetsit.cdadocumentviewer.dto.UploadResponseInfo;
import dk.kvalitetsit.cdadocumentviewer.dto.UploadFileRequest;
import dk.kvalitetsit.cdadocumentviewer.dto.UploadFileSession;

@Component
public class UploadService {
	
	@Autowired
	RepositoryProfileIti41Service repositoryProfileIti41Service;
	
	@Autowired
	CdaDocumentService documentService;
	
	@Autowired
	ReferenceDataService referenceDataService;

	@Autowired
	SearchProfileService searchProfileService;
	
	@Autowired
	SearchProfile searchProfile;

	
	public static final Logger LOGGER = LoggerFactory.getLogger(UploadService.class);
	
	public List<String> prepareUpload(String externalDocumentId, String entryUuid, String repositoryId, UploadFileSession uploadFileSession) {
		List<String> errorList = new ArrayList<String>();
		uploadFileSession.reset();
		if (externalDocumentId != null && !externalDocumentId.isEmpty() && entryUuid != null && !entryUuid.isEmpty() &&repositoryId != null && !repositoryId.isEmpty()) {
			if (repositoryProfileIti41Service.isUpdateAllowedForRepositoryUniqueId(repositoryId)) { //check just in case user edited url
				LOGGER.info("Update document is allowed. externalDocumentId: " + externalDocumentId + ". entryUuid: " + entryUuid + ". repositoryId: " + repositoryId);
				uploadFileSession.setDocumentToReplace(entryUuid, externalDocumentId, repositoryProfileIti41Service.getUpdateRepositoryFromUniqueId(repositoryId));
			} else {
				LOGGER.info("Update document is not allowed. externalDocumentId: " + externalDocumentId + ". entryUuid: " + entryUuid + ". repositoryId: " + repositoryId);
				errorList = Arrays.asList("Dokumentets repository er ikke konfigureret til ret. Du kan oprette dokumentet som et nyt istedet");

				return errorList;
			}
		} else {
			LOGGER.info("Create new document");
		}
		return errorList;
	}

	
	public List<String> validateShowUpload(UploadFileRequest request, UploadFileSession uploadFileSession)  throws XdsException, IOException{
		List<String> errorList = new ArrayList<String>();
		
		if (!uploadFileSession.isReplaceOperationActive() && (request == null || request.getRepositoryEndpoint() == null) ) {
			errorList = Arrays.asList("Der skal være valgt et repository");
			return errorList;
		}
		
		if (request.getCdaFile().getBytes().length == 0) {
			errorList = Arrays.asList("Der skal vælges en fil, som skal uploades.");
			return errorList;
		}
		
		if (uploadFileSession.isReplaceOperationActive()) {
			uploadFileSession.setRepository(uploadFileSession.getReplaceRepository());	
		} else {
			uploadFileSession.setRepository(repositoryProfileIti41Service.getUploadRepositoryFromEndpoint(request.getRepositoryEndpoint()));
		}

		//set input data from form
		DocumentMetadata metadata = getMetaDataFromUploadFile(request);

		// any validation that stops the process
		if (metadata.typeCode != null && metadata.typeCode.getCode() != null) {
			if (!uploadFileSession.getRepository().isDocTypeValid(metadata.typeCode.getCode())) {
				errorList = Arrays.asList("Dokumentets typeCode (" + metadata.typeCode.getCode() +
						") matcher ikke de lovlige værdier. Lovlige værdier er: " + uploadFileSession.getRepositoryValidDocTypesAsString());
				return errorList;
			}
		}
		return errorList;
	}

	
	public DocumentMetadata getMetaDataFromUploadFile(UploadFileRequest request)  throws XdsException, IOException{
		CreateAndRegisterCdaRequest req = new CreateAndRegisterCdaRequest();
		req.setDocument(Base64Utils.encodeToString(getBytesFromUploadFile(request)));

		DocumentMetadata metadata = documentService.getMetadata(req);
		LOGGER.info("metadata (DocumentMetadata): " + metadata.toString());

		return metadata;
	}
	
	public byte[] getBytesFromUploadFile(UploadFileRequest request)  throws XdsException, IOException{
		return request.getCdaFile().getBytes();
	}
	
	public LoggableResult doUpload(String fileContent, MetadataInput request, UploadFileSession uploadFileSession) throws XdsException {
		CreateAndRegisterCdaRequest registerRequest = new CreateAndRegisterCdaRequest();
		registerRequest.setDocument(Base64Utils.encodeToString(fileContent.getBytes()));

		CdaMetadata cdaMetadata = createMetadata(request, uploadFileSession);
		registerRequest.setDocumentMetadata(cdaMetadata);

		UploadResponseInfo uploadResponseInfo = documentService.createAndRegisterCda(registerRequest, uploadFileSession);
		if (uploadResponseInfo.allOk()) {
			LOGGER.info("Document registered externalId: " + uploadResponseInfo.getExternalDocumentId());
		}
		LoggableResult<UploadResponseInfo> lr = new LoggableResult<UploadResponseInfo>(uploadResponseInfo, searchProfile.getRequestId(), searchProfile.getResponseId());

		return lr;
	}
	
	private CdaMetadata createMetadata(MetadataInput request, UploadFileSession uploadFileSession) {
		CdaMetadata metadata = new CdaMetadata();
		metadata.setAvailabilityStatus(request.getAvailabilityStatus());
		
		metadata.setFormatCode(new Code(referenceDataService.getFormatCodeNameFromCode(request.getFormatCode()), request.getFormatCode(), referenceDataService.getFormatCodeScheme()));
		metadata.setHealthcareFacilityTypeCode(new Code(referenceDataService.getHealthcareFacilityTypeNameFromCode(request.getHealthcareFacilityTypeCode()), request.getHealthcareFacilityTypeCode(), referenceDataService.getHealthcareFacilityTypeCodeScheme()));
		metadata.setObjectType(request.getObjectType());
		metadata.setPracticeSettingCode(new Code(referenceDataService.getPracticeSettingNameFromCode(request.getPracticeSettingCode()), request.getPracticeSettingCode(), referenceDataService.getPracticeSettingCodeScheme()));

		metadata.setSubmissionTime(new Date());
		
		metadata.setHomeCommunityId(uploadFileSession.getRepository().getHomeCommunityId());
		LOGGER.info("metadata (CdaMetadata): " + metadata.toString());
		
		return metadata;
	}
	
}