package dk.kvalitetsit.cdadocumentviewer.service;

import java.text.Collator;
import java.util.*;

import dk.kvalitetsit.cdadocumentviewer.dto.DropDownListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DropDownListService {

	private static Logger LOGGER = LoggerFactory.getLogger(DropDownListService.class);

	List<DropDownListDto> dropDownList;
	

	public DropDownListService(String codes, String names, String schemes) {
		dropDownList = new LinkedList<>();
		if (codes == null || codes.isEmpty()) {
			return;
		}

		String[] codeParts;
		codeParts = codes.split(";");
		String[] nameParts;
		nameParts = names.split(";");
		String[] schemeParts;
		if (schemes != null) {
			schemeParts = schemes.split(";");
		} else {
			schemeParts = new String[codes.length()];
		}
		for (int i = 0;i<codeParts.length;i++) {
			dropDownList.add(new DropDownListDto(codeParts[i], nameParts[i], schemeParts[i]));
		}
		dropDownList.sort( new Comparator<DropDownListDto>(){
			@Override
			public int compare(DropDownListDto o1,DropDownListDto o2){
				return Collator.getInstance().compare(o1.getCode(),o2.getCode());
			}
		});
	}

	public List<DropDownListDto> getDropDownListValues() {
		LOGGER.debug("DropDownListValues er: " + dropDownList.toString());
		return dropDownList;
	}
	
	public String getNameFromCode(String code) {
		for (DropDownListDto elm : dropDownList) {
			if (elm.getCode().equals(code)) {
				return elm.getName();
			}
		}
		return null;
	}

}
