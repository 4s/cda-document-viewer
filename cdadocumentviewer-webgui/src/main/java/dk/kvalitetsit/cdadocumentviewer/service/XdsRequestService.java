package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.LinkedList;
import java.util.List;

import dk.kvalitetsit.cdadocumentviewer.dto.*;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.EbXMLFactory;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLFactory30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLQueryResponse30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLRegistryResponse30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLRetrieveDocumentSetResponse30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.RetrieveDocumentSetResponseType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.responses.ErrorInfo;
import org.openehealth.ipf.commons.ihe.xds.core.responses.QueryResponse;
import org.openehealth.ipf.commons.ihe.xds.core.responses.Response;
import org.openehealth.ipf.commons.ihe.xds.core.responses.RetrievedDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.responses.Status;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.lcm.SubmitObjectsRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.query.AdhocQueryRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.query.AdhocQueryResponse;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryError;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryResponseType;
import org.openehealth.ipf.commons.ihe.xds.core.transform.responses.QueryResponseTransformer;
import org.openehealth.ipf.commons.ihe.xds.core.transform.responses.ResponseTransformer;
import org.openehealth.ipf.commons.ihe.xds.core.transform.responses.RetrieveDocumentSetResponseTransformer;
import org.openehealth.ipf.commons.ihe.xds.iti18.Iti18PortType;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti43.Iti43PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.kvalitetsit.cdadocumentviewer.xds.ProvideAndRegisterDocumentSetRequest;

@Component
public class XdsRequestService {

	private static final EbXMLFactory ebXMLFactory = new EbXMLFactory30();
	
	@Autowired
	XdsRequestBuilderService xdsRequestBuilderService;

	@Autowired
	CdaMetaDataFactory cdaMetaDataFactory;
	
	@Autowired
	RepositoryProfileIti41Service repositoryProfileIti41Service;
	
	protected EbXMLFactory getEbXmlFactory() {
		return ebXMLFactory;
	}

	public UploadResponseInfo createAndRegisterCda(String document, CdaMetadata cdaMetadata, UploadFileSession uploadFileSession) throws XdsException {
		DocumentMetadata documentMetadata = cdaMetaDataFactory.createFromCdaRegistrationRequest(cdaMetadata, document);
		return createAndRegisterDocument(document, documentMetadata, uploadFileSession);
	}

	public UploadResponseInfo createAndRegisterDocument(String document, DocumentMetadata documentMetadata, UploadFileSession uploadFileSession) throws XdsException {
		Iti41PortType iti41PortType = uploadFileSession.getRepository().getIti41PortType();
		if (iti41PortType == null) { //this should not happen
			XdsException e = new XdsException();
			e.addError("No repository has been selected");
			throw e;
		}
		
		ProvideAndRegisterDocumentSetRequest provideAndRegisterDocumentSetRequest = 
				xdsRequestBuilderService.buildProvideAndRegisterDocumentSetRequest(document, documentMetadata,  uploadFileSession.getReplaceEntryUuid());
		RegistryResponseType registryResponse = iti41PortType.documentRepositoryProvideAndRegisterDocumentSetB(provideAndRegisterDocumentSetRequest.getProvideAndRegisterDocumentSetRequestType());
		
		ResponseTransformer responseTransformer = new ResponseTransformer(getEbXmlFactory());
		EbXMLRegistryResponse30 ebxml = new EbXMLRegistryResponse30(registryResponse);
		Response response = responseTransformer.fromEbXML(ebxml);
		
		UploadResponseInfo uploadResponseInfo = new UploadResponseInfo(documentMetadata.getPatientId().getCode(), response.getStatus());
		if (response.getStatus() == Status.SUCCESS) {
			uploadResponseInfo.setExternalDocumentId(provideAndRegisterDocumentSetRequest.getExternalDocumentId());
		}
		if (response.getErrors() != null) {
			for (ErrorInfo registryError :response.getErrors()) {
				uploadResponseInfo.addError(registryError.getCodeContext());
			}
		}
		uploadResponseInfo.setBackendCodeToSearchDocument(uploadFileSession.getRepository().getBackendCode());
		return uploadResponseInfo;
	}
	
	public RetrievedDocumentSet fetchDocument(String documentId, String repositoryId, String homecommunityid, RegistryProfileIti18Dto registryProfileIti18Dto) {

		if (registryProfileIti18Dto == null || registryProfileIti18Dto.getIti18PortType() == null) {
			return new RetrievedDocumentSet();  //don't call when no registry selected. Return empty result set
		}

		Iti43PortType iti43PortType = registryProfileIti18Dto.getIti43PortType();

		RetrieveDocumentSetResponseType repositoryResponse= iti43PortType.documentRepositoryRetrieveDocumentSet(xdsRequestBuilderService.buildRetrieveDocumentSetRequestType(documentId, repositoryId, homecommunityid));
		
		RetrieveDocumentSetResponseTransformer retrieveDocumentSetResponseTransformer = new RetrieveDocumentSetResponseTransformer(getEbXmlFactory());
		EbXMLRetrieveDocumentSetResponse30 ebXmlResponse = new EbXMLRetrieveDocumentSetResponse30(repositoryResponse);
		RetrievedDocumentSet rds = retrieveDocumentSetResponseTransformer.fromEbXML(ebXmlResponse);
		return rds;
	}	
	
	public QueryResponse searchForDocuments(SearchRequest searchRequest) {
		if (searchRequest.getRegistryProfileIti18Dto() == null || searchRequest.getRegistryProfileIti18Dto().getIti18PortType() == null) {
			return new QueryResponse();  //don't call when no registry selected. Return empty result set
		}

		if (searchRequest.getPatientID() == null || searchRequest.getPatientID().isEmpty()) {
			return new QueryResponse();  //don't call when no patient id. Return empty result set
		}

		Iti18PortType iti18PortType = searchRequest.getRegistryProfileIti18Dto().getIti18PortType();
		
		AdhocQueryRequest adhocQueryRequest = xdsRequestBuilderService.buildAdhocQueryRequest(searchRequest);

		AdhocQueryResponse adhocQueryResponse = iti18PortType.documentRegistryRegistryStoredQuery(adhocQueryRequest);
		QueryResponseTransformer queryResponseTransformer = new QueryResponseTransformer(getEbXmlFactory());
		EbXMLQueryResponse30 ebXmlresponse = new EbXMLQueryResponse30(adhocQueryResponse);
		QueryResponse queryResponse = queryResponseTransformer.fromEbXML(ebXmlresponse);

		if (queryResponse != null && queryResponse.getDocumentEntries() != null && queryResponse.getDocumentEntries().size() != 0) {
			if (searchRequest.filterUniqueID()) {
				//i det tilfælde hvor brugeren har indtastet uniqueID skal alt andet end den filtreres ud
				//Så derfor skal vi lave noget klient-side filtrering på dette
				List<DocumentEntry> matchingDocumentEntries = new LinkedList<>();
				for (DocumentEntry documentEntry : queryResponse.getDocumentEntries()) {
					if (searchRequest.getUniqueID().equals(documentEntry.getUniqueId())) {
						matchingDocumentEntries.add(documentEntry);
					}
				}
				queryResponse.setDocumentEntries(matchingDocumentEntries);
				
			}
		}
		return queryResponse;
	}
	
	public QueryResponse getDocumentEntry(String documentId, RegistryProfileIti18Dto registryProfileIti18Dto) throws XdsException {

		Iti18PortType iti18PortType = (registryProfileIti18Dto != null) ? registryProfileIti18Dto.getIti18PortType() : null;

		if (iti18PortType == null) { //this should not happen
			XdsException e = new XdsException();
			e.addError("No registry has been selected for lookup before deprecate");
			throw e;
		}

		AdhocQueryRequest adhocQueryRequest = xdsRequestBuilderService.buildAdhocQueryRequest(documentId);
		AdhocQueryResponse adhocQueryResponse = iti18PortType.documentRegistryRegistryStoredQuery(adhocQueryRequest);
		QueryResponseTransformer queryResponseTransformer = new QueryResponseTransformer(getEbXmlFactory());
		EbXMLQueryResponse30 ebXmlresponse = new EbXMLQueryResponse30(adhocQueryResponse);
		QueryResponse queryResponse = queryResponseTransformer.fromEbXML(ebXmlresponse);
		return queryResponse;
	}
	
	public void deprecateDocument(DocumentEntry toBeDeprecated, RepositoryProfileIti41Dto repositoryProfileIti41Dto) throws XdsException {
		
		Iti57PortType iti57PortType = (repositoryProfileIti41Dto != null) ? repositoryProfileIti41Dto.getIti57PortType() : null;
		if (iti57PortType == null) { //this should not happen
			XdsException e = new XdsException();
			e.addError("No repository has been selected for deprecate");
			throw e;
		}
		
		SubmitObjectsRequest body = xdsRequestBuilderService.buildDeprecateSubmitObjectsRequest(toBeDeprecated);
		RegistryResponseType registryResponse = iti57PortType.documentRegistryUpdateDocumentSet(body);
		
		if (registryResponse.getRegistryErrorList() == null || registryResponse.getRegistryErrorList().getRegistryError() == null || registryResponse.getRegistryErrorList().getRegistryError().isEmpty()) {
			//OK !
		} else {
			XdsException e = new XdsException();
			for (RegistryError registryError :registryResponse.getRegistryErrorList().getRegistryError()) {
				e.addError(registryError.getCodeContext());
			}
			throw e;
		}
	}
	
}
