package dk.kvalitetsit.cdadocumentviewer.service;

import dk.kvalitetsit.cdadocumentviewer.dto.*;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.responses.QueryResponse;
import org.openehealth.ipf.commons.ihe.xds.core.responses.RetrievedDocumentSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import dk.kvalitetsit.cdadocumentviewer.dgws.UserContextForQuery;

import java.util.LinkedList;
import java.util.List;

@Component
public class CdaDocumentService  {

	@Autowired
	XdsRequestService xdsRequestService;

	@Autowired
	CdaMetaDataFactory cdaMetaDataFactory;

	@Autowired
	SearchProfile searchProfile;

	@Autowired
	RegistryProfileIti18Service registryProfileIti18Service;
	
	public UploadResponseInfo createAndRegisterCda(CreateAndRegisterCdaRequest createAndRegister, UploadFileSession uploadFileSession) throws XdsException {
		String document = new String(Base64Utils.decode(createAndRegister.getDocument().getBytes()));
		return xdsRequestService.createAndRegisterCda(document, createAndRegister.getDocumentMetadata(), uploadFileSession);

	}
	
	public LoggableResult<RetrievedDocumentSet> fetchDocument(String documentId,String patientId, String repositoryId, String homecommunityid,  boolean vaerdispring, String backendCode) {
		UserContextForQuery.patientCpr.set(patientId);
		UserContextForQuery.consentOverride.set(vaerdispring);

		RetrievedDocumentSet response = xdsRequestService.fetchDocument(documentId, repositoryId, homecommunityid, registryProfileIti18Service.getRegistryFromBackend(backendCode));
		LoggableResult<RetrievedDocumentSet> lr = new LoggableResult<RetrievedDocumentSet>(response, searchProfile.getRequestId(), searchProfile.getResponseId());
		return lr;

	}

	public DocumentMetadata getMetadata(CreateAndRegisterCdaRequest request) throws XdsException {
		String document = new String(Base64Utils.decodeFromString(request.getDocument()));
		DocumentMetadata metadata = cdaMetaDataFactory.createFromCdaRegistrationRequest(null, document);
		return metadata;
	}

	public List<String> validateSearchRequest(SearchRequest request) {

		List<String> errorList = new LinkedList<>();
		if (request.getBackendCode() == null || request.getBackendCode().isEmpty()) {
			errorList.add("Der skal være valgt et registry øverst i feltet Søg i");
		} else {
			request.setRegistryProfileIti18Dto(registryProfileIti18Service.getRegistryFromBackend(request.getBackendCode()));
		}
		return errorList;
	}
	
	public LoggableResult<QueryResponse> findDocuments(SearchRequest request) {
		UserContextForQuery.patientCpr.set(request.getPatientID());
		UserContextForQuery.consentOverride.set(request.isVaerdispring());
		QueryResponse response = xdsRequestService.searchForDocuments(request);
		LoggableResult<QueryResponse> lr = new LoggableResult<QueryResponse>(response, searchProfile.getRequestId(), searchProfile.getResponseId());
		return lr;
	}	
	
	public LoggableResult<QueryResponse> getDocumentEntry(String documentId, String patientId, boolean vaerdispring, RepositoryProfileIti41Dto repositoryProfileIti41Dto) throws XdsException {
		UserContextForQuery.patientCpr.set(patientId);
		UserContextForQuery.consentOverride.set(vaerdispring);
		RegistryProfileIti18Dto registryProfileIti18Dto = registryProfileIti18Service.getRegistryFromBackend(repositoryProfileIti41Dto != null ? repositoryProfileIti41Dto.getBackendCode() : null);
		QueryResponse response =  xdsRequestService.getDocumentEntry(documentId, registryProfileIti18Dto);
		LoggableResult<QueryResponse> lr = new LoggableResult<QueryResponse>(response, searchProfile.getRequestId(), searchProfile.getResponseId());
		return lr;
	}
	
	public void deprecateDocument(DocumentEntry toBeDeprecated, RepositoryProfileIti41Dto repositoryProfileIti41Dto) throws XdsException {
		xdsRequestService.deprecateDocument(toBeDeprecated, repositoryProfileIti41Dto);
	}
	
	
}
