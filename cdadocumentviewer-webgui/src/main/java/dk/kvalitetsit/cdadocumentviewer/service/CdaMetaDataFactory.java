package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.kvalitetsit.cdadocumentviewer.dto.CdaMetadata;
import dk.kvalitetsit.cdadocumentviewer.dto.Code;
import dk.kvalitetsit.cdadocumentviewer.dto.DocumentMetadata;
import dk.kvalitetsit.cdadocumentviewer.dto.Person;
import dk.s4.hl7.cda.convert.CDAMetadataXmlCodec;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.cdametadata.CDAMetadata;

@Component
public class CdaMetaDataFactory {
	
	@Autowired
	ReferenceDataService referenceDataService;
	
	public CdaMetaDataFactory(ReferenceDataService referenceDataService) {
		this.referenceDataService = referenceDataService;
	}

	private static Logger LOGGER = LoggerFactory.getLogger(CdaMetaDataFactory.class);
	
	public DocumentMetadata createFromCdaRegistrationRequest(CdaMetadata cdaMetadata, String document) {
		
		DocumentMetadata documentMetadata = new DocumentMetadata();
		if(cdaMetadata != null){
			documentMetadata.setAvailabilityStatus(cdaMetadata.getAvailabilityStatus());
//			documentMetadata.setClassCode(cdaMetadata.getClassCode()); //dont map, since it is output only
			documentMetadata.setFormatCode(cdaMetadata.getFormatCode());
			documentMetadata.setHealthcareFacilityTypeCode(cdaMetadata.getHealthcareFacilityTypeCode());
			documentMetadata.setObjectType(cdaMetadata.getObjectType());
			documentMetadata.setPracticeSettingCode(cdaMetadata.getPracticeSettingCode());
			documentMetadata.setSubmissionTime(cdaMetadata.getSubmissionTime());
			documentMetadata.setHomeCommunityId(cdaMetadata.getHomeCommunityId());
		}
		
		getMetadataFromDocument(documentMetadata, document);
		return documentMetadata;
	}
	

	private Code createCode(String code, String codeSystem, String displayName){
		Code returnCode = new Code();
		returnCode.setCode(code);
		returnCode.setCodingScheme(codeSystem);
		returnCode.setName(displayName); 
		return returnCode;
	}

	private void getMetadataFromDocument(DocumentMetadata documentMetadata, String document) {

		CDAMetadataXmlCodec codec = new CDAMetadataXmlCodec();
		CDAMetadata cdaMetadataDecoded = codec.decode(document);

		//author.authorInstitution - organization
		if (cdaMetadataDecoded.getAuthor() != null &&  cdaMetadataDecoded.getAuthor().getOrganizationIdentity() != null && cdaMetadataDecoded.getAuthor().getOrganizationIdentity().getId() != null) {
			String code  = cdaMetadataDecoded.getAuthor().getOrganizationIdentity().getId().getExtension();
			String codeSystem = cdaMetadataDecoded.getAuthor().getOrganizationIdentity().getId().getRoot();
			String displayName = cdaMetadataDecoded.getAuthor().getOrganizationIdentity().getOrgName();
			Code autherOrganizationCode = createCode(code, codeSystem, displayName);
			documentMetadata.setOrganisation(autherOrganizationCode);
			LOGGER.debug(documentMetadata.getOrganisation().toString());
		}
		
		//auther.authorperson
		if (cdaMetadataDecoded.getAuthorperson() != null) {
			Person authorPerson = new Person();
			if (cdaMetadataDecoded.getAuthorperson().getFamilyName() != null) {
				authorPerson.setFamilyName(cdaMetadataDecoded.getAuthorperson().getFamilyName());
				LOGGER.debug(authorPerson.getFamilyName());
			}
			if (cdaMetadataDecoded.getAuthorperson().getGivenNames() != null && cdaMetadataDecoded.getAuthorperson().getGivenNames().length > 0) {
				authorPerson.setGivenName(cdaMetadataDecoded.getAuthorperson().getGivenNames()[0]);
				LOGGER.debug(authorPerson.getGivenName());
				if (cdaMetadataDecoded.getAuthorperson().getGivenNames().length > 1) {
					authorPerson.setSecondAndFurtherGivenNames(cdaMetadataDecoded.getAuthorperson().getGivenNames()[1]);
					for (int i = 2; i< cdaMetadataDecoded.getAuthorperson().getGivenNames().length;i++) { 
						authorPerson.setSecondAndFurtherGivenNames(authorPerson.getSecondAndFurtherGivenNames() + "&" + cdaMetadataDecoded.getAuthorperson().getGivenNames()[i]);
						LOGGER.debug(authorPerson.getSecondAndFurtherGivenNames());
					}
				}
			}
			documentMetadata.setAuthorPerson(authorPerson);
		}
		
		//classCode
		Code code = null;
		if (cdaMetadataDecoded.getCodeCodedValue() != null && cdaMetadataDecoded.getCodeCodedValue().getCode() != null ) { //even though in form (as output only), the value must come from document
			String classCode = referenceDataService.getClassCodeFromTypeCode(cdaMetadataDecoded.getCodeCodedValue().getCode());

			if (classCode != null) {
				String classCodeName = referenceDataService.getClassCodeNameFromCode(classCode);
				if (classCodeName != null) {
					code = createCode(classCode, referenceDataService.getClassCodeScheme(), classCodeName);
					documentMetadata.setClassCode(code);					
				}
			}
		}
		if (code == null) {
			String defaultClassCode = referenceDataService.getDefaultClassCode();
			String classCodeName = referenceDataService.getClassCodeNameFromCode(defaultClassCode);
			if (classCodeName != null) {
				code = createCode(defaultClassCode, referenceDataService.getClassCodeScheme(), classCodeName);
				documentMetadata.setClassCode(code);					
			}
		}
		
		//confidentialityCode
		if (cdaMetadataDecoded.getConfidentialityCodeCodedValue() != null && cdaMetadataDecoded.getConfidentialityCodeCodedValue().getCode() != null && cdaMetadataDecoded.getConfidentialityCodeCodedValue().getCodeSystem() != null) {
			Code confidentialityCode = createCode(cdaMetadataDecoded.getConfidentialityCodeCodedValue().getCode(), cdaMetadataDecoded.getConfidentialityCodeCodedValue().getCodeSystem(), cdaMetadataDecoded.getConfidentialityCodeCodedValue().getDisplayName());
			documentMetadata.setConfidentialityCode(confidentialityCode);
			LOGGER.debug(documentMetadata.getConfidentialityCode().toString());
			
		}
		
		//contentTypeCode - not used
		
		//creationTime
		if (cdaMetadataDecoded.getCreationTime() != null) {
			documentMetadata.setReportTime(cdaMetadataDecoded.getCreationTime());
			LOGGER.debug(documentMetadata.getReportTime().toString());	
		}
		
		//eventCodedList
		for (CodedValue event : cdaMetadataDecoded.getEventCodeList()) {
			if(documentMetadata.getEventCodes() == null){
				documentMetadata.setEventCodes(new LinkedList<Code>());
			}
			Code eventCode = createCode(event.getCode(), event.getCodeSystem(), event.getDisplayName());
			documentMetadata.getEventCodes().add(eventCode);
			LOGGER.debug(eventCode.toString());
		}

		//formatcode
		if (cdaMetadataDecoded.getFormatCode() != null) { //overrule from possible input, since that from document has higher priority than from input
			Code formatCode = createCode(cdaMetadataDecoded.getFormatCode().getCode(), cdaMetadataDecoded.getFormatCode().getCodeSystem(), cdaMetadataDecoded.getFormatCode().getDisplayName());
			documentMetadata.setFormatCode(formatCode);
		}
		
		//LanguageCode
		documentMetadata.setLanguageCode(cdaMetadataDecoded.getLanguageCode());
		LOGGER.debug(documentMetadata.getLanguageCode());
		
		//legalAuthenticator
		if (cdaMetadataDecoded.getLegalAuthenticator() != null && cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity() != null) {
			Person legalAuthenticator = new Person();	
			if (cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getFamilyName() != null) {
				legalAuthenticator.setFamilyName(cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getFamilyName());
				LOGGER.debug(legalAuthenticator.getFamilyName());
			}
			if (cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames() != null && cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames().length > 0) {
				legalAuthenticator.setGivenName(cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames()[0]);
				LOGGER.debug(legalAuthenticator.getGivenName());
				if (cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames().length > 1) {
					legalAuthenticator.setSecondAndFurtherGivenNames(cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames()[1]);
					for (int i = 2; i< cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames().length;i++) {
						legalAuthenticator.setSecondAndFurtherGivenNames(legalAuthenticator.getSecondAndFurtherGivenNames() + "&" + cdaMetadataDecoded.getLegalAuthenticator().getPersonIdentity().getGivenNames()[i]);
						LOGGER.debug(legalAuthenticator.getSecondAndFurtherGivenNames());
					}		
				}
			}
			documentMetadata.setLegalAuthenticator(legalAuthenticator);
		}
		
		//patientId
		if (cdaMetadataDecoded.getPatientId() != null && cdaMetadataDecoded.getPatientId().getCode() != null && cdaMetadataDecoded.getPatientId().getCodeSystem() != null) {
			Code patientIdCode = createCode(cdaMetadataDecoded.getPatientId().getCode(), cdaMetadataDecoded.getPatientId().getCodeSystem(), "");
			documentMetadata.setPatientId(patientIdCode);
			LOGGER.debug(documentMetadata.getPatientId().toString());			
		}
		
		//serviceStartTime
		if (cdaMetadataDecoded.getServiceStartTime() != null) {
			documentMetadata.setServiceStartTime(cdaMetadataDecoded.getServiceStartTime());
			LOGGER.debug(documentMetadata.getServiceStartTime().toString());
		}
		
		//serviceStopTime
		if (cdaMetadataDecoded.getServiceStopTime() != null) {
			documentMetadata.setServiceStopTime(cdaMetadataDecoded.getServiceStopTime());
			LOGGER.debug(documentMetadata.getServiceStopTime().toString());
		}
		
		//sourcePatientId
		if (cdaMetadataDecoded.getSourcePatientId() != null && cdaMetadataDecoded.getSourcePatientId().getCode() != null && cdaMetadataDecoded.getSourcePatientId().getCodeSystem() != null) {
			Code sourcePatientIdCode = createCode(cdaMetadataDecoded.getSourcePatientId().getCode(), cdaMetadataDecoded.getSourcePatientId().getCodeSystem(), "");
			documentMetadata.setSourcePatientId(sourcePatientIdCode);
			LOGGER.debug(documentMetadata.getSourcePatientId().toString());			
		}

		//sourcePatientInfo
		if (cdaMetadataDecoded.getPatient() != null) {
			Person sourcePatientInfoPerson = new Person();
			if (cdaMetadataDecoded.getPatient().getFamilyName() != null) {
				sourcePatientInfoPerson.setFamilyName(cdaMetadataDecoded.getPatient().getFamilyName());
				LOGGER.debug(sourcePatientInfoPerson.getFamilyName());
			}
			if (cdaMetadataDecoded.getPatient().getGivenNames() != null && cdaMetadataDecoded.getPatient().getGivenNames().length > 0) {
				sourcePatientInfoPerson.setGivenName(cdaMetadataDecoded.getPatient().getGivenNames()[0]);
				LOGGER.debug(sourcePatientInfoPerson.getGivenName());
				if (cdaMetadataDecoded.getPatient().getGivenNames().length > 1) {
					sourcePatientInfoPerson.setSecondAndFurtherGivenNames(cdaMetadataDecoded.getPatient().getGivenNames()[1]);
					for (int i = 2; i< cdaMetadataDecoded.getPatient().getGivenNames().length;i++) { 
						sourcePatientInfoPerson.setSecondAndFurtherGivenNames(sourcePatientInfoPerson.getSecondAndFurtherGivenNames() + "&" + cdaMetadataDecoded.getPatient().getGivenNames()[i]);
						LOGGER.debug(sourcePatientInfoPerson.getSecondAndFurtherGivenNames());
					}
				}
			}
			documentMetadata.setSourcePatientInfoPerson(sourcePatientInfoPerson);
			if (cdaMetadataDecoded.getPatient().getBirthTime() != null) {
				documentMetadata.setSourcePatientInfoDateOfBirth(cdaMetadataDecoded.getPatient().getBirthTime());
				LOGGER.debug(documentMetadata.getSourcePatientInfoDateOfBirth().toString());
			}
			if (cdaMetadataDecoded.getPatient().getGender() != null) {
				documentMetadata.setSourcePatientInfoGender(cdaMetadataDecoded.getPatient().getGender().name().substring(0,1));
				LOGGER.debug(documentMetadata.getSourcePatientInfoGender());
			}
		}
		
		//title
		if (cdaMetadataDecoded.getTitle() != null) {
			documentMetadata.setTitle(cdaMetadataDecoded.getTitle());
			LOGGER.debug(documentMetadata.getTitle());
		}
		
		//typeCode
		if ((cdaMetadataDecoded.getCodeCodedValue() != null) && (cdaMetadataDecoded.getCodeCodedValue().getCode() != null) && (cdaMetadataDecoded.getCodeCodedValue().getCodeSystem() != null) && (cdaMetadataDecoded.getCodeCodedValue().getDisplayName() != null)) {
			Code typeCode = createCode(cdaMetadataDecoded.getCodeCodedValue().getCode(), cdaMetadataDecoded.getCodeCodedValue().getCodeSystem(), cdaMetadataDecoded.getCodeCodedValue().getDisplayName());
			documentMetadata.setTypeCode(typeCode);
			LOGGER.debug(documentMetadata.getTypeCode().toString());			
		}
		
		//uniqeId
		if (cdaMetadataDecoded.getId() != null && cdaMetadataDecoded.getId().getExtension() != null && cdaMetadataDecoded.getId().getRoot() != null) {
			documentMetadata.setUniqueId(cdaMetadataDecoded.getId().getRoot() + "^" + cdaMetadataDecoded.getId().getExtension());
		} else if (cdaMetadataDecoded.getId() != null && cdaMetadataDecoded.getId().getRoot() != null) {
			documentMetadata.setUniqueId(cdaMetadataDecoded.getId().getRoot());
		}
		
	}
	
}
