package dk.kvalitetsit.cdadocumentviewer.service;

import dk.kvalitetsit.cdadocumentviewer.configuration.RegistryProfileIti18Configuration;
import dk.kvalitetsit.cdadocumentviewer.configuration.RegistryProfileIti18Configuration.Reg;
import dk.kvalitetsit.cdadocumentviewer.dto.RegistryProfileIti18Dto;
import org.openehealth.ipf.commons.ihe.xds.iti18.Iti18PortType;
import org.openehealth.ipf.commons.ihe.xds.iti43.Iti43PortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

public class RegistryProfileIti18Service {

	private static Logger LOGGER = LoggerFactory.getLogger(RegistryProfileIti18Service.class);

	@Autowired
	private BeanFactory beanFactory;

	@Autowired
    private RegistryProfileIti18Configuration registryProfileIti18Configuration;

	private List<RegistryProfileIti18Dto> registryProfileList;
	private DropDownListService registryAsDropDownListForSearch;

	public RegistryProfileIti18Service(BeanFactory beanFactory, RegistryProfileIti18Configuration repositoryProfileIti41Configuration) {
		this.beanFactory = beanFactory;
		this.registryProfileIti18Configuration = repositoryProfileIti41Configuration;
		init();

	}
	public RegistryProfileIti18Service() {
			
	}
	
	@PostConstruct
	public void init() {
		registryProfileList = new ArrayList<RegistryProfileIti18Dto>();
		StringBuilder codes = new StringBuilder();
		StringBuilder names = new StringBuilder();

		if (registryProfileIti18Configuration.getReg().isEmpty() ) {
			throw new RuntimeException("ITI-18 or ITI-43 endpoint has not been configured");
		}

		for (Reg reg: registryProfileIti18Configuration.getReg()) {
			RegistryProfileIti18Dto registryProfileDto = new RegistryProfileIti18Dto();
			registryProfileDto.setDgws(reg.isDgwsenabled());
			registryProfileDto.setDgwsIti43(reg.isDgwsenablediti43());
			registryProfileDto.setEndpoint(reg.getEndpoint());
			registryProfileDto.setEndpointIti43(reg.getEndpointiti43());
			registryProfileDto.setBackendCode(reg.getBackendCode());
			registryProfileDto.setName(reg.getName());
			registryProfileDto.setTypeCodeCodes(reg.getTypeCodeCodes());
			registryProfileDto.setTypeCodeNames(reg.getTypeCodeNames());
			registryProfileDto.setTypeCodeSchemes(reg.getTypeCodeSchemes());
			registryProfileDto.setEventCodeSchemeCodes(reg.getEventCodeSchemeCodes());
			registryProfileDto.setEventCodeSchemeNames(reg.getEventCodeSchemeNames());
			registryProfileDto.setFormatCodeCodes(reg.getFormatCodeCodes());
			registryProfileDto.setFormatCodeNames(reg.getFormatCodeNames());
			registryProfileDto.setHealthcareFacilityTypeCodeCodes(reg.getHealthcareFacilityTypeCodeCodes());
			registryProfileDto.setHealthcareFacilityTypeCodeNames(reg.getHealthcareFacilityTypeCodeNames());
			registryProfileDto.setPracticeSettingCodeCodes(reg.getPracticeSettingCodeCodes());
			registryProfileDto.setPracticeSettingCodeNames(reg.getPracticeSettingCodeNames());
			registryProfileDto.setDefaultTypeStable(reg.isDefaultTypeStable());
			registryProfileDto.setDefaultTypeOnDemand(reg.isDefaultTypeOnDemand());

			Iti18PortType iti18PortType = beanFactory.getBean(Iti18PortType.class, registryProfileDto.getEndpoint(), registryProfileDto.isDgws());
			registryProfileDto.setIti18PortType(iti18PortType);
			Iti43PortType iti43PortType = beanFactory.getBean(Iti43PortType.class, registryProfileDto.getEndpointIti43(), registryProfileDto.isDgwsIti43());
			registryProfileDto.setIti43PortType(iti43PortType);

			registryProfileList.add(registryProfileDto);
			LOGGER.info("ITI18 Configuration loaded is: " + registryProfileDto.toString());

			if (codes.length() > 0) {
				codes.append(";");
				names.append(";");
			}
			codes.append(registryProfileDto.getBackendCode());
			names.append(registryProfileDto.getName());


		}
		registryAsDropDownListForSearch = new DropDownListService(codes.toString(), names.toString(), null);
		
	}
	
	public List<RegistryProfileIti18Dto> getRegistryProfileList() {
		return registryProfileList;
	}
	
	public DropDownListService getRegistryAsDropDownListForSearch() {
		return registryAsDropDownListForSearch;
	}

	public RegistryProfileIti18Dto getRegistryFromBackend(String backendCode) {
		for (RegistryProfileIti18Dto registryProfileIti18Dto: registryProfileList) {
			if (registryProfileIti18Dto != null && backendCode != null && registryProfileIti18Dto.getBackendCode().equals(backendCode) ) {
				LOGGER.debug("Finding registry by backendCode: " + backendCode + " Found is: " + registryProfileIti18Dto.getName());
				return registryProfileIti18Dto;
			}
		}
		return null;
	}


}
