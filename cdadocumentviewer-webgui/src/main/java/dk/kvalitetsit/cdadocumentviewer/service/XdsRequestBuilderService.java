package dk.kvalitetsit.cdadocumentviewer.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.activation.DataHandler;

import org.openehealth.ipf.commons.ihe.xds.core.ebxml.EbXMLAdhocQueryRequest;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.EbXMLFactory;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLFactory30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLProvideAndRegisterDocumentSetRequest30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.ProvideAndRegisterDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.RetrieveDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssigningAuthority;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Association;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssociationLabel;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssociationType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Author;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Code;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Document;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Identifiable;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.LocalizedString;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Name;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Organization;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.PatientInfo;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Person;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.SubmissionSet;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Vocabulary;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.XcnName;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.XpnName;
import org.openehealth.ipf.commons.ihe.xds.core.requests.ProvideAndRegisterDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.requests.QueryRegistry;
import org.openehealth.ipf.commons.ihe.xds.core.requests.query.FindDocumentsQuery;
import org.openehealth.ipf.commons.ihe.xds.core.requests.query.GetAssociationsQuery;
import org.openehealth.ipf.commons.ihe.xds.core.requests.query.GetDocumentsQuery;
import org.openehealth.ipf.commons.ihe.xds.core.requests.query.Query;
import org.openehealth.ipf.commons.ihe.xds.core.requests.query.QueryList;
import org.openehealth.ipf.commons.ihe.xds.core.requests.query.QueryReturnType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.lcm.SubmitObjectsRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.query.AdhocQueryRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.AssociationType1;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.ClassificationType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.ExternalIdentifierType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.InternationalStringType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.LocalizedStringType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.ObjectFactory;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.RegistryObjectListType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.RegistryPackageType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.SlotType1;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rim.ValueListType;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.ProvideAndRegisterDocumentSetTransformer;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.QueryRegistryTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.istack.ByteArrayDataSource;

import dk.kvalitetsit.cdadocumentviewer.dto.DocumentMetadata;
import dk.kvalitetsit.cdadocumentviewer.dto.SearchRequest;
import dk.kvalitetsit.cdadocumentviewer.xds.OrganisationIdAuthority;
import dk.kvalitetsit.cdadocumentviewer.xds.ProvideAndRegisterDocumentSetRequest;
import dk.kvalitetsit.cdadocumentviewer.xds.UUID;



@Component
public class XdsRequestBuilderService {
	
	private static Logger LOGGER = LoggerFactory.getLogger(XdsRequestBuilderService.class);
	
	@Autowired
	OrganisationIdAuthority organisationIdAuthority;
	

	@Autowired
	ReferenceDataService referenceDataService;
	
	private String mimeType = "text/xml";
	
	public RetrieveDocumentSetRequestType buildRetrieveDocumentSetRequestType(String documentId, String repositoryId, String homecommunityid){
		RetrieveDocumentSetRequestType retrieveDocumentSetRequestType = new RetrieveDocumentSetRequestType();
		RetrieveDocumentSetRequestType.DocumentRequest documentRequest = new RetrieveDocumentSetRequestType.DocumentRequest();
		documentRequest.setRepositoryUniqueId(repositoryId);
		documentRequest.setDocumentUniqueId(documentId);
		if (homecommunityid != null) {
			documentRequest.setHomeCommunityId(homecommunityid);
		}
		retrieveDocumentSetRequestType.getDocumentRequest().add(documentRequest);		
		return retrieveDocumentSetRequestType;
	}
	
	public ProvideAndRegisterDocumentSetRequest buildProvideAndRegisterDocumentSetRequest(String documentPayload, DocumentMetadata documentMetadata, String entryUuidToReplace) {

		ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet = new ProvideAndRegisterDocumentSet();
		
		Identifiable patientIdentifiable = null;
		if (documentMetadata.getPatientId() != null) {
			AssigningAuthority patientIdAssigningAuthority = new AssigningAuthority(documentMetadata.getPatientId().getCodingScheme());
			patientIdentifiable = new Identifiable(documentMetadata.getPatientId().getCode(), patientIdAssigningAuthority);
		}
		Identifiable sourcePatientIdentifiable = null;
		if (documentMetadata.getSourcePatientId() != null) {
			AssigningAuthority patientIdAssigningAuthority = new AssigningAuthority(documentMetadata.getSourcePatientId().getCodingScheme());
			sourcePatientIdentifiable = new Identifiable(documentMetadata.getPatientId().getCode(), patientIdAssigningAuthority);
		}

		//author.authorInstitution - organization
		Author author = new Author(); 
		if (documentMetadata.getOrganisation() != null && documentMetadata.getOrganisation().getCode() != null && documentMetadata.getOrganisation().getCodingScheme() != null) {
			AssigningAuthority organisationAssigningAuthority = new AssigningAuthority(documentMetadata.getOrganisation().getCodingScheme());
			Organization authorOrganisation = new Organization(documentMetadata.getOrganisation().getName(), documentMetadata.getOrganisation().getCode(), organisationAssigningAuthority);
			author.getAuthorInstitution().add(authorOrganisation);
		}
		
		//author.authorperson
		if (documentMetadata.getAuthorPerson() != null && (documentMetadata.getAuthorPerson().getFamilyName() != null ||documentMetadata.getAuthorPerson().getGivenName() != null )) {
			Name<?> authorName = new XcnName();
			
			if (documentMetadata.getAuthorPerson().getFamilyName() != null) {
				authorName.setFamilyName(documentMetadata.getAuthorPerson().getFamilyName());	
			}
			if (documentMetadata.getAuthorPerson().getGivenName() != null) {
				authorName.setGivenName(documentMetadata.getAuthorPerson().getGivenName());	
			}
			if (documentMetadata.getAuthorPerson().getSecondAndFurtherGivenNames() != null) {
				authorName.setSecondAndFurtherGivenNames(documentMetadata.getAuthorPerson().getSecondAndFurtherGivenNames());	
			}
			Person authorPerson = new Person();
			authorPerson.setName(authorName);
			author.setAuthorPerson(authorPerson);
		}
		
		String submissionSetUuid = generateUUID();
		String submissionSetId = generateUUIDAdjusted();
		SubmissionSet submissionSet = new SubmissionSet();
		submissionSet.setUniqueId(submissionSetId);
		submissionSet.setSourceId(submissionSetId);
		submissionSet.setLogicalUuid(submissionSetUuid);
		submissionSet.setEntryUuid(submissionSetUuid);
		submissionSet.setPatientId(patientIdentifiable);
		submissionSet.setTitle(new LocalizedString(submissionSetUuid));
		submissionSet.setAuthor(author);
		
		//TODO: What to do here???
		submissionSet.setContentTypeCode(new Code("NscContentType", new LocalizedString("NscContentType"), UUID.XDSSubmissionSet_contentTypeCode));
		//submissionSet.setContentTypeCode(createCode(documentMetadata.getContentTypeCode()));

		//i submessionTime		
		if (documentMetadata.getReportTime() != null) {
			submissionSet.setSubmissionTime(documentMetadata.getReportTimeStringUTC());
		}
		
		
		//i availabilityStatus
		submissionSet.setAvailabilityStatus(documentMetadata.getAvailabilityStatus());
		
		if (documentMetadata.getHomeCommunityId() != null) {
			submissionSet.setHomeCommunityId(documentMetadata.getHomeCommunityId());	
		}
		
		provideAndRegisterDocumentSet.setSubmissionSet(submissionSet);
		
		String documentUuid = generateUUID();
		
		DocumentEntry documentEntry = new DocumentEntry();
		// 4.1 Patient Identification
		//patientId
		documentEntry.setPatientId(patientIdentifiable);
		//sourcePatientId		
		documentEntry.setSourcePatientId(sourcePatientIdentifiable);

		// 4.2 Name, Address and Telecommunications
		//sourcePatientInfo
		PatientInfo sourcePatientInfo = new PatientInfo();
		documentEntry.setSourcePatientInfo(sourcePatientInfo);

		// 4.2.1 Name
		if (documentMetadata.getSourcePatientInfoPerson() != null && (documentMetadata.getSourcePatientInfoPerson().getFamilyName() != null) ||documentMetadata.getSourcePatientInfoPerson().getGivenName() != null ) {
			Name<?> name = new XpnName();
			if (documentMetadata.getSourcePatientInfoPerson().getFamilyName() != null) {
				name.setFamilyName(documentMetadata.getSourcePatientInfoPerson().getFamilyName());
			}
			if (documentMetadata.getSourcePatientInfoPerson().getGivenName() != null) {
				name.setGivenName(documentMetadata.getSourcePatientInfoPerson().getGivenName());	
			}
			if (documentMetadata.getSourcePatientInfoPerson().getSecondAndFurtherGivenNames() != null) {
				name.setSecondAndFurtherGivenNames(documentMetadata.getSourcePatientInfoPerson().getSecondAndFurtherGivenNames());
			}
			sourcePatientInfo.setName(name);
		}
		
		if (documentMetadata.getSourcePatientInfoGender() != null) {
			sourcePatientInfo.setGender(documentMetadata.getSourcePatientInfoGender());	
		}
		if (documentMetadata.getSourcePatientInfoDateOfBirth() != null) {
			
			sourcePatientInfo.setDateOfBirth(documentMetadata.getSourcePatientInfoDateOfBirthString());

		}
		
		
		documentEntry.setEntryUuid(generateUUID());
		documentEntry.setAuthor(author);
		documentEntry.setAvailabilityStatus(documentMetadata.getAvailabilityStatus());
		
		//i classCode
		if (documentMetadata.getClassCode() != null) {
			documentEntry.setClassCode(createCode(documentMetadata.getClassCode()));
		}
		
		//confidentialityCode
		if (documentMetadata.getConfidentialityCode() != null) {
			// Code name is most likely null
			LocalizedString confidentialityName = documentMetadata.getConfidentialityCode().getName()!=null? new LocalizedString(documentMetadata.getConfidentialityCode().getName()):new LocalizedString(documentMetadata.getConfidentialityCode().getCode());
			Code confidentialityCode = new Code(documentMetadata.getConfidentialityCode().getCode(), confidentialityName, documentMetadata.getConfidentialityCode().getCodingScheme());
			documentEntry.getConfidentialityCodes().add(confidentialityCode);
		}
		//creationTime
		if (documentMetadata.getReportTime() != null) {
			documentEntry.setCreationTime(documentMetadata.getReportTimeStringUTC());	
		}
		
		//eventCodedList		
		List<Code> eventCodesEntry = documentEntry.getEventCodeList();
		if (documentMetadata.getEventCodes() != null) {
			for (dk.kvalitetsit.cdadocumentviewer.dto.Code eventCode : documentMetadata.getEventCodes()) {
				eventCodesEntry.add(createCode(eventCode));
			}
		}
		//i formatCode
		if (documentMetadata.getFormatCode() != null) {
			documentEntry.setFormatCode(createCode(documentMetadata.getFormatCode()));
		}
		//i healthcareFacilityTypeCode
		if (documentMetadata.getHealthcareFacilityTypeCode() != null){
			documentEntry.setHealthcareFacilityTypeCode(createCode(documentMetadata.getHealthcareFacilityTypeCode()));
		}
		//LanguageCode		
		if (documentMetadata.getLanguageCode() != null) {
			documentEntry.setLanguageCode(documentMetadata.getLanguageCode());
		}
		
		documentEntry.setMimeType(mimeType); //mimetype is included because some repositories required this to work. Example is KIH
		
		//i objectType
		if (documentMetadata.getObjectType() != null) {
			documentEntry.setType(documentMetadata.getObjectType());
		}
	
		//title
		if (documentMetadata.getTitle() != null) {
			documentEntry.setTitle(new LocalizedString(documentMetadata.getTitle()));
		}
		//typeCode
		if (documentMetadata.getTypeCode() != null) {
			Code typeCode = new Code(documentMetadata.getTypeCode().getCode(), new LocalizedString(documentMetadata.getTypeCode().getName()), documentMetadata.getTypeCode().getCodingScheme());
			documentEntry.setTypeCode(typeCode);
		}
		//i practiceSettingCode
		if (documentMetadata.getPracticeSettingCode() != null) {
			documentEntry.setPracticeSettingCode(createCode(documentMetadata.getPracticeSettingCode()));
		}
		
		//uniqeId
		String extenalDocumentId = null;
		if (documentMetadata.getUniqueId() != null) {
			extenalDocumentId = documentMetadata.getUniqueId();
		}
		documentEntry.setUniqueId(extenalDocumentId); 
		documentEntry.setLogicalUuid(documentUuid);
		
		
		//legalAuthenticator
		if (documentMetadata.getLegalAuthenticator() != null && (documentMetadata.getLegalAuthenticator().getFamilyName() != null ||documentMetadata.getLegalAuthenticator().getGivenName() != null )) {
			Name<?> legalAuthenticatorName = new XcnName();
			if (documentMetadata.getLegalAuthenticator().getFamilyName() != null) {
				legalAuthenticatorName.setFamilyName(documentMetadata.getLegalAuthenticator().getFamilyName());	
			}
			if (documentMetadata.getLegalAuthenticator().getGivenName() != null) {
				legalAuthenticatorName.setGivenName(documentMetadata.getLegalAuthenticator().getGivenName());	
			}
			if (documentMetadata.getLegalAuthenticator().getSecondAndFurtherGivenNames() != null) {
				legalAuthenticatorName.setSecondAndFurtherGivenNames(documentMetadata.getLegalAuthenticator().getSecondAndFurtherGivenNames());	
			}
			Person legalAuthenticatorPerson = new Person();
			legalAuthenticatorPerson.setName(legalAuthenticatorName);
			documentEntry.setLegalAuthenticator(legalAuthenticatorPerson);
		}
		

		//serviceStartTime
		if (documentMetadata.getServiceStartTime() != null) {
			documentEntry.setServiceStartTime(documentMetadata.getServiceStartTimeStringUTC());
		}
		
		
		//serviceStopTime
		if (documentMetadata.getServiceStopTime() != null) {
			documentEntry.setServiceStopTime(documentMetadata.getServiceStopTimeStringUTC());	
		}
		
		if (documentMetadata.getHomeCommunityId() != null) {
			documentEntry.setHomeCommunityId(documentMetadata.getHomeCommunityId());	
		}
		

		//i referenceIdlist - not included

		Document document = new Document(documentEntry, new DataHandler(new ByteArrayDataSource(documentPayload.getBytes(), mimeType)));
		provideAndRegisterDocumentSet.getDocuments().add(document);

		Association association = new Association();
		association.setAssociationType(AssociationType.HAS_MEMBER);
		association.setEntryUuid(generateUUID());
		association.setSourceUuid(submissionSet.getEntryUuid());
		association.setTargetUuid(documentEntry.getEntryUuid());
		association.setAvailabilityStatus(documentMetadata.getAvailabilityStatus());
		association.setOriginalStatus(documentMetadata.getAvailabilityStatus());
		association.setNewStatus(documentMetadata.getAvailabilityStatus());
		association.setLabel(AssociationLabel.ORIGINAL);
		provideAndRegisterDocumentSet.getAssociations().add(association);
		
		if (entryUuidToReplace != null) {
			Association replacementAssociation = new Association(AssociationType.REPLACE, generateUUID(), documentEntry.getEntryUuid(), entryUuidToReplace);
			provideAndRegisterDocumentSet.getAssociations().add(replacementAssociation);
		}

		ProvideAndRegisterDocumentSetTransformer registerDocumentSetTransformer = new ProvideAndRegisterDocumentSetTransformer(getEbXmlFactory());
		EbXMLProvideAndRegisterDocumentSetRequest30 ebxmlRequest = (EbXMLProvideAndRegisterDocumentSetRequest30) registerDocumentSetTransformer.toEbXML(provideAndRegisterDocumentSet);
		ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetRequestType = ebxmlRequest.getInternal();

		ProvideAndRegisterDocumentSetRequest provideAndRegisterDocumentSetRequest = new ProvideAndRegisterDocumentSetRequest(extenalDocumentId, provideAndRegisterDocumentSetRequestType);
		return provideAndRegisterDocumentSetRequest;
		
		
	}

	private Code createCode(dk.kvalitetsit.cdadocumentviewer.dto.Code code) {
		Code result = new Code(code.getCode(), new LocalizedString(code.getName()), code.getCodingScheme());
		return result;
	}

	private static final EbXMLFactory ebXMLFactory = new EbXMLFactory30();

	protected EbXMLFactory getEbXmlFactory() {
		return ebXMLFactory;
	}

	private String generateUUID() {
		java.util.UUID uuid = java.util.UUID.randomUUID();
		return uuid.toString();
	}
	
	private String generateUUIDAdjusted() {
		java.util.UUID uuid = java.util.UUID.randomUUID();
		return Math.abs(uuid.getLeastSignificantBits()) + "." + Math.abs(uuid.getMostSignificantBits())+"."+Calendar.getInstance().getTimeInMillis(); 
	}
	
	public AdhocQueryRequest buildAdhocQueryRequest(SearchRequest request) {
		
		FindDocumentsQuery fdq = new FindDocumentsQuery();
		
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		AssigningAuthority authority = new AssigningAuthority(organisationIdAuthority.getPatientIdAuthority());

		
		// Patient ID
		Identifiable patientIdentifiable = new Identifiable(request.getPatientID(), authority);
		fdq.setPatientId(patientIdentifiable);
				
		
		if (request.getStatus() != null && request.getStatus().trim().length() > 0) {
			List<AvailabilityStatus> stati = new ArrayList<>();
			stati.add(AvailabilityStatus.valueOfOpcode(request.getStatus()));
			fdq.setStatus(stati);
		}
		
		// Type code
		if (request.getTypeCode() != null && !request.getTypeCode().isEmpty() && request.getTypeCodeScheme() != null && !request.getTypeCodeScheme().isEmpty()) {
			String[] codes = request.getTypeCode().split(",");
			List<Code> typeCodes = getCodes(codes, request.getTypeCodeScheme());
			fdq.setTypeCodes(typeCodes);
		}
		
		// Format code
		if (request.getFormatCode() != null && !request.getFormatCode().isEmpty()) {	
			String[] codes = request.getFormatCode().split(",");
			List<Code> formatCodes = getCodes(codes, referenceDataService.getFormatCodeScheme());
			
			fdq.setFormatCodes(formatCodes);
		}
		
		
		// Event code
		if (request.getEventCode() != null && !request.getEventCode().isEmpty() && request.getEventCodeScheme() != null && !request.getEventCodeScheme().isEmpty()) {
			String[] codes = request.getEventCode().split(",");
			String codesScheme = request.getEventCodeScheme();
			List<Code> eventCodes = getCodes(codes, codesScheme);
			fdq.setEventCodes(new QueryList<Code>());
			fdq.getEventCodes().getOuterList().add(eventCodes);
		}

		
		// HealthcareFacilityType code
		if (request.getHealthcareFacilityTypeCode() != null && !request.getHealthcareFacilityTypeCode().isEmpty()) {	
			String[] codes = request.getHealthcareFacilityTypeCode().split(",");
			List<Code> healthcareFacilityTypeCodes = getCodes(codes, referenceDataService.getHealthcareFacilityTypeCodeScheme());
			
			fdq.setHealthcareFacilityTypeCodes(healthcareFacilityTypeCodes);
		}
		
		
		// Practicesetting code
		if (request.getPracticesettingCode() != null && !request.getPracticesettingCode().isEmpty()) {	
			String[] codes = request.getPracticesettingCode().split(",");
			List<Code> practicesettingCodes = getCodes(codes, referenceDataService.getPracticeSettingCodeScheme());
			
			fdq.setPracticeSettingCodes(practicesettingCodes);
		}
		
		
		// ServiceStart
		if (request.getServiceStartFromLow() != null && !request.getServiceStartFromLow().isEmpty()) {
			try {
				Date startFromLow = dateFormat.parse(request.getServiceStartFromLow());
				LOGGER.info(dateTimeFormat.format(startFromLow));
				fdq.getServiceStartTime().setFrom(dateTimeFormat.format(startFromLow));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if (request.getServiceStartFromHigh() != null && !request.getServiceStartFromHigh().isEmpty()) {
			try {
				Date startFromHigh = dateFormat.parse(request.getServiceStartFromHigh());
				LOGGER.info(dateTimeFormat.format(startFromHigh));
				fdq.getServiceStartTime().setTo(dateTimeFormat.format(startFromHigh));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		
		// ServiceStop
		if (request.getServiceStopToLow() != null && !request.getServiceStopToLow().isEmpty()) {
			try {
				Date stopToLow = dateFormat.parse(request.getServiceStopToLow());
				LOGGER.info(dateTimeFormat.format(stopToLow));

				fdq.getServiceStopTime().setFrom(dateTimeFormat.format(stopToLow));

			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if (request.getServiceStopToHigh() != null && !request.getServiceStopToHigh().isEmpty()) {
			try {
				Date stopToHigh = dateFormat.parse(request.getServiceStopToHigh());
				LOGGER.info(dateTimeFormat.format(stopToHigh));
				fdq.getServiceStopTime().setTo(dateTimeFormat.format(stopToHigh));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if (request.isTypeOnDemand()) {
			if (fdq.getDocumentEntryTypes() == null) {
				fdq.setDocumentEntryTypes(new LinkedList<>());
			}
			fdq.getDocumentEntryTypes().add(DocumentEntryType.ON_DEMAND);
		}
		
		if (request.isTypeStable()) {
			if (fdq.getDocumentEntryTypes() == null) {
				fdq.setDocumentEntryTypes(new LinkedList<>());
			}
			fdq.getDocumentEntryTypes().add(DocumentEntryType.STABLE);
		}
		

		QueryRegistry queryRegistry = new QueryRegistry(fdq);
		queryRegistry.setReturnType(QueryReturnType.LEAF_CLASS);

		QueryRegistryTransformer queryRegistryTransformer = new QueryRegistryTransformer();
		EbXMLAdhocQueryRequest ebxmlAdhocQueryRequest = queryRegistryTransformer.toEbXML(queryRegistry);
		AdhocQueryRequest internal = (AdhocQueryRequest)ebxmlAdhocQueryRequest.getInternal();
		LOGGER.info(internal.toString());
		
		return internal;
	}
	

	public AdhocQueryRequest buildAdhocQueryRequest(String documentId) {
		List<String> uniqueIds = new LinkedList<String>();
		uniqueIds.add(documentId);

		GetDocumentsQuery gdq = new GetDocumentsQuery();
		gdq.setUniqueIds(uniqueIds);
		
		return createAdhocQueryRequest(gdq, QueryReturnType.LEAF_CLASS);
	}
	
	public AdhocQueryRequest buildGetAssociationsQueryRequest(String uuid) {
		List<String> uuids = new LinkedList<String>();
	    uuids.add(uuid);
	    GetAssociationsQuery geq = new GetAssociationsQuery();
	    geq.setUuids(uuids);
	    return createAdhocQueryRequest(geq, QueryReturnType.LEAF_CLASS);	    
	}
	
	
	private AdhocQueryRequest createAdhocQueryRequest(Query query, QueryReturnType qrt) {
		QueryRegistry queryRegistry = new QueryRegistry(query);
		if (qrt != null) {
			queryRegistry.setReturnType(qrt);
		}
		QueryRegistryTransformer queryRegistryTransformer = new QueryRegistryTransformer();
		EbXMLAdhocQueryRequest ebxmlAdhocQueryRequest = queryRegistryTransformer.toEbXML(queryRegistry);
		AdhocQueryRequest internal = (AdhocQueryRequest)ebxmlAdhocQueryRequest.getInternal();

		return internal;
	}
	
	
	private List<Code> getCodes(String[] codes, String typeCodeScheme) {
		List<Code> result = new ArrayList<>();
		for (String codeStr : codes) {
			Code code = new Code();
			code.setCode(codeStr);
			code.setSchemeName(typeCodeScheme);
			
			result.add(code);
		}
		
		return result;
	}
	
	public SubmitObjectsRequest buildDeprecateSubmitObjectsRequest(DocumentEntry documentEntry) {
		
		ObjectFactory factory = new ObjectFactory();
		SubmitObjectsRequest body = new SubmitObjectsRequest();
		RegistryObjectListType registryObjectList = factory.createRegistryObjectListType();
		RegistryPackageType registryPackageType = makeRegistryPackageType(documentEntry);
		registryObjectList.getIdentifiable().add(factory.createRegistryPackage(registryPackageType));
		registryObjectList.getIdentifiable().add(factory.createAssociation(makeAssociation(documentEntry.getEntryUuid(), documentEntry.getAvailabilityStatus().getQueryOpcode(), AvailabilityStatus.DEPRECATED.getQueryOpcode())));

		body.setRegistryObjectList(registryObjectList);

		return body;
	}
	
	private RegistryPackageType makeRegistryPackageType(DocumentEntry documentEntry) {
		RegistryPackageType registryPackage = new RegistryPackageType();
		registryPackage.setId("SubmissionSet");
		registryPackage.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage");
		registryPackage.setHome(documentEntry.getHomeCommunityId());
		
		String cpr = documentEntry.getPatientId().getId();
		String entryUuid = documentEntry.getEntryUuid();
		String sourceId = documentEntry.getRepositoryUniqueId();
		
		ClassificationType classificationNode = new ClassificationType();
		classificationNode.setClassifiedObject("SubmissionSet");
		classificationNode.setClassificationNode(Vocabulary.SUBMISSION_SET_CLASS_NODE);
		registryPackage.getClassification().add(classificationNode);
		
		Code docEntryTypeCode = documentEntry.getTypeCode();
		ClassificationType contentTypeClassification = new ClassificationType();
		contentTypeClassification.setClassifiedObject("SubmissionSet");
		contentTypeClassification.setClassificationScheme(Vocabulary.SUBMISSION_SET_CONTENT_TYPE_CODE_CLASS_SCHEME);		
		contentTypeClassification.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
		contentTypeClassification.setNodeRepresentation(docEntryTypeCode.getCode());
		contentTypeClassification.setName(makeInternationalStringType(docEntryTypeCode.getDisplayName().getValue()));		
		contentTypeClassification.getSlot().add(makeSlot("codingScheme", docEntryTypeCode.getSchemeName()));
		registryPackage.getClassification().add(contentTypeClassification);
		
		registryPackage.getExternalIdentifier().add(makeExternalIdentifier(Vocabulary.SUBMISSION_SET_UNIQUE_ID_EXTERNAL_ID, entryUuid));
		registryPackage.getExternalIdentifier().add(makeExternalIdentifier(Vocabulary.SUBMISSION_SET_PATIENT_ID_EXTERNAL_ID, 
				organisationIdAuthority.formatPatientIdentifier(cpr)));
		registryPackage.getExternalIdentifier().add(makeExternalIdentifier(Vocabulary.SUBMISSION_SET_SOURCE_ID_EXTERNAL_ID, sourceId));
		
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		registryPackage.getSlot().add(makeSlot(Vocabulary.SLOT_NAME_SUBMISSION_TIME, dateTimeFormat.format(new Date())));
		
		return registryPackage;
	}
	
	private SlotType1 makeSlot(String name, String value) {
		SlotType1 slot = new SlotType1();
		ValueListType slotList = new ValueListType();		
		slot.setName(name);
		slotList.getValue().add(value);
		slot.setValueList(slotList);		
		return slot;	
	}

	private InternationalStringType makeInternationalStringType(String value) {
		LocalizedStringType lst = new LocalizedStringType();		
		lst.setValue(value);
		return makeInternationalStringType(lst);
	}

	private InternationalStringType makeInternationalStringType(LocalizedStringType value) {
		InternationalStringType ist = new InternationalStringType();
		ist.getLocalizedString().add(value);
		return ist;
	}

	private ExternalIdentifierType makeExternalIdentifier(String identificationScheme, String value) {
		ExternalIdentifierType externalIdentifier = new ExternalIdentifierType();
		externalIdentifier.setObjectType("urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier");
		externalIdentifier.setIdentificationScheme(identificationScheme);
		externalIdentifier.setValue(value);
		externalIdentifier.setRegistryObject("SubmissionSet");
		return externalIdentifier;
	}
	private AssociationType1 makeAssociation(String documentEntryUUID, String originalStatus, String newStatus) {
		AssociationType1 assocation = new AssociationType1();

		assocation.setAssociationType(AssociationType.UPDATE_AVAILABILITY_STATUS.getOpcode30());
		assocation.setSourceObject("SubmissionSet");
		assocation.setTargetObject(documentEntryUUID);
		assocation.getSlot().add(makeSlot(Vocabulary.SLOT_NAME_ORIGINAL_STATUS, originalStatus));
		assocation.getSlot().add(makeSlot(Vocabulary.SLOT_NAME_NEW_STATUS, newStatus));

		return assocation;

	}

	
}
