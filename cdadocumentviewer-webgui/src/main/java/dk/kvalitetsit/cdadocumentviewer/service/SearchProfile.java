package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.UUID;

import dk.kvalitetsit.cdadocumentviewer.dgws.AbstractSosiUser;
import dk.kvalitetsit.cdadocumentviewer.dto.SearchProfileDto;

public class SearchProfile {

	AbstractSosiUser current;

	private String id = UUID.randomUUID().toString();

	private String responseId;

	private String requestId;

	public void setSearchRequest() {
		
	}
	
	public AbstractSosiUser getCurrent() {
		return current;
	}

	public void setCurrent(AbstractSosiUser current) {
		this.current = current;
	}

	public SearchProfileDto getProfilesFromName(String key) {
		return new SearchProfileDto(key, key);
	}

	public String getUniqueId() {
		return id;
	}

	public void setResponseId(String id) {
		this.responseId = id;
	}
	
	public String getResponseId() {
		return responseId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String id) {
		this.requestId = id;
	}
}
