package dk.kvalitetsit.cdadocumentviewer.service;

import java.util.List;

import dk.kvalitetsit.cdadocumentviewer.dto.RegistryProfileIti18Dto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import dk.kvalitetsit.cdadocumentviewer.dto.DropDownListDto;

@Component
public class ReferenceDataService {

	private static Logger LOGGER = LoggerFactory.getLogger(ReferenceDataService.class);

	@Value("${map.type.class.type.codes}")
	protected String mapTypeCodes;
	@Value("${map.type.class.class.codes}")
	protected String mapClassCodes;
	
	@Value("${class.code.scheme}")
	protected String classCodeScheme;
	@Value("${class.code.codes}")
	protected String classCodeCodes;
	@Value("${class.code.names}")
	protected String classCodeNames;

	@Value("${default.class.code}")
	protected String defaultClassCode;
	
	@Value("${availabilitystatus.codes}")
	private String availabilityStatusCodes;
	@Value("${availabilitystatus.names}")
	private String availabilityStatusNames;
	
	@Value("${format.code.scheme}")
	private String formatCodeScheme;
	@Value("${format.code.codes}")
	private String formatCodeCodes;
	@Value("${format.code.names}")
	private String formatCodeNames;

	@Value("${healthcarefacilitytype.code.scheme}")
	private String healthcareFacilityTypeCodeScheme;
	@Value("${healthcarefacilitytype.code.codes}")
	private String healthcareFacilityTypeCodeCodes;
	@Value("${healthcarefacilitytype.code.names}")
	private String healthcareFacilityTypeCodeNames; 	

	@Value("${practicesetting.code.scheme}")
	private String practiceSettingCodeScheme;
	@Value("${practicesetting.code.codes}")
	private String practiceSettingCodeCodes;
	@Value("${practicesetting.code.names}")
	private String practiceSettingCodeNames;

	@Value("${object.type.codes}")
	private String objectTypeCodes = "STABLE;";
	@Value("${object.type.names}")
	private String objectTypeNames = "Stable;";

	@Value("${type.code.codes}")
	private String typeCodeCodes;
	@Value("${type.code.names}")
	private String typeCodeNames;
	@Value("${type.code.schemes}")
	private String typeCodeSchemes;
	@Value("${event.code.scheme.codes}")
	private String eventCodeSchemeCodes;
	@Value("${event.code.scheme.names}")
	private String eventCodeSchemeNames;

	@Autowired
	RegistryProfileIti18Service registryProfileIti18Service;

	
	// classcode type code mapping ****************************************************************

	public String getClassCodeFromTypeCode(String typeCode) {
		return new DropDownListService(mapTypeCodes, mapClassCodes, null).getNameFromCode(typeCode);
	}
	
	// classcode ****************************************************************
	public String getClassCodeScheme() {
		return classCodeScheme;
	}
	
	public String getDefaultClassCode() {
		return defaultClassCode;
	}

	public String getClassCodeNameFromCode(String code) {
		return new DropDownListService(classCodeCodes, classCodeNames, null).getNameFromCode(code);
	}

	// formatcode ****************************************************************
	public List<DropDownListDto> getFormatCodeList(String backendCode) {
		if (backendCode != null && registryProfileIti18Service.getRegistryFromBackend(backendCode).getFormatCodeCodes() != null) {
			return new DropDownListService(registryProfileIti18Service.getRegistryFromBackend(backendCode).getFormatCodeCodes(),
					registryProfileIti18Service.getRegistryFromBackend(backendCode).getFormatCodeNames(), null).getDropDownListValues();
		}
		return new DropDownListService(formatCodeCodes, formatCodeNames, null).getDropDownListValues();
	}

	public String getFormatCodeNameFromCode(String formatCode) {
		return new DropDownListService(formatCodeCodes, formatCodeNames, null).getNameFromCode(formatCode);
	}

	public String getFormatCodeScheme() {
		return formatCodeScheme;
	}

	// Healthcare ****************************************************************
	public List<DropDownListDto> getHealthcareFacilityTypeCodeList(String backendCode) {
		if (backendCode != null && registryProfileIti18Service.getRegistryFromBackend(backendCode).getHealthcareFacilityTypeCodeCodes() != null) {
			return new DropDownListService(registryProfileIti18Service.getRegistryFromBackend(backendCode).getHealthcareFacilityTypeCodeCodes(),
					registryProfileIti18Service.getRegistryFromBackend(backendCode).getHealthcareFacilityTypeCodeNames(), null).getDropDownListValues();
		}
		return new DropDownListService(healthcareFacilityTypeCodeCodes, healthcareFacilityTypeCodeNames, null).getDropDownListValues();
	}
	
	public String getHealthcareFacilityTypeNameFromCode(String healthcareFacilityTypeCode) {
		return new DropDownListService(healthcareFacilityTypeCodeCodes, healthcareFacilityTypeCodeNames, null).getNameFromCode(healthcareFacilityTypeCode);
	}

	public String getHealthcareFacilityTypeCodeScheme() {
		return healthcareFacilityTypeCodeScheme;
	}

	// PracticeSetting ****************************************************************
	public List<DropDownListDto> getPracticeSettingCodeList(String backendCode) {
		if (backendCode != null && registryProfileIti18Service.getRegistryFromBackend(backendCode).getPracticeSettingCodeCodes() != null) {
			return new DropDownListService(registryProfileIti18Service.getRegistryFromBackend(backendCode).getPracticeSettingCodeCodes(),
					registryProfileIti18Service.getRegistryFromBackend(backendCode).getPracticeSettingCodeNames(), null).getDropDownListValues();
		}
		return new DropDownListService(practiceSettingCodeCodes, practiceSettingCodeNames, null).getDropDownListValues();
	}
	
	public String getPracticeSettingNameFromCode(String practiceSettingCode) {
		return new DropDownListService(practiceSettingCodeCodes, practiceSettingCodeNames, null).getNameFromCode(practiceSettingCode);
	}

	public String getPracticeSettingCodeScheme() {
		return practiceSettingCodeScheme;
	}
	
	// typecode ****************************************************************
	
	public List<DropDownListDto>  getTypeCodeList(String backendCode) {
		if (backendCode != null && registryProfileIti18Service.getRegistryFromBackend(backendCode).getTypeCodeCodes() != null) {
			RegistryProfileIti18Dto registryProfileIti18Dto = registryProfileIti18Service.getRegistryFromBackend(backendCode);
			LOGGER.info("RegistryProfileIti18Dto er: " + registryProfileIti18Dto.toString());

			return new DropDownListService(registryProfileIti18Dto.getTypeCodeCodes(),
					registryProfileIti18Dto.getTypeCodeNames(), registryProfileIti18Dto.getTypeCodeSchemes()).getDropDownListValues();
		}
		return new DropDownListService(typeCodeCodes, typeCodeNames, typeCodeSchemes).getDropDownListValues();
	}
	public String getRelatedTypeCodeScheme(String backendCode, String typeCode) {
		List<DropDownListDto> typeCodeList = getTypeCodeList(backendCode);
		for (DropDownListDto dropDownListDto : typeCodeList) {
			if (dropDownListDto.getCode().equals(typeCode)) {
				return dropDownListDto.getScheme();
			}
		}
		return null;
	}

	// availability ****************************************************************
	public List<DropDownListDto> getAvailabilityStatusList() {
		return new DropDownListService(availabilityStatusCodes, availabilityStatusNames, null).getDropDownListValues();
	}
	
	// objecttype ****************************************************************
	public List<DropDownListDto> getObjectTypeList() {
		return new DropDownListService(objectTypeCodes, objectTypeNames, null).getDropDownListValues();
	}

	// eventcode ****************************************************************
	public List<DropDownListDto> getEventCodeSchemeList(String backendCode) {
		if (backendCode != null && registryProfileIti18Service.getRegistryFromBackend(backendCode).getEventCodeSchemeCodes() != null) {
			return new DropDownListService(registryProfileIti18Service.getRegistryFromBackend(backendCode).getEventCodeSchemeCodes(),
					registryProfileIti18Service.getRegistryFromBackend(backendCode).getEventCodeSchemeNames(), null).getDropDownListValues();
		}
		return new DropDownListService(eventCodeSchemeCodes, eventCodeSchemeNames, null).getDropDownListValues();
	}
	
	public String getEventCodeSchemeFromCode(String eventCodeSchemeCode) {
		return new DropDownListService(eventCodeSchemeCodes, eventCodeSchemeNames, null).getNameFromCode(eventCodeSchemeCode);
	}
	
}
