package dk.kvalitetsit.cdadocumentviewer.service;

import dk.kvalitetsit.cdadocumentviewer.dto.RepositoryProfileIti41Dto;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.responses.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import dk.kvalitetsit.cdadocumentviewer.dto.DeprecateResponseInfo;
import dk.kvalitetsit.cdadocumentviewer.dto.LoggableResult;

@Component
public class DeprecateService {
	
	@Autowired
	RepositoryProfileIti41Service repositoryProfileIti41Service;

	@Autowired
	CdaDocumentService documentService;
	
	@Autowired
	SearchProfile searchProfile;

	
	public static final Logger LOGGER = LoggerFactory.getLogger(DeprecateService.class);

	public LoggableResult doDeprecate(String externalDocumentId, String repositoryUniqueId, String patientId) throws XdsException {
		DeprecateResponseInfo deprecateResponseInfo = new DeprecateResponseInfo();

		if (repositoryProfileIti41Service.isDeprecateAllowedForRepositoryUniqueId(repositoryUniqueId)) { //check just in case user edited url

			RepositoryProfileIti41Dto repositoryProfileIti41Dto = repositoryProfileIti41Service.getDeprecateRepositoryFromUniqueId(repositoryUniqueId);

			LOGGER.info("Deprecating document. externalDocumentId: " + externalDocumentId + ". repositoryId: " + repositoryUniqueId);
			LoggableResult<QueryResponse> loggableDocEntryQueryResponse = documentService.getDocumentEntry(externalDocumentId, patientId, true, repositoryProfileIti41Dto);
			
			QueryResponse docEntryQueryResponse = loggableDocEntryQueryResponse.getResult();
			if (docEntryQueryResponse != null && docEntryQueryResponse.getDocumentEntries() != null && docEntryQueryResponse.getDocumentEntries().size() > 0) {
				
				
				LOGGER.info("Document was found before deprecate using backend: " + (repositoryProfileIti41Dto != null ? repositoryProfileIti41Dto.getBackendCode() : null));
				DocumentEntry toBeDeprecated = docEntryQueryResponse.getDocumentEntries().get(0);
				documentService.deprecateDocument(toBeDeprecated, repositoryProfileIti41Dto);
				LOGGER.info("Document was deprecated");
			} else {
				LOGGER.info("Document was not found before deprecate");
				deprecateResponseInfo.addError("Dokumentet " + externalDocumentId + " kunne ikke fremsøges til deprecate");
				if (docEntryQueryResponse != null && docEntryQueryResponse.getErrors() != null && !docEntryQueryResponse.getErrors().isEmpty()) {
					deprecateResponseInfo.addErrorAll(ErrorConverterUtil.convertErrors(docEntryQueryResponse.getErrors()));
				}	
			}
		} else {
			LOGGER.info("Deprecating document is not allowed. externalDocumentId: " + externalDocumentId + ". repositoryId: " + repositoryUniqueId);
			deprecateResponseInfo.addError("Repository er ikke konfiguret til deprecate");
		}
		LoggableResult<DeprecateResponseInfo> lr = new LoggableResult<DeprecateResponseInfo>(deprecateResponseInfo, searchProfile.getRequestId(), searchProfile.getResponseId());
		return lr;
	}

}