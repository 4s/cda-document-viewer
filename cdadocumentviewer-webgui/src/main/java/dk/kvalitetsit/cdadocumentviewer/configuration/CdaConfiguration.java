package dk.kvalitetsit.cdadocumentviewer.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dk.kvalitetsit.cdadocumentviewer.xds.Codes;
import dk.kvalitetsit.cdadocumentviewer.xds.OrganisationIdAuthority;


@Configuration
public class CdaConfiguration {

	@Bean
	public OrganisationIdAuthority organisationIdAuthority() {
		return new OrganisationIdAuthority(Codes.DK_SOR_CLASSIFICAION_OID);
	}
	
}
