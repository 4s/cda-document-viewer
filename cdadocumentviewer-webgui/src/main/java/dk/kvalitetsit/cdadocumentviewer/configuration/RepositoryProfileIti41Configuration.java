package dk.kvalitetsit.cdadocumentviewer.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "xds.iti41.repos")
public class RepositoryProfileIti41Configuration {

	private List<Repo> repo = new ArrayList<Repo>();
	
    public static class Repo {

    		private String name;
    		private String endpoint;
    		private boolean dgwsenabled;
    		private String endpointiti57;
    		private boolean dgwsenablediti57;
    		private String backendCode;
    		private String repositoryuniqueid;
    		private String homeCommunityId;
    		private boolean uploadallowed;
    		private boolean updateallowed;
    		private boolean deprecateallowed;
    		
    		private List<String> validdoctypes = new ArrayList<String>();
    		
    		public Repo() {
    			
    		}
    		public Repo(String name, String endpoint, boolean dgws, String endpointiti57, boolean dgwsiti57, String backendCode, String repositoryuniqueid, String homeCommunityId, boolean uploadallowed, boolean updateallowed, boolean deprecateallowed) {
    			this.name = name;
    			this.endpoint = endpoint;
    			this.dgwsenabled = dgws;
    			this.endpointiti57 = endpointiti57;
    			this.dgwsenablediti57 = dgwsiti57;
    			this.backendCode = backendCode;
    			this.repositoryuniqueid = repositoryuniqueid;
    			this.homeCommunityId = homeCommunityId;
    			this.uploadallowed = uploadallowed;
    			this.updateallowed = updateallowed;
    			this.deprecateallowed = deprecateallowed;
    		}


    		public String getName() {
    			return name;
    		}
    		public void setName(String name) {
    			this.name = name;
    		}
    		public String getEndpoint() {
    			return endpoint;
    		}
    		public void setEndpoint(String endpoint) {
    			this.endpoint = endpoint;
    		}
			public boolean isDgwsenabled() {
				return dgwsenabled;
			}
			public void setDgwsenabled(boolean dgwsenabled) {
				this.dgwsenabled = dgwsenabled;
			}
    		public String getEndpointiti57() {
    			return endpointiti57;
    		}
    		public void setEndpointiti57(String endpointiti57) {
    			this.endpointiti57 = endpointiti57;
    		}
			public boolean isDgwsenablediti57() {
				return dgwsenablediti57;
			}
			public void setDgwsenablediti57(boolean dgwsenablediti57) {
				this.dgwsenablediti57 = dgwsenablediti57;
			}
			public String getBackendCode() {
				return backendCode;
			}
			public void setBackendCode(String backendCode) {
			this.backendCode = backendCode;
		}
			public String getRepositoryuniqueid() {
				return repositoryuniqueid;
			}
			public void setRepositoryuniqueid(String repositoryuniqueid) {
				this.repositoryuniqueid = repositoryuniqueid;
			}
			public String getHomeCommunityId() {
				return homeCommunityId;
			}
			public void setHomeCommunityId(String homeCommunityId) {
				this.homeCommunityId = homeCommunityId;
			}
			public boolean isUploadallowed() {
				return uploadallowed;
			}
			public void setUploadallowed(boolean uploadallowed) {
				this.uploadallowed = uploadallowed;
			}
			public boolean isUpdateallowed() {
				return updateallowed;
			}
			public void setUpdateallowed(boolean updateallowed) {
				this.updateallowed = updateallowed;
			}
			public boolean isDeprecateallowed() {
				return deprecateallowed;
			}
			public void setDeprecateallowed(boolean deprecateallowed) {
				this.deprecateallowed = deprecateallowed;
			}
			public List<String> getValiddoctypes() {
				return validdoctypes;
			}
			public void setValiddoctypes(List<String> validdoctypes) {
				this.validdoctypes = validdoctypes;
			}
			
			public void addValidDoctype(String validDoctype) {
				validdoctypes.add(validDoctype);
			}
    		
    	}

		public List<Repo> getRepo() {
			return repo;
		}

		public void setRepo(List<Repo> repo) {
			this.repo = repo;
		}
		
		public void addRepo(Repo r) {
			repo.add(r);
		}
    	
		public String getNamesForSelection() {
			StringBuilder names = new StringBuilder(); 
			for (Repo r: repo) {
				if (names.length() > 0) {
					names.append(";");
				}
				names.append(r.getName());
			}
			return names.toString();
		}
    	
		public String getEndpointsForSelection() {
			StringBuilder endpoints = new StringBuilder(); 
			for (Repo r: repo) {
				if (endpoints.length() > 0) {
					endpoints.append(";");
				}
				endpoints.append(r.getEndpoint());
			}
			return endpoints.toString();
		}
	
}
