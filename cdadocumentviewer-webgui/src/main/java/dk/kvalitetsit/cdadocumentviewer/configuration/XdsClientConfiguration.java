package dk.kvalitetsit.cdadocumentviewer.configuration;

import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.namespace.QName;

import dk.kvalitetsit.cdadocumentviewer.service.RegistryProfileIti18Service;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.openehealth.ipf.commons.ihe.ws.WsTransactionConfiguration;
import org.openehealth.ipf.commons.ihe.xds.core.XdsClientFactory;
import org.openehealth.ipf.commons.ihe.xds.iti18.Iti18PortType;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti43.Iti43PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import dk.kvalitetsit.cdadocumentviewer.client.Iti18TestClient;
import dk.kvalitetsit.cdadocumentviewer.client.Iti41TestClient;
import dk.kvalitetsit.cdadocumentviewer.client.Iti43TestClient;
import dk.kvalitetsit.cdadocumentviewer.client.Iti57TestClient;
import dk.kvalitetsit.cdadocumentviewer.cxf.InResponseInterceptor;
import dk.kvalitetsit.cdadocumentviewer.cxf.OutRequestInterceptor;
import dk.kvalitetsit.cdadocumentviewer.dgws.DefaultFocesUser;
import dk.kvalitetsit.cdadocumentviewer.dgws.MocesUserFactory;
import dk.kvalitetsit.cdadocumentviewer.dgws.UserContextForQuery;
import dk.kvalitetsit.cdadocumentviewer.service.RepositoryProfileIti41Service;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfile;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfileService;

@Configuration
public class XdsClientConfiguration {

	private static Logger LOGGER = LoggerFactory.getLogger(XdsClientConfiguration.class);
	
	private TrustManager[] trustAllCerts = new TrustManager[] {
		       new X509TrustManager() {
		          public java.security.cert.X509Certificate[] getAcceptedIssuers() {
		            return null;
		          }

		          public void checkClientTrusted(X509Certificate[] certs, String authType) {  }

		          public void checkServerTrusted(X509Certificate[] certs, String authType) {  }

		       }
		    };
	
	@Value("${moces.hsuid.issuer}")
	private String issuer;

	@Value("${moces.hsuid.systemversion}")
	private String systemversion;

	@Value("${moces.hsuid.orgname}")
	private String orgname;

	@Value("${moces.hsuid.systemname}")
	private String systemname;

	@Value("${moces.hsuid.systemowner}")
	private String systemowner;

	@Value("${moces.hsuid.usertype}")
	private String usertype;
	
	@Value("${moces.active.users:0}")
	private String activeMocesUsers;
	
	@Autowired
	private ApplicationContext appContext;
	
	@Bean
	@Scope(value = "prototype")
	public Iti18PortType getDocumentRegistryServiceIti18(String endpoint, boolean dgwsEnabled) {
		LOGGER.info("Creating Iti18PortType for url: "+endpoint);
		
		if (dgwsEnabled) {
			Iti18PortType client = new Iti18TestClient(endpoint);
			return client;
		} else {

			XdsClientFactory xdsClientFactory = generateXdsRegistryClientFactory("wsdl/iti18.wsdl", endpoint, Iti18PortType.class);
			Iti18PortType client = (Iti18PortType) xdsClientFactory.getClient();

			initProxy(client, false, true);	

			return client;
		}
	}
	
	@Bean
	@Scope(value = "prototype")
	public Iti41PortType getDocumentRepositoryServiceIti41(String endpoint, boolean dgwsEnabled) {
		LOGGER.info("Creating Iti41PortType for url: "+endpoint);
		
		if (dgwsEnabled) {
			Iti41PortType client = new Iti41TestClient(endpoint);
			return client;
		} else {
			XdsClientFactory xdsClientFactory = generateXdsRepositoryClientFactory("wsdl/iti41.wsdl", endpoint, Iti41PortType.class);
			Iti41PortType client = (Iti41PortType) xdsClientFactory.getClient();

			initProxy(client, false, true);

			return client;
		}
	}
	
	@Bean
	@Scope(value = "prototype")
	public Iti43PortType getDocumentRepositoryServiceIti43(String endpoint, boolean dgwsEnabled) {
		LOGGER.info("Creating Iti43PortType for url: "+endpoint);

		if (dgwsEnabled) {
			Iti43PortType client = new Iti43TestClient(endpoint);
			return client;
		} else {

			XdsClientFactory xdsClientFactory = generateXdsRepositoryClientFactory("wsdl/iti43.wsdl", endpoint, Iti43PortType.class);
			Iti43PortType client = (Iti43PortType) xdsClientFactory.getClient();

			initProxy(client, false, true);	

			return client;
		}
	}
	
	@Bean
	@Scope(value = "prototype")
	public Iti57PortType getDocumentRepositoryServiceIti57(String endpoint, boolean dgwsEnabled) {
		LOGGER.info("Creating Iti57PortType for url: "+endpoint);
		
		if (dgwsEnabled) {
			Iti57PortType client = new Iti57TestClient(endpoint);
			return client;
		} else {
			XdsClientFactory xdsClientFactory = generateXdsRegistryClientFactory("urn:ihe:iti:xds-b:2010", "wsdl/iti57.wsdl", endpoint, Iti57PortType.class);
			Iti57PortType client = (Iti57PortType) xdsClientFactory.getClient();

			initProxy(client, false, true);

			return client;
		}
	}
	
	private XdsClientFactory generateXdsRegistryClientFactory(String wsdl, String url, Class<?> clazz){
		return generateXdsRegistryClientFactory("urn:ihe:iti:xds-b:2007", wsdl, url, clazz);
	}

	private XdsClientFactory generateXdsRegistryClientFactory(String namespace, String wsdl, String url, Class<?> clazz){
		final WsTransactionConfiguration WS_CONFIG = new WsTransactionConfiguration(
				new QName(namespace, "DocumentRegistry_Service",
						"ihe"), clazz, new QName(
								namespace,
								"DocumentRegistry_Binding_Soap12", "ihe"), false,
				wsdl, true, false, false, false);

		return new XdsClientFactory(WS_CONFIG, url, null, null,null);
	}

	private XdsClientFactory generateXdsRepositoryClientFactory(String wsdl, String url, Class<?> clazz){
		final WsTransactionConfiguration WS_CONFIG = new WsTransactionConfiguration(
				new QName("urn:ihe:iti:xds-b:2007", "DocumentRepository_Service",
						"ihe"), clazz, new QName(
								"urn:ihe:iti:xds-b:2007",
								"DocumentRepository_Binding_Soap12", "ihe"), true,
				wsdl, true, false, false, false);
		

		XdsClientFactory xcf = new XdsClientFactory(WS_CONFIG, url, null, null,null);
		return xcf;
	}

	
	private void initProxy(Object o, boolean dgwsEnabled, boolean addRequestInterceptor) {		
		Client proxy = ClientProxy.getClient(o);		
		
		if (LOGGER.isDebugEnabled()) {
			proxy.getOutInterceptors().add(new LoggingOutInterceptor());
			proxy.getInInterceptors().add(new LoggingInInterceptor());
			
		}	
		
		if (addRequestInterceptor) {
			InResponseInterceptor in = appContext.getBean(InResponseInterceptor.class);
			proxy.getInInterceptors().add(in);
			
			OutRequestInterceptor out = appContext.getBean(OutRequestInterceptor.class);
			proxy.getOutInterceptors().add(out);
		}
		
		HTTPConduit conduit = (HTTPConduit)proxy.getConduit();
		TLSClientParameters tcp = new TLSClientParameters();
		tcp.setTrustManagers(trustAllCerts);
		tcp.setHostnameVerifier(new HostnameVerifier() {			
			@Override
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});
		conduit.setTlsClientParameters(tcp);
	}
	
	@Bean
	public DefaultFocesUser defaultFoces() {
		return new DefaultFocesUser();
	}
		
	@Bean
	public MocesUserFactory mocesUserFactory() {
		String[] codeParts = activeMocesUsers.split(";");
		return new MocesUserFactory(issuer, orgname, systemname, systemversion, systemowner, usertype, Arrays.asList(codeParts));
	}
	
	@Bean
	public UserContextForQuery userContextForQuery() {
		return new UserContextForQuery();
	}
	
	@Bean
	public InResponseInterceptor inResponseInterceptor() {
		return new InResponseInterceptor();
	}

	@Bean
	public OutRequestInterceptor outRequestInterceptor() {
		return new OutRequestInterceptor();
	}

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public SearchProfile searchProfile() {
		SearchProfile searchProfile = new SearchProfile();
		return searchProfile;
	}
	
	@Bean
	public SearchProfileService searchProfileService() {
		return new SearchProfileService();
	}
	
	@Bean
	public RepositoryProfileIti41Service repositoryProfileIti41Service() {
		return new RepositoryProfileIti41Service();
	}
	@Bean
	public RegistryProfileIti18Service registryProfileIti18Service() {
		return new RegistryProfileIti18Service();
	}

}
