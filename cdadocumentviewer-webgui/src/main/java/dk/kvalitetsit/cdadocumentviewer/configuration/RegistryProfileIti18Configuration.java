package dk.kvalitetsit.cdadocumentviewer.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "xds.iti18.registry")
public class RegistryProfileIti18Configuration {

	private List<Reg> reg = new ArrayList<Reg>();
	
    public static class Reg {

		    private String backendCode;
    		private String name;
    		private String endpoint;
    		private boolean dgwsenabled;
    		private String endpointiti43;
    		private boolean dgwsenablediti43;
    		private String typeCodeCodes;
    		private String typeCodeNames;
			private String typeCodeSchemes;
    		private String eventCodeSchemeCodes;
    		private String eventCodeSchemeNames;
    		private String formatCodeCodes;
    		private String formatCodeNames;
    		private String healthcareFacilityTypeCodeCodes;
    		private String healthcareFacilityTypeCodeNames;
    		private String practiceSettingCodeCodes;
    		private String practiceSettingCodeNames;
    		private boolean defaultTypeStable;
    		private boolean defaultTypeOnDemand;

    		public Reg() {
    			
    		}
    		public Reg(String backendCode, String name, String endpoint, boolean dgws, String endpointiti43, boolean dgwsiti43) {
    			this.backendCode = backendCode;
				this.name = name;
    			this.endpoint = endpoint;
    			this.dgwsenabled = dgws;
    			this.endpointiti43 = endpointiti43;
    			this.dgwsenablediti43 = dgwsiti43;

    		}


    		public String getName() {
    			return name;
    		}
    		public void setName(String name) {
    			this.name = name;
    		}
			public String getBackendCode() {
			return backendCode;
		}
			public void setBackendCode(String backendCode) {
			this.backendCode = backendCode;
		}
    		public String getEndpoint() {
    			return endpoint;
    		}
    		public void setEndpoint(String endpoint) {
    			this.endpoint = endpoint;
    		}
			public boolean isDgwsenabled() {
				return dgwsenabled;
			}
			public void setDgwsenabled(boolean dgwsenabled) {
				this.dgwsenabled = dgwsenabled;
			}
    		public String getEndpointiti43() {
    			return endpointiti43;
    		}
    		public void setEndpointiti43(String endpointiti43) {
    			this.endpointiti43 = endpointiti43;
    		}
			public boolean isDgwsenablediti43() {
				return dgwsenablediti43;
			}
			public void setDgwsenablediti43(boolean dgwsenablediti43) {
				this.dgwsenablediti43 = dgwsenablediti43;
			}

			public String getTypeCodeCodes() {
				return typeCodeCodes;
			}

			public void setTypeCodeCodes(String typeCodeCodes) {
				this.typeCodeCodes = typeCodeCodes;
			}

			public String getTypeCodeNames() {
				return typeCodeNames;
			}

			public void setTypeCodeNames(String typeCodeNames) {
				this.typeCodeNames = typeCodeNames;
			}

			public String getTypeCodeSchemes() {
				return typeCodeSchemes;
			}

			public void setTypeCodeSchemes(String typeCodeSchemes) {
				this.typeCodeSchemes = typeCodeSchemes;
			}



			public String getEventCodeSchemeCodes() {
				return eventCodeSchemeCodes;
			}

			public void setEventCodeSchemeCodes(String eventCodeSchemeCodes) {
				this.eventCodeSchemeCodes = eventCodeSchemeCodes;
			}

			public String getEventCodeSchemeNames() {
				return eventCodeSchemeNames;
			}

			public void setEventCodeSchemeNames(String eventCodeSchemeNames) {
				this.eventCodeSchemeNames = eventCodeSchemeNames;
			}

			public String getFormatCodeCodes() {
				return formatCodeCodes;
			}

			public void setFormatCodeCodes(String formatCodeCodes) {
				this.formatCodeCodes = formatCodeCodes;
			}

			public String getFormatCodeNames() {
				return formatCodeNames;
			}

			public void setFormatCodeNames(String formatCodeNames) {
				this.formatCodeNames = formatCodeNames;
			}

			public String getHealthcareFacilityTypeCodeCodes() {
				return healthcareFacilityTypeCodeCodes;
			}

			public void setHealthcareFacilityTypeCodeCodes(String healthcareFacilityTypeCodeCodes) {
				this.healthcareFacilityTypeCodeCodes = healthcareFacilityTypeCodeCodes;
			}

			public String getHealthcareFacilityTypeCodeNames() {
				return healthcareFacilityTypeCodeNames;
			}

			public void setHealthcareFacilityTypeCodeNames(String healthcareFacilityTypeCodeNames) {
				this.healthcareFacilityTypeCodeNames = healthcareFacilityTypeCodeNames;
			}

			public String getPracticeSettingCodeCodes() {
				return practiceSettingCodeCodes;
			}

			public void setPracticeSettingCodeCodes(String practiceSettingCodeCodes) {
				this.practiceSettingCodeCodes = practiceSettingCodeCodes;
			}

			public String getPracticeSettingCodeNames() {
				return practiceSettingCodeNames;
			}

			public void setPracticeSettingCodeNames(String practiceSettingCodeNames) {
				this.practiceSettingCodeNames = practiceSettingCodeNames;
			}
			public boolean isDefaultTypeStable() {
				return defaultTypeStable;
			}

			public void setDefaultTypeStable(boolean defaultTypeStable) {
				this.defaultTypeStable = defaultTypeStable;
			}

			public boolean isDefaultTypeOnDemand() {
				return defaultTypeOnDemand;
			}
			public void setDefaultTypeOnDemand(boolean defaultTypeOnDemand) {
				this.defaultTypeOnDemand = defaultTypeOnDemand;
			}
	}

		public List<Reg> getReg() {
			return reg;
		}

		public void setReg(List<Reg> reg) {
			this.reg = reg;
		}
		
		public void addReg(Reg r) {
			reg.add(r);
		}
    	
		public String getNamesForSelection() {
			StringBuilder names = new StringBuilder();
			for (Reg r: reg) {
				if (names.length() > 0) {
					names.append(";");
				}
				names.append(r.getName());
			}
			return names.toString();
		}

		public String getEndpointsForSelection() {
			StringBuilder endpoints = new StringBuilder();
			for (Reg r: reg) {
				if (endpoints.length() > 0) {
					endpoints.append(";");
				}
				endpoints.append(r.getEndpoint());
			}
			return endpoints.toString();
		}
	
}
