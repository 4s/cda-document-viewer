package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

import java.util.ArrayList;
import java.util.List;


public class AuthorMetadata {

	private List<AuthorInstitutionMetadata> authorInstitution = new ArrayList<>();
	private AuthorPersonMetadata authorPerson;
	
	public AuthorMetadata() {
		
	}
	public AuthorMetadata(List<AuthorInstitutionMetadata> authorInstitution, AuthorPersonMetadata authorPerson) {

		this.authorInstitution = authorInstitution;
		this.authorPerson = authorPerson;
	}
	public List<AuthorInstitutionMetadata> getAuthorInstitution() {
		return authorInstitution;
	}
	public void setAuthorInstitutions(List<AuthorInstitutionMetadata> authorInstitutions) {
		this.authorInstitution = authorInstitutions;
	}
	public AuthorPersonMetadata getAuthorPerson() {
		return authorPerson;
	}
	public void setAuthorPerson(AuthorPersonMetadata authorPerson) {
		this.authorPerson = authorPerson;
	}
	@Override
	public String toString() {
		return "AuthorMetadata [authorInstitutions=" + authorInstitution + ", authorPerson=" + authorPerson + "]";
	}

}
