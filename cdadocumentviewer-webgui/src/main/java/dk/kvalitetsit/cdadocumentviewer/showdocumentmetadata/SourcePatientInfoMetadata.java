package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

public class SourcePatientInfoMetadata {

	private String givenName;
	private String secondAndFurtherGivenNames;
	private String familyName;
	private String gender;
	private String dateOfBirth;
	
	public SourcePatientInfoMetadata() {
		
	}
	public SourcePatientInfoMetadata(String givenName, String secondAndFurtherGivenNames, String familyName) {
		this.givenName = givenName;
		this.secondAndFurtherGivenNames = secondAndFurtherGivenNames;
		this.familyName = familyName;
	}
	
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getSecondAndFurtherGivenNames() {
		return secondAndFurtherGivenNames;
	}
	public void setSecondAndFurtherGivenNames(String secondAndFurtherGivenNames) {
		this.secondAndFurtherGivenNames = secondAndFurtherGivenNames;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	@Override
	public String toString() {
		return "SourcePatientInfoMetadata [givenName=" + givenName + ", secondAndFurtherGivenNames="
				+ secondAndFurtherGivenNames + ", familyName=" + familyName + ", gender=" + gender + ", dateOfBirth="
				+ dateOfBirth + "]";
	}

	
}
