package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

public class AuthorPersonMetadata {

	private String givenName;
	private String secondAndFurtherGivenNames;
	private String familyName;

	public AuthorPersonMetadata() {

	}
	public AuthorPersonMetadata(String givenName, String secondAndFurtherGivenNames, String familyName) {
		this.givenName = givenName;
		this.secondAndFurtherGivenNames = secondAndFurtherGivenNames;
		this.familyName = familyName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getSecondAndFurtherGivenNames() {
		return secondAndFurtherGivenNames;
	}
	public void setSecondAndFurtherGivenNames(String secondAndFurtherGivenNames) {
		this.secondAndFurtherGivenNames = secondAndFurtherGivenNames;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setAuthorPerson_familyName(String familyName) {
		this.familyName = familyName;
	}
	@Override
	public String toString() {
		return "AuthorPersonMetadata [givenName=" + givenName
				+ ", secondAndFurtherGivenNames=" + secondAndFurtherGivenNames
				+ ", familyName=" + familyName + "]";
	}

}
