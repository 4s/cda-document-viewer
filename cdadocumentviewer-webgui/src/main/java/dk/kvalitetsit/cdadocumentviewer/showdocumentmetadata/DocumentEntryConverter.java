package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Author;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Code;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Identifiable;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Organization;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.PatientInfo;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.ReferenceId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DocumentEntryConverter {
	
	private static Logger LOGGER = LoggerFactory.getLogger(DocumentEntryConverter.class);

	
	public DocumentMetadata getShowDocumentMetadataFromJsonBase64Encoded(String documentEntryJsonFormatBase64Encoded) {

		if (documentEntryJsonFormatBase64Encoded != null) {
			try {
				byte[] decodedBytes = Base64.getDecoder().decode(documentEntryJsonFormatBase64Encoded);
				ObjectMapper objectMapper = new ObjectMapper();
				DocumentMetadata documentMetadata = objectMapper.readValue(new String(decodedBytes), DocumentMetadata.class);
				return documentMetadata;
			} catch (IOException | IllegalArgumentException e) {
				LOGGER.error("Fejl ved json til model konvertering", e);
			}
		}
		return null; //metadata tabellen udfyldes ikke på siden
	}
	
	public String getJsonBase64EncodedFromDocumentEntry(DocumentEntry documentEntry) throws JsonProcessingException {
		DocumentMetadata showDocumentMetadata = createShowDocumentMetadata(documentEntry);
		return getJsonBase64EncodedFromDocumentEntry(showDocumentMetadata);
	}

	public String getJsonBase64EncodedFromDocumentEntry(DocumentMetadata showDocumentMetadata) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString( showDocumentMetadata );

		String encodedString = Base64.getEncoder().encodeToString(json.getBytes());
		return encodedString;
	}

	public DocumentMetadata createShowDocumentMetadata(DocumentEntry documentEntry) throws JsonProcessingException {
		DocumentMetadata showDocumentMetadata = new DocumentMetadata();

		for (Author author : documentEntry.getAuthors()) {
			showDocumentMetadata.addAuthor(getAuthor(author));
		}

		showDocumentMetadata.setAvailabilityStatus(documentEntry.getAvailabilityStatus());
		showDocumentMetadata.setClassCode(getCodeMetadata(documentEntry.getClassCode()));
		showDocumentMetadata.setComments(documentEntry.getComments() != null ? documentEntry.getComments().getValue() : null);
		for (Code code : documentEntry.getConfidentialityCodes()) {
			showDocumentMetadata.addConfidentialityCode(getCodeMetadata(code));
		}

		showDocumentMetadata.setCreationTime(getDateTime(documentEntry.getCreationTime()));//Datoerne er ikke iorden. skal komme ud som 2014-05-31T07:37:00.000Z
		showDocumentMetadata.setDocumentAvailability(documentEntry.getDocumentAvailability()); 

		showDocumentMetadata.setEntryUuid(documentEntry.getEntryUuid());
		for (Code code : documentEntry.getEventCodeList()) {
			showDocumentMetadata.addEventCode(getCodeMetadata(code));
		}
		showDocumentMetadata.setExtraMetadata(documentEntry.getExtraMetadata() != null ? documentEntry.getExtraMetadata().toString() : null);
		showDocumentMetadata.setFormatCode(getCodeMetadata(documentEntry.getFormatCode()));
		showDocumentMetadata.setHash(documentEntry.getHash());
		showDocumentMetadata.setHealthcareFacilityTypeCode(getCodeMetadata(documentEntry.getHealthcareFacilityTypeCode()));
		showDocumentMetadata.setHomeCommunityId(documentEntry.getHomeCommunityId());
		showDocumentMetadata.setLanguageCode(documentEntry.getLanguageCode());
		if (documentEntry.getLegalAuthenticator() != null && documentEntry.getLegalAuthenticator().getName() != null) {
			showDocumentMetadata.setLegalAuthenticator_givenName(documentEntry.getLegalAuthenticator().getName().getGivenName());
			showDocumentMetadata.setLegalAuthenticator_secondAndFurtherGivenNames(documentEntry.getLegalAuthenticator().getName().getSecondAndFurtherGivenNames());
			showDocumentMetadata.setLegalAuthenticator_familyName(documentEntry.getLegalAuthenticator().getName().getFamilyName());
		}
		showDocumentMetadata.setLogicalUuid(documentEntry.getLogicalUuid());
		showDocumentMetadata.setMimeType(documentEntry.getMimeType());
		showDocumentMetadata.setTypeUuid(documentEntry.getType() != null ? documentEntry.getType().getUuid() : null);
		showDocumentMetadata.setPatientId(getSourcePatientIdMetadata(documentEntry.getPatientId()));
		showDocumentMetadata.setPracticeSettingCode(getCodeMetadata(documentEntry.getPracticeSettingCode()));
		for (ReferenceId reference : documentEntry.getReferenceIdList()) {
			if (reference != null) {
				showDocumentMetadata.addReferenceId(new ReferenceIdMetadata(reference.getId()));
			}
		}
		showDocumentMetadata.setRepositoryUniqueId(documentEntry.getRepositoryUniqueId());

		showDocumentMetadata.setServiceStartTime(getDateTime(documentEntry.getServiceStartTime()));
		showDocumentMetadata.setServiceStopTime(getDateTime(documentEntry.getServiceStopTime()));
		showDocumentMetadata.setSize(documentEntry.getSize());

		showDocumentMetadata.setSourcePatientId(getSourcePatientIdMetadata(documentEntry.getSourcePatientId()));
		showDocumentMetadata.setSourcePatientInfo(getSourcePatientInfoMetadata(documentEntry.getSourcePatientInfo()));
		showDocumentMetadata.setTitle(documentEntry.getTitle() != null ? documentEntry.getTitle().getValue() : null);
		showDocumentMetadata.setType(documentEntry.getType() != null ? documentEntry.getType().toString() : null);
		showDocumentMetadata.setTypeCode(getCodeMetadata(documentEntry.getTypeCode()));
		showDocumentMetadata.setUniqueId(documentEntry.getUniqueId());
		showDocumentMetadata.setUri(documentEntry.getUri());
		showDocumentMetadata.setVersion(documentEntry.getVersion() != null ? documentEntry.getVersion().getVersionName() : null);
		
		//special handling for on-demand
		if (DocumentEntryType.ON_DEMAND.equals(documentEntry.getType())) {
			String valueNA = "< not applicable >";
			
			if (showDocumentMetadata.getSize() == null) {
				showDocumentMetadata.setSize(valueNA);
			}
			if (showDocumentMetadata.getHash() == null) {
				showDocumentMetadata.setHash(valueNA);
			}
		}


		return showDocumentMetadata;
	}


	private AuthorMetadata getAuthor(Author author) {

		if (author != null) {
			List<AuthorInstitutionMetadata> authorInstitutions = new ArrayList<>();
			AuthorPersonMetadata authorPerson = null;
			for (Organization org : author.getAuthorInstitution()) {
				if (org != null) {
					authorInstitutions.add(new AuthorInstitutionMetadata(org.getIdNumber(), org.getOrganizationName(), org.getAssigningAuthority() != null ? org.getAssigningAuthority().getUniversalId() : null));
				}
			}
			if (author.getAuthorPerson() != null && author.getAuthorPerson().getName() != null) {
				authorPerson = new AuthorPersonMetadata(author.getAuthorPerson().getName().getGivenName(), author.getAuthorPerson().getName().getSecondAndFurtherGivenNames(), author.getAuthorPerson().getName().getFamilyName());
			}
			return new AuthorMetadata(authorInstitutions, authorPerson);
		}
		return null;
	}

	private CodeMetadata getCodeMetadata(Code code) {
		if (code != null) {
			return new CodeMetadata(code.getCode(), code.getDisplayName() != null ? code.getDisplayName().getValue() : null, code.getSchemeName());
		}
		return null;
	}

	private String getDateTime(DateTime dateTime) {
		if (dateTime != null) {
			DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
			return fmt.print(dateTime);
		}
		return null;
	}
	private String getDate(DateTime dateTime) {
		if (dateTime != null) {
			DateTimeFormatter fmt = ISODateTimeFormat.date();
			return fmt.print(dateTime);
		}
		return null;
	}

	private SourcePatientIdMetadata getSourcePatientIdMetadata(Identifiable sourcePatientId) {
		if (sourcePatientId != null ) {
			return new SourcePatientIdMetadata(sourcePatientId.getId(), sourcePatientId.getAssigningAuthority() != null ? sourcePatientId.getAssigningAuthority().getUniversalId() : null);	
		}
		return null;
	}


	private SourcePatientInfoMetadata getSourcePatientInfoMetadata(PatientInfo patientInfo) {

		if (patientInfo != null) {
			SourcePatientInfoMetadata sourcePatientInfoMetadata;
			if (patientInfo.getName() != null) {
				sourcePatientInfoMetadata = new SourcePatientInfoMetadata(patientInfo.getName().getGivenName(), patientInfo.getName().getSecondAndFurtherGivenNames(), patientInfo.getName().getFamilyName());	
			} else {
				sourcePatientInfoMetadata = new SourcePatientInfoMetadata();
			}
			sourcePatientInfoMetadata.setGender(patientInfo.getGender());
			sourcePatientInfoMetadata.setDateOfBirth(getDate(patientInfo.getDateOfBirth()));
			return sourcePatientInfoMetadata;
		}
		return null;
	}


}
