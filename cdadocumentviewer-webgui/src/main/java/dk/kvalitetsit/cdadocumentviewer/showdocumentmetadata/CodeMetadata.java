package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

public class CodeMetadata {

	private String code;
	private String displayName;
	private String schemeName;
	public CodeMetadata() {
		
	}
	public CodeMetadata(String code, String displayName, String schemeName) {
		this.code = code;
		this.displayName = displayName;
		this.schemeName = schemeName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(String schemeName) {
		this.schemeName = schemeName;
	}
	@Override
	public String toString() {
		return "CodeMetadata [code=" + code + ", displayName=" + displayName + ", schemeName=" + schemeName + "]";
	}


}
