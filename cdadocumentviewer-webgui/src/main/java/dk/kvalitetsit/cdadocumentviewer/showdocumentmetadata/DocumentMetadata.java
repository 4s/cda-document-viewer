package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

import java.util.ArrayList;
import java.util.List;

import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentAvailability;


public class DocumentMetadata {

	private List<AuthorMetadata> authors = new ArrayList<>();
	private AvailabilityStatus availabilityStatus;
	private CodeMetadata classCode;
	private String comments;
	private List<CodeMetadata> confidentialityCodes = new ArrayList<>();
	private String creationTime;
	private DocumentAvailability documentAvailability;
	private String entryUuid;
	private List<CodeMetadata> eventCodeList = new ArrayList<>();
	private String extraMetadata;
	private CodeMetadata formatCode;
	private String hash;
	private CodeMetadata healthcareFacilityTypeCode;
	private String homeCommunityId;
	private String languageCode;
	private String legalAuthenticator_givenName;
	private String legalAuthenticator_secondAndFurtherGivenNames;
	private String legalAuthenticator_familyName;
	private String logicalUuid;
	private String mimeType;
	private String typeUuid;

	private SourcePatientIdMetadata patientId;
	private CodeMetadata practiceSettingCode;
	private List<ReferenceIdMetadata> referenceIdList = new ArrayList<>();
	private String repositoryUniqueId;
	private String serviceStartTime;
	private String serviceStopTime;
	private String size; //I metadata er feltet Long, men fordi data skal vises som not applicable når on-demand type, er den her String
	private SourcePatientIdMetadata sourcePatientId;
	private SourcePatientInfoMetadata sourcePatientInfo;
	private String title;
	private String type;
	private CodeMetadata typeCode;
	private String uniqueId;
	private String uri;
	private String version;
	
	
	public List<AuthorMetadata> getAuthors() {
		return authors;
	}
	public void setAuthors(List<AuthorMetadata> authors) {
		this.authors = authors;
	}
	public void addAuthor(AuthorMetadata author) {
		this.authors.add(author);
	}
	public AvailabilityStatus getAvailabilityStatus() {
		return availabilityStatus;
	}
	public void setAvailabilityStatus(AvailabilityStatus availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}
	public CodeMetadata getClassCode() {
		return classCode;
	}
	public void setClassCode(CodeMetadata classCode) {
		this.classCode = classCode;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public List<CodeMetadata> getConfidentialityCodes() {
		return confidentialityCodes;
	}
	public void addConfidentialityCode(CodeMetadata confidentialityCode) {
		this.confidentialityCodes.add(confidentialityCode);
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public DocumentAvailability getDocumentAvailability() {
		return documentAvailability;
	}
	public void setDocumentAvailability(DocumentAvailability documentAvailability) {
		this.documentAvailability = documentAvailability;
	}
	public String getEntryUuid() {
		return entryUuid;
	}
	public void setEntryUuid(String entryUuid) {
		this.entryUuid = entryUuid;
	}
	public List<CodeMetadata> getEventCodeList() {
		return eventCodeList;
	}
	public void addEventCode(CodeMetadata eventCode) {
		this.eventCodeList.add(eventCode);
	}
	public String getExtraMetadata() {
		return extraMetadata;
	}
	public void setExtraMetadata(String extraMetadata) {
		this.extraMetadata = extraMetadata;
	}
	public CodeMetadata getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(CodeMetadata formatCode) {
		this.formatCode = formatCode;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public CodeMetadata getHealthcareFacilityTypeCode() {
		return healthcareFacilityTypeCode;
	}
	public void setHealthcareFacilityTypeCode(CodeMetadata healthcareFacilityTypeCode) {
		this.healthcareFacilityTypeCode = healthcareFacilityTypeCode;
	}
	public String getHomeCommunityId() {
		return homeCommunityId;
	}
	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLegalAuthenticator_givenName() {
		return legalAuthenticator_givenName;
	}
	public void setLegalAuthenticator_givenName(String legalAuthenticator_givenName) {
		this.legalAuthenticator_givenName = legalAuthenticator_givenName;
	}
	public String getLegalAuthenticator_secondAndFurtherGivenNames() {
		return legalAuthenticator_secondAndFurtherGivenNames;
	}
	public void setLegalAuthenticator_secondAndFurtherGivenNames(String legalAuthenticator_secondAndFurtherGivenNames) {
		this.legalAuthenticator_secondAndFurtherGivenNames = legalAuthenticator_secondAndFurtherGivenNames;
	}
	public String getLegalAuthenticator_familyName() {
		return legalAuthenticator_familyName;
	}
	public void setLegalAuthenticator_familyName(String legalAuthenticator_familyName) {
		this.legalAuthenticator_familyName = legalAuthenticator_familyName;
	}
	public String getLogicalUuid() {
		return logicalUuid;
	}
	public void setLogicalUuid(String logicalUuid) {
		this.logicalUuid = logicalUuid;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public String getTypeUuid() {
		return typeUuid;
	}
	public void setTypeUuid(String typeUuid) {
		this.typeUuid = typeUuid;
	}
	public SourcePatientIdMetadata getPatientId() {
		return patientId;
	}
	public void setPatientId(SourcePatientIdMetadata patientId) {
		this.patientId = patientId;
	}
	public CodeMetadata getPracticeSettingCode() {
		return practiceSettingCode;
	}
	public void setPracticeSettingCode(CodeMetadata practiceSettingCode) {
		this.practiceSettingCode = practiceSettingCode;
	}
	public List<ReferenceIdMetadata> getReferenceIdList() {
		return referenceIdList;
	}
	public void addReferenceId(ReferenceIdMetadata referenceId) {
		this.referenceIdList.add(referenceId);
	}
	public String getRepositoryUniqueId() {
		return repositoryUniqueId;
	}
	public void setRepositoryUniqueId(String repositoryUniqueId) {
		this.repositoryUniqueId = repositoryUniqueId;
	}
	public String getServiceStartTime() {
		return serviceStartTime;
	}
	public void setServiceStartTime(String serviceStartTime) {
		this.serviceStartTime = serviceStartTime;
	}
	public String getServiceStopTime() {
		return serviceStopTime;
	}
	public void setServiceStopTime(String serviceStopTime) {
		this.serviceStopTime = serviceStopTime;
	}
	public String getSize() {
		return size;
	}
	public void setSize(Long size) {
		if (size != null) {
			this.setSize(Long.toString(size));	
		}
	}
	public void setSize(String size) {
		this.size = size;
	}
	public SourcePatientIdMetadata getSourcePatientId() {
		return sourcePatientId;
	}
	public void setSourcePatientId(SourcePatientIdMetadata sourcePatientId) {
		this.sourcePatientId = sourcePatientId;
	}
	public SourcePatientInfoMetadata getSourcePatientInfo() {
		return sourcePatientInfo;
	}
	public void setSourcePatientInfo(SourcePatientInfoMetadata sourcePatientInfo) {
		this.sourcePatientInfo = sourcePatientInfo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public CodeMetadata getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(CodeMetadata typeCode) {
		this.typeCode = typeCode;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	@Override
	public String toString() {
		return "DocumentMetadata [authors=" + authors + ", availabilityStatus=" + availabilityStatus + ", classCode="
				+ classCode + ", comments=" + comments + ", confidentialityCodes=" + confidentialityCodes
				+ ", creationTime=" + creationTime + ", documentAvailability=" + documentAvailability + ", entryUuid="
				+ entryUuid + ", eventCodeList=" + eventCodeList + ", extraMetadata=" + extraMetadata + ", formatCode="
				+ formatCode + ", hash=" + hash + ", healthcareFacilityTypeCode=" + healthcareFacilityTypeCode
				+ ", homeCommunityId=" + homeCommunityId + ", languageCode=" + languageCode
				+ ", legalAuthenticator_givenName=" + legalAuthenticator_givenName
				+ ", legalAuthenticator_secondAndFurtherGivenNames=" + legalAuthenticator_secondAndFurtherGivenNames
				+ ", legalAuthenticator_familyName=" + legalAuthenticator_familyName + ", logicalUuid=" + logicalUuid
				+ ", mimeType=" + mimeType + ", typeUuid=" + typeUuid + ", practiceSettingCode=" + practiceSettingCode
				+ ", referenceIdList=" + referenceIdList + ", repositoryUniqueId=" + repositoryUniqueId
				+ ", serviceStartTime=" + serviceStartTime + ", serviceStopTime=" + serviceStopTime + ", size=" + size
				+ ", sourcePatientId=" + sourcePatientId + ", sourcePatientInfo=" + sourcePatientInfo + ", title="
				+ title + ", type=" + type + ", typeCode=" + typeCode + ", uniqueId=" + uniqueId + ", uri=" + uri
				+ ", version=" + version + "]";
	}

	
}
