package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

public class SourcePatientIdMetadata {

	private String id;
	private String assigningAuthority_universalId;
	
	public SourcePatientIdMetadata() {
		
	}
	
	public SourcePatientIdMetadata(String id, String assigningAuthority_universalId) {
		this.id = id;
		this.assigningAuthority_universalId = assigningAuthority_universalId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAssigningAuthority_universalId() {
		return assigningAuthority_universalId;
	}
	public void setAssigningAuthority_universalId(String assigningAuthority_universalId) {
		this.assigningAuthority_universalId = assigningAuthority_universalId;
	}

	@Override
	public String toString() {
		return "SourcePatientIdMetadata [id=" + id + ", assigningAuthority_universalId="
				+ assigningAuthority_universalId + "]";
	}
	
	
}
