package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

public class AuthorInstitutionMetadata {

		String idNumber;
		String organizationName;
		String assigningAuthority_universalId;
		
		
		public AuthorInstitutionMetadata() {
			
		}
		public AuthorInstitutionMetadata(String idNumber, String organizationName, String assigningAuthority_universalId) {
			this.idNumber = idNumber;
			this.organizationName = organizationName;
			this.assigningAuthority_universalId = assigningAuthority_universalId;
		}
		public String getIdNumber() {
			return idNumber;
		}
		public void setIdNumber(String idNumber) {
			this.idNumber = idNumber;
		}
		public String getOrganizationName() {
			return organizationName;
		}
		public void setOrganizationName(String organizationName) {
			this.organizationName = organizationName;
		}
		public String getAssigningAuthority_universalId() {
			return assigningAuthority_universalId;
		}
		public void setAssigningAuthority_universalId(String assigningAuthority_universalId) {
			this.assigningAuthority_universalId = assigningAuthority_universalId;
		}
		@Override
		public String toString() {
			return "AuthorInstitutionMetadata [idNumber=" + idNumber + ", organizationName=" + organizationName
					+ ", assigningAuthority_universalId=" + assigningAuthority_universalId + "]";
		}

	
}
