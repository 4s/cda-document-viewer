package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

public class ReferenceIdMetadata {

	private String id;

	public ReferenceIdMetadata() {
		
	}
	
	public ReferenceIdMetadata(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ReferenceIdMetadata [id=" + id + "]";
	}
	
	
}
