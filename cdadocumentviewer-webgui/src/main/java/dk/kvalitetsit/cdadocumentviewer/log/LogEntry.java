package dk.kvalitetsit.cdadocumentviewer.log;

import java.io.Serializable;

public class LogEntry implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String payload;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
