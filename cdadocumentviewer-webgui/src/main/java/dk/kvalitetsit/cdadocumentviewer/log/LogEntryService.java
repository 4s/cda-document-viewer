package dk.kvalitetsit.cdadocumentviewer.log;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service
public class LogEntryService {
	
	@Cacheable(value="logentry", key = "#id")
	public LogEntry getLogFromId(String id) {
		LogEntry le = new LogEntry();
		le.setId(id);
		return le;
	}
	
	@CachePut(value="logentry", key = "#id")
	public LogEntry saveLog(String id, String payload) {
		LogEntry le = new LogEntry();
		le.setId(id);
		le.setPayload(payload);
		return le;
	}
}
