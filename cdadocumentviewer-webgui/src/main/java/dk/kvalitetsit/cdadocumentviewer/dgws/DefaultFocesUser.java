package dk.kvalitetsit.cdadocumentviewer.dgws;

import dk.nsp.test.idp.OrganizationIdentities;
import dk.nsp.test.idp.model.Identity;

public class DefaultFocesUser extends AbstractSosiUser {

	private Identity identity =  OrganizationIdentities.sundhedsdatastyrelsen();
	
	//Pt returnere alle metoder null, da man ikke søger med FOCES pt
	
	@Override
	public String getIssuer() {
		return null;
	}

	@Override
	public String getAuhorizationCode() {
		return null;
	}

	@Override
	public String getOrg() {
		return null;
	}

	@Override
	public String getSystemVersion() {
		return null;
	}

	@Override
	public String getSystemName() {
		return null;
	}

	@Override
	public String getSystemOwner() {
		return null;
	}

	@Override
	public String getSor() {
		return null;
	}

	@Override
	public String getUserCpr() {
		return null;
	}

	@Override
	public String getUserType() {
		return null;
	}

	@Override
	public Identity getIdentity() {
		return identity;
	}
	
}
