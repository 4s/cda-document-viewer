package dk.kvalitetsit.cdadocumentviewer.dgws;

import java.util.ArrayList;
import java.util.List;

import dk.nsi.hsuid.ActingUserCivilRegistrationNumberAttribute;
import dk.nsi.hsuid.Attribute;
import dk.nsi.hsuid.CitizenCivilRegistrationNumberAttribute;
import dk.nsi.hsuid.ConsentOverrideAttribute;
import dk.nsi.hsuid.HealthcareServiceUserIdentificationHeaderUtil;
import dk.nsi.hsuid.OperationsOrganisationNameAttribute;
import dk.nsi.hsuid.OrganisationIdentifierAttribute;
import dk.nsi.hsuid.ResponsibleUserAuthorizationCodeAttribute;
import dk.nsi.hsuid.ResponsibleUserCivilRegistrationNumberAttribute;
import dk.nsi.hsuid.SystemNameAttribute;
import dk.nsi.hsuid.SystemVendorNameAttribute;
import dk.nsi.hsuid.SystemVersionAttribute;
import dk.nsi.hsuid.UserTypeAttribute;
import dk.nsi.hsuid._2016._08.hsuid_1_1.HsuidHeader;
import dk.nsi.hsuid._2016._08.hsuid_1_1.SubjectIdentifierType;
import dk.nsp.test.idp.model.Identity;

public abstract class AbstractSosiUser {

	public abstract Identity getIdentity();

	public HsuidHeader getHsuidHeader(String cpr, Boolean consentOverride) {
		List<Attribute> attributes = new ArrayList<Attribute>();
		attributes.add(new CitizenCivilRegistrationNumberAttribute(cpr));
		attributes.add(UserTypeAttribute.get(getUserType()));
		attributes.add(new ActingUserCivilRegistrationNumberAttribute(getUserCpr()));
		attributes.add(new OrganisationIdentifierAttribute(getSor(), SubjectIdentifierType.NSI_SORCODE.toString()));
		attributes.add(new SystemVendorNameAttribute(getSystemOwner()));
		attributes.add(new SystemNameAttribute(getSystemName()));
		attributes.add(new SystemVersionAttribute(getSystemVersion()));
		attributes.add(new OperationsOrganisationNameAttribute(getOrg()));
		if (consentOverride != null && consentOverride) {
			attributes.add(new ConsentOverrideAttribute(true));
		}
		attributes.add(new ResponsibleUserCivilRegistrationNumberAttribute(getUserCpr()));
		attributes.add(new ResponsibleUserAuthorizationCodeAttribute(getAuhorizationCode()));
		return HealthcareServiceUserIdentificationHeaderUtil.createHealthcareServiceUserIdentificationHeader(getIssuer(), attributes);
	}

	public abstract String getIssuer();

	public abstract String getAuhorizationCode();

	public abstract String getOrg();

	public abstract String getSystemVersion();

	public abstract String getSystemName();

	public abstract String getSystemOwner();

	public abstract String getSor();

	public abstract String getUserCpr();

	public abstract String getUserType();

}
