package dk.kvalitetsit.cdadocumentviewer.dgws;

import java.util.ArrayList;
import java.util.List;

import dk.nsp.test.idp.EmployeeIdentities;

public class MocesUserFactory {
	
	private List<MocesUser> mocesUsers = new ArrayList<MocesUser>();
	private MocesUser defaultMocesUser;

	public MocesUserFactory(String issuer, String orgname, String systemname, String systemversion, String systemowner, String usertype, List<String> activeMocesUsers) {
		
		defaultMocesUser = new MocesUser(EmployeeIdentities.lægeCharlesBabbage(), "0611809735", "Charles", "Babbage", "Læge", issuer, orgname, systemname, systemversion, systemowner, usertype);
		mocesUsers.add(defaultMocesUser);
		
		checkAndAdd(new MocesUser(EmployeeIdentities.sundhedsassistentKristenNygaard(), "1711809763", "Kristen", "Nygaard", "SundAssistR1", issuer, orgname, systemname, systemversion, systemowner, usertype), activeMocesUsers, "1");
		checkAndAdd(new MocesUser(EmployeeIdentities.sundhedsassistentEdsgerDijkstra(), "1411809893", "Edsger", "Dijkstra", "SundAssistR2", issuer, orgname, systemname, systemversion, systemowner, usertype), activeMocesUsers, "2");
		checkAndAdd(new MocesUser(EmployeeIdentities.sygeplejerskeGraceHopper(), "0411809250", "Grace", "Hopper", "Sygeplejerske", issuer, orgname, systemname, systemversion, systemowner, usertype), activeMocesUsers, "3");
		checkAndAdd(new MocesUser(EmployeeIdentities.peterNaur(), "1811804807", "Peter", "Naur", "Datalog", issuer, orgname, systemname, systemversion, systemowner, usertype), activeMocesUsers, "4");
	}
	
	private void checkAndAdd(MocesUser mocesUser, List<String> activeMocesUsers, String current) {
		if (activeMocesUsers.contains(current)) {
			mocesUsers.add(mocesUser);	
		}
	}
	
	public List<MocesUser>  getAllMocesUsers() {
		return  mocesUsers;
	}
	
	public MocesUser getDefaultMocesUser() {
		return defaultMocesUser;
	}

}
