package dk.kvalitetsit.cdadocumentviewer.dgws;

import dk.nsp.test.idp.model.EmployeeIdentity;
import dk.nsp.test.idp.model.Identity;

public class MocesUser extends AbstractSosiUser {
	
	private String issuer;
	private String orgname;
	private String systemname;
	private String systemversion;
	private String systemowner;
	private String usertype;
	
	private EmployeeIdentity identity;	
	private String usercpr;
	private String firstName;
	private String lastName;
	private String role;
	
	private String userauthorizationcode; 
	private String sorcode;

	public MocesUser(EmployeeIdentity identity, String cpr, String firstName, String lastName, String role, String issuer, String orgname, String systemname, String systemversion, String systemowner, String usertype) {
		this.identity = identity;
		this.usercpr = cpr;
		this.firstName = firstName;
		this.lastName = lastName;
		this.role = role;
		
		this.issuer = issuer;
		this.orgname = orgname;
		this.systemname = systemname;
		this.systemversion = systemversion;
		this.systemowner = systemowner;
		this.usertype = usertype;
		
		this.userauthorizationcode = (identity.getEmployee().getAuthorizationCode() != null) ? identity.getEmployee().getAuthorizationCode() : "-";
		this.sorcode = identity.getOrganization().getIdentifier();
	}
	
	public String getIdForList() {
		return role + " " + firstName + " " + lastName;
	}

	
	@Override
	public String getIssuer() {
		return issuer;
	}

	@Override
	public String getAuhorizationCode() {
		return userauthorizationcode;
	}

	@Override
	public String getOrg() {
		return orgname;
	}

	@Override
	public String getSystemVersion() {
		return systemversion;
	}

	@Override
	public String getSystemName() {
		return systemname;
	}

	@Override
	public String getSystemOwner() {
		return systemowner;
	}

	@Override
	public String getSor() {
		return sorcode;
	}

	@Override
	public String getUserCpr() {
		return usercpr;
	}

	@Override
	public String getUserType() {
		return usertype;
	}

	
	public String getLastName() {
		return lastName;
	}

	
	public String getFirstName() {
		return firstName;
	}
	@Override
	public Identity getIdentity() {
		return identity;
	}


	
}
