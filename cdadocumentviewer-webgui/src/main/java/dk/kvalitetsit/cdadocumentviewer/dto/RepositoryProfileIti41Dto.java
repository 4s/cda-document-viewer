package dk.kvalitetsit.cdadocumentviewer.dto;

import java.util.ArrayList;
import java.util.List;

import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;

public class RepositoryProfileIti41Dto {
	
	private String name;
	private String endpoint;
	private boolean dgws;
	private String endpointIti57;
	private boolean dgwsIti57;
	private String backendCode;
	private String repositoryuniqueid;
	private boolean uploadallowed;
	private boolean updateallowed;
	private boolean deprecateallowed;
	private Iti41PortType iti41PortType;
	private Iti57PortType iti57PortType;
	private List<String> validDocTypes;
	private String homeCommunityId;
	
	public RepositoryProfileIti41Dto() {
		validDocTypes = new ArrayList<>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public boolean isDgws() {
		return dgws;
	}
	public void setDgws(boolean dgws) {
		this.dgws = dgws;
	}
	public String getEndpointIti57() {
		return endpointIti57;
	}
	public void setEndpointIti57(String endpointIti57) {
		this.endpointIti57 = endpointIti57;
	}
	public boolean isDgwsIti57() {
		return dgwsIti57;
	}
	public void setDgwsIti57(boolean dgwsIti57) {
		this.dgwsIti57 = dgwsIti57;
	}
	public String getBackendCode() {
		return backendCode;
	}
	public void setBackendCode(String backendCode) {
		this.backendCode = backendCode;
	}
	public String getRepositoryuniqueid() {
		return repositoryuniqueid;
	}
	public void setRepositoryuniqueid(String repositoryuniqueid) {
		this.repositoryuniqueid = repositoryuniqueid;
	}
	public String getHomeCommunityId() {
		return homeCommunityId;
	}
	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}
	public boolean isUploadallowed() {
		return uploadallowed;
	}
	public void setUploadallowed(boolean uploadallowed) {
		this.uploadallowed = uploadallowed;
	}
	public boolean isUpdateallowed() {
		return updateallowed;
	}
	public void setUpdateallowed(boolean updateallowed) {
		this.updateallowed = updateallowed;
	}
	public boolean isdeprecateallowed() {
		return deprecateallowed;
	}
	public void setDeprecateallowed(boolean deprecateallowed) {
		this.deprecateallowed = deprecateallowed;
	}
	public Iti41PortType getIti41PortType() {
		return iti41PortType;
	}
	public void setIti41PortType(Iti41PortType iti41PortType) {
		this.iti41PortType = iti41PortType;
	}
	public Iti57PortType getIti57PortType() {
		return iti57PortType;
	}
	public void setIti57PortType(Iti57PortType iti57PortType) {
		this.iti57PortType = iti57PortType;
	}
	public List<String> getValidDocTypes() {
		return validDocTypes;
	}
	public void setValidDocTypes(List<String> validDocTypes) {
		this.validDocTypes = validDocTypes;
	}
	public void addDocType(String docType) {
		this.validDocTypes.add(docType);
	}
	public boolean isDocTypeValid(String docTypeToCheck) {
		if (validDocTypes.size() == 0) {
			return true;
		}
		for (String docType: this.validDocTypes) {
			if (docType.equals(docTypeToCheck)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString() {
		return "RepositoryProfileIti41Dto{" +
				"name='" + name + '\'' +
				", endpoint='" + endpoint + '\'' +
				", dgws=" + dgws +
				", endpointIti57='" + endpointIti57 + '\'' +
				", dgwsIti57=" + dgwsIti57 +
				", backendCode='" + backendCode + '\'' +
				", repositoryuniqueid='" + repositoryuniqueid + '\'' +
				", uploadallowed=" + uploadallowed +
				", updateallowed=" + updateallowed +
				", deprecateallowed=" + deprecateallowed +
				", iti41PortType=" + iti41PortType +
				", iti57PortType=" + iti57PortType +
				", validDocTypes=" + validDocTypes +
				", homeCommunityId='" + homeCommunityId + '\'' +
				'}';
	}
}
