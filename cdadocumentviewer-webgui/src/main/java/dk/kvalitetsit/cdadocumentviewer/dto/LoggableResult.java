package dk.kvalitetsit.cdadocumentviewer.dto;

public class LoggableResult<T> {

	private T result;
	
	private String requestId;
	
	private String responseId;
	
	public LoggableResult(T result, String requestId, String responseId) {
		this.result = result;
		this.requestId = requestId;
		this.responseId = responseId;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getResponseId() {
		return responseId;
	}

	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
}
