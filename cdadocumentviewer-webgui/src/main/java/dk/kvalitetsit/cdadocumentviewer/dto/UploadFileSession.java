package dk.kvalitetsit.cdadocumentviewer.dto;

public class UploadFileSession {

	private String replaceEntryUuid;
	private String replaceExternalDocumentId;
	private RepositoryProfileIti41Dto replaceRepository;
	private RepositoryProfileIti41Dto repository;

	
	public void reset() {
		this.replaceEntryUuid = null;
		this.replaceExternalDocumentId  = null;
		this.replaceRepository = null;
		this.repository = null;
		
	}
	
	public void setDocumentToReplace(String replaceEntryUuid, String replaceExternalDocumentId, RepositoryProfileIti41Dto replaceRepository) {
		this.replaceEntryUuid = replaceEntryUuid;
		this.replaceExternalDocumentId  = replaceExternalDocumentId;
		this.replaceRepository = replaceRepository;
		
	}
	
	public boolean isReplaceOperationActive() {
		return (replaceEntryUuid != null && replaceExternalDocumentId != null && replaceRepository != null);
	}
	public String getRepositoryValidDocTypesAsString() {
	if (repository != null) {
			StringBuilder sb = new StringBuilder(); 
	        for (String str : repository.getValidDocTypes()) {
	        	if (sb.length() > 0) {
	        		sb.append(", ");
	        	}
		        sb.append(str); 
		    }
	        return sb.toString(); 
		}
		return null;
	}

	
	public String getReplaceEntryUuid() {
		return replaceEntryUuid;
	}

	public void setReplaceEntryUuid(String replaceEntryUuid) {
		this.replaceEntryUuid = replaceEntryUuid;
	}

	public String getReplaceExternalDocumentId() {
		return replaceExternalDocumentId;
	}

	public void setReplaceExternalDocumentId(String replaceExternalDocumentId) {
		this.replaceExternalDocumentId = replaceExternalDocumentId;
	}

	public RepositoryProfileIti41Dto getReplaceRepository() {
		return replaceRepository;
	}

	public void setReplaceRepository(RepositoryProfileIti41Dto replaceRepository) {
		this.replaceRepository = replaceRepository;
	}

	public RepositoryProfileIti41Dto getRepository() {
		return repository;
	}

	public void setRepository(RepositoryProfileIti41Dto repository) {
		this.repository = repository;
	}

}
