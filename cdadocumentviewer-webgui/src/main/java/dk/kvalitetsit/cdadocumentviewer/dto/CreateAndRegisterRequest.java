package dk.kvalitetsit.cdadocumentviewer.dto;

public class CreateAndRegisterRequest<T extends CdaMetadata> {

	T documentMetadata;
	
	String document;

	public T getDocumentMetadata() {
		return documentMetadata;
	}

	public void setDocumentMetadata(T documentMetadata) {
		this.documentMetadata = documentMetadata;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}
}
