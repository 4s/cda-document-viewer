package dk.kvalitetsit.cdadocumentviewer.dto;

import org.openehealth.ipf.commons.ihe.xds.iti18.Iti18PortType;
import org.openehealth.ipf.commons.ihe.xds.iti43.Iti43PortType;

public class RegistryProfileIti18Dto {

	private String backendCode;
	private String name;
	private String endpoint;
	private boolean dgws;
	private String endpointIti43;
	private boolean dgwsIti43;
	private Iti18PortType iti18PortType;
	private Iti43PortType iti43PortType;

	private String typeCodeCodes;
	private String typeCodeNames;
	private String typeCodeSchemes;
	private String eventCodeSchemeCodes;
	private String eventCodeSchemeNames;
	private String formatCodeCodes;
	private String formatCodeNames;
	private String healthcareFacilityTypeCodeCodes;
	private String healthcareFacilityTypeCodeNames;
	private String practiceSettingCodeCodes;
	private String practiceSettingCodeNames;
	private boolean defaultTypeStable;
	private boolean defaultTypeOnDemand;



	public RegistryProfileIti18Dto() {

	}
	public String getBackendCode() {
		return backendCode;
	}
	public void setBackendCode(String backendCode) {
		this.backendCode = backendCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public boolean isDgws() {
		return dgws;
	}
	public void setDgws(boolean dgws) {
		this.dgws = dgws;
	}
	public String getEndpointIti43() {
		return endpointIti43;
	}
	public void setEndpointIti43(String endpointIti43) {
		this.endpointIti43 = endpointIti43;
	}
	public boolean isDgwsIti43() {
		return dgwsIti43;
	}
	public void setDgwsIti43(boolean dgwsIti43) {
		this.dgwsIti43 = dgwsIti43;
	}
	public Iti18PortType getIti18PortType() {
		return iti18PortType;
	}
	public void setIti18PortType(Iti18PortType iti18PortType) {
		this.iti18PortType = iti18PortType;
	}
	public Iti43PortType getIti43PortType() {
		return iti43PortType;
	}
	public void setIti43PortType(Iti43PortType iti43PortType) {
		this.iti43PortType = iti43PortType;
	}
	public String getTypeCodeCodes() {
		return typeCodeCodes;
	}
	public void setTypeCodeCodes(String typeCodeCodes) {
		this.typeCodeCodes = typeCodeCodes;
	}
	public String getTypeCodeNames() {
		return typeCodeNames;
	}
	public void setTypeCodeNames(String typeCodeNames) {
		this.typeCodeNames = typeCodeNames;
	}
	public String getTypeCodeSchemes() {
		return typeCodeSchemes;
	}
	public void setTypeCodeSchemes(String typeCodeSchemes) {
		this.typeCodeSchemes = typeCodeSchemes;
	}
	public String getEventCodeSchemeCodes() {
		return eventCodeSchemeCodes;
	}
	public void setEventCodeSchemeCodes(String eventCodeSchemeCodes) {
		this.eventCodeSchemeCodes = eventCodeSchemeCodes;
	}
	public String getEventCodeSchemeNames() {
		return eventCodeSchemeNames;
	}
	public void setEventCodeSchemeNames(String eventCodeSchemeNames) {
		this.eventCodeSchemeNames = eventCodeSchemeNames;
	}
	public String getFormatCodeCodes() {
		return formatCodeCodes;
	}

	public void setFormatCodeCodes(String formatCodeCodes) {
		this.formatCodeCodes = formatCodeCodes;
	}

	public String getFormatCodeNames() {
		return formatCodeNames;
	}

	public void setFormatCodeNames(String formatCodeNames) {
		this.formatCodeNames = formatCodeNames;
	}

	public String getHealthcareFacilityTypeCodeCodes() {
		return healthcareFacilityTypeCodeCodes;
	}

	public void setHealthcareFacilityTypeCodeCodes(String healthcareFacilityTypeCodeCodes) {
		this.healthcareFacilityTypeCodeCodes = healthcareFacilityTypeCodeCodes;
	}

	public String getHealthcareFacilityTypeCodeNames() {
		return healthcareFacilityTypeCodeNames;
	}

	public void setHealthcareFacilityTypeCodeNames(String healthcareFacilityTypeCodeNames) {
		this.healthcareFacilityTypeCodeNames = healthcareFacilityTypeCodeNames;
	}

	public String getPracticeSettingCodeCodes() {
		return practiceSettingCodeCodes;
	}

	public void setPracticeSettingCodeCodes(String practiceSettingCodeCodes) {
		this.practiceSettingCodeCodes = practiceSettingCodeCodes;
	}

	public String getPracticeSettingCodeNames() {
		return practiceSettingCodeNames;
	}

	public void setPracticeSettingCodeNames(String practiceSettingCodeNames) {
		this.practiceSettingCodeNames = practiceSettingCodeNames;
	}

	public boolean isDefaultTypeStable() {
		return defaultTypeStable;
	}

	public void setDefaultTypeStable(boolean defaultTypeStable) {
		this.defaultTypeStable = defaultTypeStable;
	}

	public boolean isDefaultTypeOnDemand() {
		return defaultTypeOnDemand;
	}
	public void setDefaultTypeOnDemand(boolean defaultTypeOnDemand) {
		this.defaultTypeOnDemand = defaultTypeOnDemand;
	}

	@Override
	public String toString() {
		return "RegistryProfileIti18Dto{" +
				"backendCode='" + backendCode + '\'' +
				", name='" + name + '\'' +
				", endpoint='" + endpoint + '\'' +
				", dgws=" + dgws +
				", endpointIti43='" + endpointIti43 + '\'' +
				", dgwsIti43=" + dgwsIti43 +
				", typeCodeCodes=" + typeCodeCodes +
				", typeCodeNames=" + typeCodeNames +
				", typeCodeSchemes=" + typeCodeSchemes +
				", eventCodeSchemeCodes=" + eventCodeSchemeCodes +
				", eventCodeSchemeNames=" + eventCodeSchemeNames +
				", formatCodeCodes=" + formatCodeCodes +
				", formatCodeNames=" + formatCodeNames +
				", healthcareFacilityTypeCodeCodes=" + healthcareFacilityTypeCodeCodes +
				", healthcareFacilityTypeCodeNames=" + healthcareFacilityTypeCodeNames +
				", practiceSettingCodeCodes=" + practiceSettingCodeCodes +
				", practiceSettingCodeNames=" + practiceSettingCodeNames +
				", defaultTypeStable=" + defaultTypeStable +
				", defaultTypeOnDemand=" + defaultTypeOnDemand +

				", iti18PortType=" + iti18PortType +
				", iti43PortType=" + iti43PortType +
				'}';
	}
}
