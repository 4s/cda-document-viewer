package dk.kvalitetsit.cdadocumentviewer.dto;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;

public class DocumentEntryTO {
	
	private static DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern("dd/MM/YYYY HH:mm");

	private DocumentEntry de;
	private boolean allowUpdate;
	private boolean allowDeprecate;
	private String documentEntryJsonFormatBase64Encoded;
	private String backendCode;
	
	public DocumentEntryTO(DocumentEntry de, boolean itiAllowUpdate, boolean itiAllowDeprecate, String documentEntryJsonFormatBase64Encoded, String backendCode) {
		this.de = de;
		if ((itiAllowUpdate) &&
			(de.getAvailabilityStatus().equals(AvailabilityStatus.APPROVED))) {
			this.allowUpdate = true;
		} else {
			this.allowUpdate = false;
		}

		if ((itiAllowDeprecate) &&
			(de.getAvailabilityStatus().equals(AvailabilityStatus.APPROVED))) {
			this.allowDeprecate = true;
		} else {
			this.allowDeprecate = false;
		}
		this.documentEntryJsonFormatBase64Encoded = documentEntryJsonFormatBase64Encoded;
		this.backendCode = backendCode;

	}
	
	public String getUniqueId() {
		return de.getUniqueId();
	}
	
	public String getEntryUuid() {
		return de.getEntryUuid();
	}
	
	public String getRepositoryUniqueId() {
		return de.getRepositoryUniqueId();
	}
	
	public String getDocumentType() {
		return (de.getTypeCode() != null ? (de.getTypeCode().getDisplayName() != null ? de.getTypeCode().getDisplayName().getValue() : "") : "");
	}
	
	public String getServiceStartTime() {
		if (de != null && de.getServiceStartTime() != null) {
			return dateTimeFormat.print(de.getServiceStartTime());
		} else {
			return "";
		}
	}
	
	public String getServiceStopTime() {
		if (de != null && de.getServiceStopTime() != null) {
			return dateTimeFormat.print(de.getServiceStopTime());
		} else {
			return "";
		}		
	}
	
	public String getStartSort() {		
	if (de != null && de.getServiceStartTime() != null) {
		return ""+de.getServiceStartTime().getMillis();	
	} else {
		return "0";
	}	
	}
	
	public String getStopSort() {
		if (de != null && de.getServiceStopTime() != null) {
			return ""+de.getServiceStopTime().getMillis();	
		} else {
			return "0";
		}		
	}
	
	public String getPatientId() {
		return de.getPatientId().getId();	
	}
	
	public String getHomeCommunityId() {
		return de.getHomeCommunityId();
	}

	public boolean isAllowUpdate() {
		return allowUpdate;
	}

	public boolean isAllowDeprecate() {
		return allowDeprecate;
	}
	
	public String getDocumentEntryJsonFormatBase64Encoded() {
		return documentEntryJsonFormatBase64Encoded;
	}

	public String getBackendCode() {
		return backendCode;
	}
	
}
