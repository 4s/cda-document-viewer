package dk.kvalitetsit.cdadocumentviewer.dto;

import org.springframework.web.multipart.MultipartFile;

public class UploadKeystoreRequest {

	private MultipartFile keystore;
	
	private String alias;
	
	private String keystorePassword;
	
	private String keyPassword;

	public MultipartFile getKeystore() {
		return keystore;
	}

	public void setKeystore(MultipartFile keystore) {
		this.keystore = keystore;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}

	public String getKeyPassword() {
		return keyPassword;
	}

	public void setKeyPassword(String keyPassword) {
		this.keyPassword = keyPassword;
	}
}
