package dk.kvalitetsit.cdadocumentviewer.dto;

public class SearchProfileDto {

	public String id;
	
	public String name;

	public SearchProfileDto() {
		
	}
	
	public SearchProfileDto(String id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
