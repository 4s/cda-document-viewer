package dk.kvalitetsit.cdadocumentviewer.dto;

import java.util.Date;

import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CdaMetadata {

	public AvailabilityStatus availabilityStatus;
	public Code classCode;
	public Code formatCode;
	public Code healthcareFacilityTypeCode;
	public DocumentEntryType objectType;
	public Code practiceSettingCode;
	@JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ssZ")
	public Date submissionTime;
	public String homeCommunityId;

	public CdaMetadata(){}
	
	public Code getClassCode() {
		return classCode;
	}

	public void setClassCode(Code classCode) {
		this.classCode = classCode;
	}

	public Code getFormatCode() {
		return formatCode;
	}

	public void setFormatCode(Code formatCode) {
		this.formatCode = formatCode;
	}

	public Code getHealthcareFacilityTypeCode() {
		return healthcareFacilityTypeCode;
	}

	public void setHealthcareFacilityTypeCode(Code healthcareFacilityTypeCode) {
		this.healthcareFacilityTypeCode = healthcareFacilityTypeCode;
	}

	public Date getSubmissionTime() {
		return submissionTime;
	}

	public void setSubmissionTime(Date submissionTime) {
		this.submissionTime = submissionTime;
	}
	
	public Code getPracticeSettingCode() {
		return practiceSettingCode;
	}

	public void setPracticeSettingCode(Code practiceSettingCode) {
		this.practiceSettingCode = practiceSettingCode;
	}

	public AvailabilityStatus getAvailabilityStatus() {
		return availabilityStatus;
	}

	public void setAvailabilityStatus(AvailabilityStatus availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public DocumentEntryType getObjectType() {
		return objectType;
	}

	public void setObjectType(DocumentEntryType objectType) {
		this.objectType = objectType;
	}
	
	public String getHomeCommunityId() {
		return homeCommunityId;
	}

	public void setHomeCommunityId(String homeCommunityId) {
		this.homeCommunityId = homeCommunityId;
	}

	@Override
	public String toString() {
		return "CdaMetadata [availabilityStatus=" + availabilityStatus + ", classCode=" + classCode + ", formatCode="
				+ formatCode + ", healthcareFacilityTypeCode=" + healthcareFacilityTypeCode + ", objectType="
				+ objectType + ", practiceSettingCode=" + practiceSettingCode + ", submissionTime=" + submissionTime + "]";
	}
	
	
}
