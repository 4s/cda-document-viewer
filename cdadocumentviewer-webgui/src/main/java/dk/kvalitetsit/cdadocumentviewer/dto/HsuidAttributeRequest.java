package dk.kvalitetsit.cdadocumentviewer.dto;


public class HsuidAttributeRequest {

	private String userCpr;
	
	private String firstName;
	
	private String lastName;
	
	private String orgName;
	
	private String cvrNumber;

	private String itSystem;

	private String sor;

	private String systemVersion;
	
	private String systemOwner;

	private String systemName;

	private String authorizationCode;

	private String issuer;		

	public HsuidAttributeRequest() {
	}
	
	public HsuidAttributeRequest(String itsystem, String systemversion, String systemname) {
		this.itSystem = itsystem;
		this.systemVersion = systemversion;
		this.systemName = systemname;
	}


	public String getUserCpr() {
		return userCpr;
	}


	public void setUserCpr(String userCpr) {
		this.userCpr = userCpr;
	}

	public String getSystemOwner() {
		return systemOwner;
	}


	public void setSystemOwner(String systemOwner) {
		this.systemOwner = systemOwner;
	}


	public String getSystemName() {
		return systemName;
	}


	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}


	public String getOrgName() {
		return orgName;
	}


	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}


	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getCvrNumber() {
		return cvrNumber;
	}


	public void setCvrNumber(String cvrNumber) {
		this.cvrNumber = cvrNumber;
	}


	public String getItSystem() {
		return itSystem;
	}


	public void setItSystem(String itSystem) {
		this.itSystem = itSystem;
	}


	public String getSor() {
		return sor;
	}


	public void setSor(String sor) {
		this.sor = sor;
	}


	public String getAuthorizationCode() {
		return authorizationCode;
	}


	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}	
}
