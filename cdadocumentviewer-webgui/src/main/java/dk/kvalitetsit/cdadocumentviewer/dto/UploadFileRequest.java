package dk.kvalitetsit.cdadocumentviewer.dto;

import org.springframework.web.multipart.MultipartFile;

public class UploadFileRequest {
	
	private MultipartFile cdaFile ;

	private String repositoryEndpoint;
	
	private String repositoryName;

	public MultipartFile getCdaFile() {
		return cdaFile;
	}

	public void setCdaFile(MultipartFile cdaFile) {
		this.cdaFile = cdaFile;
	}

	public String getRepositoryEndpoint() {
		return repositoryEndpoint;
	}

	public void setRepositoryEndpoint(String repositoryEndpoint) {
		this.repositoryEndpoint = repositoryEndpoint;
	}
	
	public String getRepositoryName() {
		return repositoryName;
	}

	public void setRepositoryName(String repositoryName) {
		this.repositoryName = repositoryName;
	}

	
}
