package dk.kvalitetsit.cdadocumentviewer.dto;

public class DropDownListDto {

	public String code;
	
	public String name;

	public String scheme;

	public DropDownListDto() {
		
	}
	
	public DropDownListDto(String code, String name, String scheme) {
		this.code = code;
		this.name = name;
		this.scheme = scheme;
	}
	
	public String getCode() {
		return code;
	}

	public void setId(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	@Override
	public String toString() {
		return "DropDownListDto{" +
				"code='" + code + '\'' +
				", name='" + name + '\'' +
				", scheme='" + scheme + '\'' +
				'}';
	}
}
