package dk.kvalitetsit.cdadocumentviewer.dto;

public class SearchRequest {
	
	private String patientID;
	private String uniqueID;
	private String typeCode;
	private String typeCodeScheme;
	private String formatCode;
	private String eventCode;
	private String eventCodeScheme;
	private String healthcareFacilityTypeCode;
	private String practicesettingCode;
	private String serviceStartFromLow;
	private String serviceStartFromHigh;
	private String serviceStopToLow;
	private String serviceStopToHigh;
	private String status;
	private boolean typeStable;
	private String typeStableDefaultValue;
	private boolean typeOnDemand;
	private String typeOnDemandDefaultValue;
	private boolean vaerdispring;
	private String profile;
	private String backendCode;
	private RegistryProfileIti18Dto registryProfileIti18Dto;
	
	public boolean filterUniqueID() {
		if (getUniqueID() != null && !getUniqueID().isEmpty()) {
			return true;
		}
		return false;
	}
	
	public String getPatientID() {
		return patientID;
	}
	
	public void setPatientID(String patientID) {
		this.patientID = patientID;
	}
	
	public String getUniqueID() {
		return uniqueID;
	}
	
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}
	
	public String getTypeCode() {
		return typeCode;
	}
	
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTypeCodeScheme() {
		return typeCodeScheme;
	}

	public void setTypeCodeScheme(String typeCodeScheme) {
		this.typeCodeScheme = typeCodeScheme;
	}

	public String getFormatCode() {
		return formatCode;
	}
	
	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}
	
	public String getEventCode() {
		return eventCode;
	}
	
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}
	
	public String getEventCodeScheme() {
		return eventCodeScheme;
	}
	
	public void setEventCodeScheme(String eventCodeScheme) {
		this.eventCodeScheme = eventCodeScheme;
	}

	public String getHealthcareFacilityTypeCode() {
		return healthcareFacilityTypeCode;
	}
	
	public void setHealthcareFacilityTypeCode(String healthcareFacilityTypeCode) {
		this.healthcareFacilityTypeCode = healthcareFacilityTypeCode;
	}
	
	public String getPracticesettingCode() {
		return practicesettingCode;
	}
	
	public void setPracticesettingCode(String practicesettingCode) {
		this.practicesettingCode = practicesettingCode;
	}

	public String getServiceStartFromLow() {
		return serviceStartFromLow;
	}

	public void setServiceStartFromLow(String serviceStartFromLow) {
		this.serviceStartFromLow = serviceStartFromLow;
	}

	public String getServiceStartFromHigh() {
		return serviceStartFromHigh;
	}

	public void setServiceStartFromHigh(String serviceStartFromHigh) {
		this.serviceStartFromHigh = serviceStartFromHigh;
	}

	public String getServiceStopToLow() {
		return serviceStopToLow;
	}

	public void setServiceStopToLow(String serviceStopToLow) {
		this.serviceStopToLow = serviceStopToLow;
	}

	public String getServiceStopToHigh() {
		return serviceStopToHigh;
	}

	public void setServiceStopToHigh(String serviceStopToHigh) {
		this.serviceStopToHigh = serviceStopToHigh;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isTypeStable() {
		return typeStable;
	}

	public void setTypeStable(boolean typeStable) {
		this.typeStable = typeStable;
	}

	public boolean isTypeOnDemand() {
		return typeOnDemand;
	}

	public void setTypeOnDemand(boolean typeOnDemand) {
		this.typeOnDemand = typeOnDemand;
	}

	public String getTypeStableDefaultValue() {
		return typeStableDefaultValue;
	}

	public void setTypeStableDefaultValue(String typeStableDefaultValue) {
		this.typeStableDefaultValue = typeStableDefaultValue;
	}

	public String getTypeOnDemandDefaultValue() {
		return typeOnDemandDefaultValue;
	}

	public void setTypeOnDemandDefaultValue(String typeOnDemandDefaultValue) {
		this.typeOnDemandDefaultValue = typeOnDemandDefaultValue;
	}

	public boolean isVaerdispring() {
		return vaerdispring;
	}
	public void setVaerdispring(boolean vaerdispring) {
		this.vaerdispring = vaerdispring;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getBackendCode() {
		return backendCode;
	}

	public void setBackendCode(String backendCode) {
		this.backendCode = backendCode;
	}

	public RegistryProfileIti18Dto getRegistryProfileIti18Dto() {
		return registryProfileIti18Dto;
	}

	public void setRegistryProfileIti18Dto(RegistryProfileIti18Dto registryProfileIti18Dto) {
		this.registryProfileIti18Dto = registryProfileIti18Dto;
	}
}
