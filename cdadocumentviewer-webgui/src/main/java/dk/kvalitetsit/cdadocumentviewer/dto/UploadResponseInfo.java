package dk.kvalitetsit.cdadocumentviewer.dto;

import java.util.ArrayList;
import java.util.List;
import org.openehealth.ipf.commons.ihe.xds.core.responses.Status;

public class UploadResponseInfo {

	List<String> errorList;
	Status status;
	
	String patientId;
	String externalDocumentId;
	String backendCodeToSearchDocument;

	public boolean allOk( ) {
		//ITI41 suppoprterer kun success og Failure: https://profiles.ihe.net/ITI/TF/Volume3/ch-4.2.html#4.2.4.2
		if (status != Status.SUCCESS) { 
			return false;
		}
		return true;
	}

	public UploadResponseInfo(String patientId, Status status) {
		this.patientId = patientId;
		this.status = status;
		errorList = new ArrayList<>();
	}
	
	public void addError(String error) {
		errorList.add(error);
	}
	
	public String getExternalDocumentId() {
		return externalDocumentId;
	}

	public void setExternalDocumentId(String externalDocumentId) {
		this.externalDocumentId = externalDocumentId;
	}


	
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}

	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}


	public String getBackendCodeToSearchDocument() {
		return backendCodeToSearchDocument;
	}

	public void setBackendCodeToSearchDocument(String backendCodeToSearchDocument) {
		this.backendCodeToSearchDocument = backendCodeToSearchDocument;
	}
}
