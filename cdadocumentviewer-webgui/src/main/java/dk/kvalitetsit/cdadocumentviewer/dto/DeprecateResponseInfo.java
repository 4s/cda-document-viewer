package dk.kvalitetsit.cdadocumentviewer.dto;

import java.util.ArrayList;
import java.util.List;

public class DeprecateResponseInfo {

	List<String> errorList;

	public boolean allOk( ) {
		if (errorList != null && errorList.size() > 0 ) {
			return false;
		}
		return true;
	}

	public DeprecateResponseInfo() {
		errorList = new ArrayList<>();
	}
	
	public void addError(String error) {
		errorList.add(error);
	}
	
	public void addErrorAll(List<String> errors) {
		errorList.addAll(errors);
	}

	public List<String> getErrorList() {
		return errorList;
	}

	public void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}
	
}
