package dk.kvalitetsit.cdadocumentviewer.dto;

import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;

public class MetadataInput {
	private AvailabilityStatus availabilityStatus;
	private String classCode;
	private String formatCode;
	private String healthcareFacilityTypeCode;
	private DocumentEntryType objectType;
	private String practiceSettingCode;
	private String submissionTime;
	
	public String getClassCode() {
		return classCode;
	}
	
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	
	public String getFormatCode() {
		return formatCode;
	}
	
	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}
	
	public String getHealthcareFacilityTypeCode() {
		return healthcareFacilityTypeCode;
	}
	
	public void setHealthcareFacilityTypeCode(String healthcareFacilityTypeCode) {
		this.healthcareFacilityTypeCode = healthcareFacilityTypeCode;
	}
	
	public String getSubmissionTime() {
		return submissionTime;
	}
	
	public void setSubmissionTime(String submissionTime) {
		this.submissionTime = submissionTime;
	}
	
	public String getPracticeSettingCode() {
		return practiceSettingCode;
	}
	
	public void setPracticeSettingCode(String practiceSettingCode) {
		this.practiceSettingCode = practiceSettingCode;
	}

	public AvailabilityStatus getAvailabilityStatus() {
		return availabilityStatus;
	}

	public void setAvailabilityStatus(AvailabilityStatus availabilityStatus) {
		this.availabilityStatus = availabilityStatus;
	}

	public DocumentEntryType getObjectType() {
		return objectType;
	}

	public void setObjectType(DocumentEntryType objectType) {
		this.objectType = objectType;
	}

	@Override
	public String toString() {
		return "MetadataInput [availabilityStatus=" + availabilityStatus + ", classCode=" + classCode + ", formatCode="
				+ formatCode + ", healthcareFacilityTypeCode=" + healthcareFacilityTypeCode + ", objectType="
				+ objectType + ", practiceSettingCode=" + practiceSettingCode
				+ ", submissionTime=" + submissionTime + "]";
	}
	
	
	
}
