package dk.kvalitetsit.cdadocumentviewer.xds;


public class OrganisationIdAuthority implements IdAuthority {
	
	private static final String PATIENT_ID_PATTERN = "%s^^^&%s&ISO";

	private String organisationIdAuthority;

	public OrganisationIdAuthority(String organisationIdAuthority) {
		this.organisationIdAuthority = organisationIdAuthority;
	}
	
	@Override
	public String getPatientIdAuthority() {
		return Codes.DK_CPR_CLASSIFICAION_OID;
	}
	
	public String formatPatientIdentifier(String patientIdentifier) {
		return String.format(PATIENT_ID_PATTERN, patientIdentifier, Codes.DK_CPR_CLASSIFICAION_OID);
	}

}
