package dk.kvalitetsit.cdadocumentviewer.xds;

public interface IdAuthority {

	String getPatientIdAuthority();
	
}
