package dk.kvalitetsit.cdadocumentviewer.cxf;

import java.util.UUID;

import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.log.LogEntry;
import dk.kvalitetsit.cdadocumentviewer.log.LogEntryService;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfile;

public class OutRequestInterceptor extends LoggingOutInterceptor {

	@Autowired
	SearchProfile searchProfile;

	@Autowired
	LogEntryService logEntryService;

    public OutRequestInterceptor() {
        super(-1); // Ingen øvre grænse for størrelsen
    }

    @Override
    protected String formatLoggingMessage(LoggingMessage loggingMessage) {
        String soapXmlPayload = super.formatLoggingMessage(loggingMessage);
        
        String uuid = UUID.randomUUID().toString();
        LogEntry le = logEntryService.saveLog(uuid, soapXmlPayload);
        searchProfile.setRequestId(le.getId());
        
        return "";
    }

}
