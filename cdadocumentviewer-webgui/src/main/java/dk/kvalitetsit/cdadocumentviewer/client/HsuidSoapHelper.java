package dk.kvalitetsit.cdadocumentviewer.client;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import dk.kvalitetsit.cdadocumentviewer.dgws.AbstractSosiUser;
import dk.nsi.hsuid._2016._08.hsuid_1_1.HsuidHeader;

public class HsuidSoapHelper {
	
	public String getHsuidHeaderPerson(String cpr, boolean vaerdispring, AbstractSosiUser user) {
		HsuidHeader hsUidHeader =  getHeaderPerson(cpr, vaerdispring, user);
		try {
			String header = toXml(hsUidHeader);
			return header;
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}
	
	private String toXml(HsuidHeader hsUidHeader) throws JAXBException {
		final StringWriter writer = new StringWriter();
		JAXBContext.newInstance(hsUidHeader.getClass()).createMarshaller().marshal(hsUidHeader, writer);
		return writer.toString();
	}
	
	private HsuidHeader getHeaderPerson(String cpr, boolean vaerdispring, AbstractSosiUser user) {
		
		HsuidHeader hsuidHeader = user.getHsuidHeader(cpr, vaerdispring);
		
		return hsuidHeader;

	}
	

}
