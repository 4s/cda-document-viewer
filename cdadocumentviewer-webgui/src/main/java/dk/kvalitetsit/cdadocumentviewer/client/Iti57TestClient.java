package dk.kvalitetsit.cdadocumentviewer.client;

import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.lcm.SubmitObjectsRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryResponseType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;
import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.dgws.DefaultFocesUser;

public class Iti57TestClient extends AbstractTestClient<SubmitObjectsRequest, RegistryResponseType> implements Iti57PortType { 

	private String soapAction = "urn:ihe:iti:2010:UpdateDocumentSet";
	
	@Autowired
	DefaultFocesUser defaultFocesUser;

	
	public Iti57TestClient(String endpoint) {
		super(endpoint);
	}

	@Override
	public RegistryResponseType documentRegistryUpdateDocumentSet(SubmitObjectsRequest body) {
		return callService(body, soapAction, RegistryResponseType.class, defaultFocesUser.getIdentity(), null);
	}
}
