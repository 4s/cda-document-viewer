package dk.kvalitetsit.cdadocumentviewer.client;

import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.ProvideAndRegisterDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryResponseType;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.dgws.DefaultFocesUser;

public class Iti41TestClient extends AbstractTestClient<ProvideAndRegisterDocumentSetRequestType, RegistryResponseType> implements Iti41PortType {
	
	private String soapAction = "urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b";
	
	@Autowired
	DefaultFocesUser defaultFocesUser;

	public Iti41TestClient(String endpoint) {
		super(endpoint);
	}

	@Override
	public RegistryResponseType documentRepositoryProvideAndRegisterDocumentSetB(ProvideAndRegisterDocumentSetRequestType body) {

		return callService(body, soapAction, RegistryResponseType.class, defaultFocesUser.getIdentity(), null);

	}
	
}
