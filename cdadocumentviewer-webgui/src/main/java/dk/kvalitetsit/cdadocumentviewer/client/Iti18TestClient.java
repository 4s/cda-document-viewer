package dk.kvalitetsit.cdadocumentviewer.client;

import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.query.AdhocQueryRequest;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.query.AdhocQueryResponse;
import org.openehealth.ipf.commons.ihe.xds.iti18.Iti18PortType;
import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.dgws.UserContextForQuery;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfile;

public class Iti18TestClient extends AbstractTestClient<AdhocQueryRequest, AdhocQueryResponse> implements Iti18PortType {

	private String soapAction = "urn:ihe:iti:2007:RegistryStoredQuery";
	
	@Autowired
	protected SearchProfile searchProfile;

	
	public Iti18TestClient(String endpoint) {
		super(endpoint);
	}

	@Override
	public AdhocQueryResponse documentRegistryRegistryStoredQuery(AdhocQueryRequest body) {
		
		String patientCpr = UserContextForQuery.patientCpr.get();
		Boolean consentOverride = UserContextForQuery.consentOverride.get();
		UserContextForQuery.patientCpr.remove();
		UserContextForQuery.consentOverride.remove();
		
		String header = hsuidSoapHelper.getHsuidHeaderPerson(patientCpr, consentOverride, searchProfile.getCurrent());
		return callService(body, soapAction, AdhocQueryResponse.class, searchProfile.getCurrent().getIdentity(), header);

	}

}
