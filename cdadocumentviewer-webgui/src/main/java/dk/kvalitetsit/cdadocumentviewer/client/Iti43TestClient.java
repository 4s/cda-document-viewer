package dk.kvalitetsit.cdadocumentviewer.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.xml.bind.JAXBException;
import javax.xml.soap.SOAPException;

import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.RetrieveDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.RetrieveDocumentSetResponseType;
import org.openehealth.ipf.commons.ihe.xds.iti43.Iti43PortType;
import org.springframework.beans.factory.annotation.Autowired;

import dk.kvalitetsit.cdadocumentviewer.dgws.UserContextForQuery;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfile;

public class Iti43TestClient extends AbstractTestClient<RetrieveDocumentSetRequestType, RetrieveDocumentSetResponseType> implements Iti43PortType {

	private String soapAction = "urn:ihe:iti:2007:RetrieveDocumentSet";
	
	public Iti43TestClient(String endpoint) {
		super(endpoint);
	}
	
	@Autowired
	protected SearchProfile searchProfile;
	
	@Override
	public RetrieveDocumentSetResponseType documentRepositoryRetrieveDocumentSet(RetrieveDocumentSetRequestType body) {
		String patientCpr = UserContextForQuery.patientCpr.get();
		Boolean consentOverride = UserContextForQuery.consentOverride.get();
		UserContextForQuery.patientCpr.remove();
		UserContextForQuery.consentOverride.remove();
		
		String header = hsuidSoapHelper.getHsuidHeaderPerson(patientCpr, consentOverride, searchProfile.getCurrent());
		return callService(body, soapAction, RetrieveDocumentSetResponseType.class, searchProfile.getCurrent().getIdentity(), header);
		
	}
	
	@Override
	public RetrieveDocumentSetResponseType parseNspSoapResponse(byte[] response, Class<RetrieveDocumentSetResponseType> c) throws SOAPException, IOException, JAXBException {
		
		// prøv først at se om der er attachments. Hvis ikke prøv med almindelig parse
		List<String> responseParts = fetchResponseParts(response);
		if (responseParts.size() < 1) {
			try {	
				return super.parseNspSoapResponse(response, c);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		// metadata (envelope) findes i første del
		String responseXmlMetaData = responseParts.get(0);
		InputStream xmlStreamDocument = new ByteArrayInputStream(responseXmlMetaData.getBytes());
		RetrieveDocumentSetResponseType retrieveDocumentSetResponseType = parseNspSoapResponse(xmlStreamDocument, c);
		
		includeResponsePartsToDocumentEntry(retrieveDocumentSetResponseType, responseParts);

		return retrieveDocumentSetResponseType;

	}
	
	public void includeResponsePartsToDocumentEntry (RetrieveDocumentSetResponseType retrieveDocumentSetResponseType, List<String> responseParts) {
		
		// vi forventer ikke blandede svar med både attachment og indlejrede dokumenter. (idws ser ud til at være indlejret, da andre attached)
		// er det attached dokumenter, forventer vi alle dokumenter og i korrekt rækkefølge i de efterølgende dele.

		// hvis der mindre end 2 dele er der ingen attachede dokumenter, så stop inkluderingen.
		if (responseParts.size() < 2) {
			return;
		}
		
		for (int i = 0; i<retrieveDocumentSetResponseType.getDocumentResponse().size(); i++) {
			if (responseParts.size() > i+1) {
				String responseXmlDocument = responseParts.get(i+1);
				String mime = retrieveDocumentSetResponseType.getDocumentResponse().get(i).getMimeType();
				DataHandler datahandler = new DataHandler(new ByteArrayDataSource(responseXmlDocument.getBytes(),  mime));
				retrieveDocumentSetResponseType.getDocumentResponse().get(i).setDocument(datahandler);
			}
		}

	}

	public List<String>  fetchResponseParts(byte[] isMtm) {
		List<String> attachments = new ArrayList<String>();
		try {

			ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(isMtm, "text/xml"); 

			MimeMultipart mp = new MimeMultipart(byteArrayDataSource);
			int count = mp.getCount();
			for (int i = 0; i < count; i++) {
				BodyPart bodyPart = mp.getBodyPart(i);
				String attachment = null;
				try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
					bodyPart.writeTo(baos);
					attachment = new String(baos.toString());
				}
				
				if (attachment != null) {
					// fjern alle headers og blanke linier, da de ikke kan parses	            
					Enumeration<Header> headerList = bodyPart.getAllHeaders();
					while(headerList.hasMoreElements()) {
						String headerName = headerList.nextElement().getName();
						attachment = clearLineStartingWith(attachment, headerName);
					}
					attachment = removeBlankLines(attachment);
				}
				attachments.add(attachment);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		return attachments;
	}


	private String clearLineStartingWith(String input, String removeLineStart) {
		String text = input.replaceAll("(?m)^"+removeLineStart+".*", "");
		return text;
	}

	private String removeBlankLines(String input) {
		String text = input.trim();
//		String text = input.replaceAll("(?m)^[ \t]*\r?\n", "");
		return text;
	}

	
}
