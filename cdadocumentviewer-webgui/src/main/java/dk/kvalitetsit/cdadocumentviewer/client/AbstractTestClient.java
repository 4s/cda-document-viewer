package dk.kvalitetsit.cdadocumentviewer.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Document;

import dk.kvalitetsit.cdadocumentviewer.log.LogEntry;
import dk.kvalitetsit.cdadocumentviewer.log.LogEntryService;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfile;
import dk.nsp.test.idp.NspSoapClient;
import dk.nsp.test.idp.NspSoapClient.NspSoapResponse;
import dk.nsp.test.idp.model.Identity;


public abstract class AbstractTestClient<S, T>  {

	@Autowired
	SearchProfile searchProfile;
	
	@Autowired
	LogEntryService logEntryService;
	
	private String endpoint;
	protected String validStartEnvelope = "<soap:Envelope";
	protected String validEndEnvelope = "</soap:Envelope>";
	
	private String validStartFaultString = "<faultstring>";
	private String validEndFaultString = "</faultstring>";
	
	private String validStartFaultCode = "<faultcode>";
	private String validEndFaultCode = "</faultcode>";
	
	protected HsuidSoapHelper hsuidSoapHelper = new HsuidSoapHelper();

	public AbstractTestClient(String endpoint) {
		this.endpoint = endpoint;
	}
	public AbstractTestClient(String endpoint, SearchProfile searchProfile, LogEntryService logEntryService) {
		this.endpoint = endpoint;
		this.searchProfile = searchProfile;
		this.logEntryService = logEntryService;
	}

	public T callService(S body, String soapAction, Class<T> c, Identity identity, String header) {

		try {
			String bodyAsString = unparseNspSoapRequest(body);
			
			NspSoapResponse response = executeNspRequest(new URI(endpoint), soapAction, identity, bodyAsString, header);
			if (response.isFault()) {
				throw new SOAPFaultException(parseNspSoapfault(response));
			}
			byte[] resultAsByte = responseToByteArray(response);
			return parseNspSoapResponse(resultAsByte, c);

		} catch (SOAPFaultException e) {
			throw e;

		} catch (Exception e) {
			throw new RuntimeException(e);

		}
	}
	
	public String unparseNspSoapRequest(S v) throws JAXBException {
        final ByteArrayOutputStream writer = new ByteArrayOutputStream();
		JAXBContext.newInstance(v.getClass()).createMarshaller().marshal(v, writer);
		return new String(writer.toByteArray(), StandardCharsets.UTF_8);
	}
	
	public NspSoapResponse executeNspRequest(URI uri, String soapAction, Identity identity, String req, String header) throws Exception {
		Logger log = Logger.getLogger(getClass());
		
		List<String> loggedItems = new ArrayList<String>();
		
		Consumer<String> nspSoapLogger = message -> {
			log.info(message);
			loggedItems.add(message);
		};
		try(NspSoapClient c = new NspSoapClient(nspSoapLogger)) {
			NspSoapClient.NspSoapRequest nspSoapRequest = (identity != null) ? 
					c.request(uri, soapAction).as(identity) : c.request(uri, soapAction).withoutSecurity();   
			
			if (header != null) {
				try(NspSoapClient.NspSoapResponse res = nspSoapRequest.execute(req, header)) { 
					return res; }				
			} else {
				try(NspSoapClient.NspSoapResponse res = nspSoapRequest.execute(req)) { 
					return res; }
			}

		} finally {
			
			if (loggedItems.size() > 2) {
				searchProfile.setRequestId(logIt(loggedItems.get(2)));//request for iti kald
			}
			if (loggedItems.size() > 3) {
				searchProfile.setResponseId(logIt(loggedItems.get(3)));//response for iti kald
			}
		
		}
	}
	
	private String logIt(String soapXmlPayload) {
		
        String uuid = UUID.randomUUID().toString();
        LogEntry le = logEntryService.saveLog(uuid, soapXmlPayload);
        return le.getId();
        
		
	}
	
	public SOAPFault parseNspSoapfault(NspSoapClient.NspSoapResponse response) throws SOAPException, IOException, JAXBException {
		String responseXml = IOUtils.toString(response.getResponse(), StandardCharsets.UTF_8);
		String faultString =  StringUtils.substringBetween(responseXml, validStartFaultString, validEndFaultString);
		String faultCode =  StringUtils.substringBetween(responseXml, validStartFaultCode, validEndFaultCode);
		SOAPFault soapFault = SOAPFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL).createFault(faultString, new QName("nameSpaceURI", faultCode));
		return soapFault;
	}
	
	private byte[] responseToByteArray(NspSoapClient.NspSoapResponse response) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte[] buffer = new byte[1024];
		int len;
		while ((len = response.getResponse().read(buffer)) > -1 ) {
		    baos.write(buffer, 0, len);
		}
		baos.flush();
		
		return baos.toByteArray();

	}
	
	public T parseNspSoapResponse(byte[] response, Class<T> c) throws SOAPException, IOException, JAXBException {
		String responseXml = new String(response, StandardCharsets.UTF_8);
		InputStream responseAdjusted = new ByteArrayInputStream(adjustResponse(responseXml, validStartEnvelope, validEndEnvelope).getBytes());
		return parseNspSoapResponse(responseAdjusted, c);
	}
	
	public T parseNspSoapResponse(InputStream inputStream, Class<T> c) throws SOAPException, IOException, JAXBException {
		final Document message = MessageFactory
				.newInstance()
				.createMessage(null, inputStream)
				.getSOAPBody()
				.extractContentAsDocument();
		return JAXBContext.newInstance(c)
				.createUnmarshaller()
				.unmarshal(message, c)
				.getValue();
	}

	public String adjustResponse(String xml, String startTag, String endTag) throws IOException {

		//kald til repository inkluderer mere end Envelope med i svaret. Blandt andet content type etc. Derfor denne justering
		if (!xml.startsWith(startTag)) {
			int envelopeStartIdx = xml.indexOf(startTag);
			int envelopeEndIdx = xml.indexOf(endTag);
			if (envelopeStartIdx > -1 && envelopeEndIdx > -1) {
				envelopeEndIdx = xml.indexOf(endTag) + endTag.length();
				xml = xml.substring(envelopeStartIdx, envelopeEndIdx);	
			}
		}
		return xml;
	}

}
