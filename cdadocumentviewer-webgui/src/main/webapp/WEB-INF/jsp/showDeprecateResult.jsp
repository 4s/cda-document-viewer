<%@taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp" />

<div class="container">
	<c:set var="deprecateResult" value="${deprecateResult}" />

	<c:if test="${notification != null}">
		<div class="alert alert-success" role="alert">
			<c:out value="${notification}" />
		</div>
	</c:if>
	<c:if test="${deprecateErrors != null}">
		<div class="alert alert-warning" role="alert">
			<c:forEach var="error" items="${deprecateErrors}">
					<p>
						<c:out value="${error}" />
					</p>
			</c:forEach>
		</div>
	</c:if>
	<button onclick="window.history.back();" class="btn btn-primary">Tilbage til søgning</button>
	<div>
		<br/>
		<a href="getLogEntry?logEntryId=<c:out value="${requestId}"/>">Download request (Deprecate)</a> <a href="getLogEntry?logEntryId=<c:out value="${responseId}"/>">Download response (Deprecate)</a>
	</div>
</div>

<jsp:include page="footer.jsp" />