<%@taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp" />

<c:set var="uploadResult" value="${uploadResult}" />

<div class="container">
	<c:if test="${notification != null}">
		<div class="alert alert-success" role="alert">
			<c:out value="${notification}" />
		</div>
		<c:url value="/search" var="url">
			<c:param name="patientID" value="${uploadResponseInfo.patientId}" />
			<c:param name="uniqueID" value="${uploadResponseInfo.externalDocumentId}" />
			<c:param name="backendCode" value="${uploadResponseInfo.backendCodeToSearchDocument}" />
			<c:param name="status" value="Approved" />
			<c:param name="typeStable" value="true" />
		</c:url>
		<a href="${url}">
  				<button class="btn btn-primary">Fremsøg</button>
		</a>
		<p>&nbsp;</p> 
	</c:if>
	
	<c:if test="${errorList != null}">
		<div class="alert alert-warning" role="alert">
		<c:forEach var="errorElm" items="${errorList}">
				<p>
					<c:out value="${errorElm}" />
				</p>
		</c:forEach>
		</div>
	</c:if>
	
	
	<div>
		<br/>
		<a href="getLogEntry?logEntryId=<c:out value="${requestId}"/>">Download request (Upload)</a> <a href="getLogEntry?logEntryId=<c:out value="${responseId}"/>">Download response (Upload)</a>
	</div>
	
</div>


<jsp:include page="footer.jsp" />