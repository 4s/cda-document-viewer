<%@taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp" />

<script type="text/javascript">
$(document).ready(function () {	
		
	$('#serviceStartFromLow').datepicker({dateFormat: "dd/mm/yy"});
	$('#serviceStartFromHigh').datepicker({dateFormat: "dd/mm/yy"});
	$('#serviceStopToLow').datepicker({dateFormat: "dd/mm/yy"});
	$('#serviceStopToHigh').datepicker({dateFormat: "dd/mm/yy"});
	
});
$(document).ready(function () {
        if(document.getElementById("typeStableDefaultValue").value == 'true'){
            document.getElementById("typeStable").checked = true;
        } else {
            document.getElementById("typeStable").checked = false;
        }
        if(document.getElementById("typeOnDemandDefaultValue").value == 'true'){
            document.getElementById("typeOnDemand").checked = true;
        } else {
            document.getElementById("typeOnDemand").checked = false;
        }
});


</script>


<div class="container">

	<c:if test="${searchErrorsTop != null}">
		<div class="alert alert-warning" role="alert">
			<c:forEach var="error" items="${searchErrorsTop}">
					<p>
						<c:out value="${error}" />
					</p>
				</c:forEach>
		</div>
	</c:if>

<div class="panel panel-default">
	<div class="panel-heading">
		Søg efter dokumenter
	</div>
	<div class="panel-body">
	<form:form modelAttribute="searchRequest" id="searchForm" action="search" method="GET">
	  	   <div class="row">

				<div class="form-group col-sm-1">
					<label for="registryEndpoint">Søg i</label>
				</div>
                <div class="form-group col-sm-3">
                    <form:select class="form-control" id="backendCodeDropdown" path="backendCode" onchange="document.getElementById('filterButton').click();">
                        <c:forEach var="registry" items="${searchRegistries}">
                            <form:option value="${registry.code}"><c:out value="${registry.name}" /></form:option>
                        </c:forEach>
                    </form:select>
                </div>
                <button type="submit" name="submitAction" value="filter" id="filterButton" style="display: none;">Skjult knap</button>
			</div>
			<hr />
	  	   <div class="row">
				
				<div class="form-group col-sm-1">
					<label for="profile">Søg som</label>
				</div>
				
				<div class="form-group col-sm-3">				
					<form:select class="form-control" path="profile">
					<c:forEach var="profile" items="${searchProfiles}">
						<form:option value="${profile.id}"><c:out value="${profile.name}" /></form:option>
					</c:forEach>
    				</form:select>
				</div>
				<div class="form-group col-sm-1">
					<form:checkbox class="form-control" id="vaerdispring" name="vaerdispring" path="vaerdispring" />
				</div>
				<div class="form-group col-sm-2">
					<label for="vaerdispring">Værdispring</label>
				</div>
				
			</div>
	
			<hr />
			<div class="row">
				<label for="patientID" class="col-form-label col-sm-1 col-form-label-sm" >Patient ID</label>
				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="patientID" name="patientID" path="patientID" />
				</div>
				
				<div class="form-group col-sm-1">
					<label for="uniqueID" title="${tooltipUniqueId}">Unique ID &#9432;</label>
				</div>
				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="uniqueID" name="uniqueID" path="uniqueID" />
					
				</div>
            </div>
			<div class="row">
                <div class="form-group col-sm-1">
                    <label for="typeCode">Type Code (code)</label>
                </div>
				<div class="form-group col-sm-3">
					<form:select class="form-control" id="typeCodeDropdown" path="typeCode" onchange="document.getElementById('chgDropDownButton').click();">
						<form:option value=""><c:out value="" /></form:option>
						<c:forEach var="typeCodeSelect" items="${typeCodeList}"	>
							<form:option value="${typeCodeSelect.code}"><c:out value="${typeCodeSelect.code} - ${typeCodeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
			    </div>
				<div class="form-group col-sm-1">
					<label for="typeCodeScheme">Type Code (scheme)</label>
				</div>
                <div class="form-group col-sm-3">
                    <form:input type="text" class="form-control" id="typeCodeScheme" name="typeCodeScheme" path="typeCodeScheme" readonly="true" value="${typeCodeScheme}"/>
                </div>
                <button type="submit" name="submitAction" value="chgDropDown" id="chgDropDownButton" style="display: none;">Skjult knap</button>

			</div>	
			<div class="row">
				
				<div class="form-group col-sm-1">
					<label for="formatCode" title="${tooltipFormatcode}">Format Code &#9432;</label>
				</div>
				<div class="form-group col-sm-3">
					<form:select class="form-control" path="formatCode">
						<form:option value=""><c:out value="" /></form:option>
						<c:forEach var="formatCodeSelect" items="${formatCodeList}"	>
							<form:option value="${formatCodeSelect.code}"><c:out value="${formatCodeSelect.code} - ${formatCodeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>
			
				<div class="form-group col-sm-1">
					<label for="healthcareFacilityTypeCode" title="${tooltipHealthcarefacilitytypecode}">Healthcare Facility Type Code &#9432;</label>
				</div>
				<div class="form-group col-sm-3">
					<form:select class="form-control" path="healthcareFacilityTypeCode">
						<form:option value=""><c:out value="" /></form:option>
						<c:forEach var="healthcareFacilityTypeCodeSelect" items="${healthcareFacilityTypeCodeList}"	>
							<form:option value="${healthcareFacilityTypeCodeSelect.code}"><c:out value="${healthcareFacilityTypeCodeSelect.code} - ${healthcareFacilityTypeCodeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>
				
			</div>	
			
			<div class="row">
			

				<div class="form-group col-sm-1">
					<label for="eventCode">Event Code (code)</label>
				</div>
				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="eventCode" name="eventCode" path="eventCode"/>
				</div>
				
				<div class="form-group col-sm-1">
					<label for="eventCodeScheme">Event Code (scheme)</label>
				</div>
								<div class="form-group col-sm-3">
					<form:select class="form-control" path="eventCodeScheme">
						<form:option value=""><c:out value="" /></form:option>
						<c:forEach var="eventCodeSchemeSelect" items="${eventCodeSchemeList}"	>
							<form:option value="${eventCodeSchemeSelect.code}"><c:out value="${eventCodeSchemeSelect.code} - ${eventCodeSchemeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>
			
			</div>
			
			<div class="row">
				
				<div class="form-group col-sm-1">
					<label for="practicesettingCode" title="${tooltipPracticesettingtypeCodeScheme}">Practice Setting Code &#9432;</label>
				</div>
				<div class="form-group col-sm-3">
					<form:select class="form-control" path="practicesettingCode">
						<form:option value=""><c:out value="" /></form:option>
						<c:forEach var="practiceSettingCodeSelect" items="${practiceSettingCodeList}">
						<form:option value="${practiceSettingCodeSelect.code}"><c:out value="${practiceSettingCodeSelect.code} - ${practiceSettingCodeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>
				
				<div class="form-group col-sm-1">
					<label title="${tooltipDocumentType}">Document Type &#9432;</label>
				</div>
				<div class="form-group col-sm-1">
					<form:checkbox class="form-control" id="typeStable" name="typeStable" path="typeStable" />
					<form:input type="hidden" id="typeStableDefaultValue" path="typeStableDefaultValue" value="${typeStableDefaultValue}"/>
				</div>
				<div class="form-group col-sm-2">
					<label for="typeStable">Stable</label>
				</div>
				<div class="form-group col-sm-1">
					<form:checkbox class="form-control" id="typeOnDemand" name="typeOnDemand" path="typeOnDemand" />
					<form:input type="hidden" id="typeOnDemandDefaultValue" path="typeOnDemandDefaultValue" value="${typeOnDemandDefaultValue}"/>
				</div>
				<div class="form-group col-sm-2">
					<label for="typeOnDemand">On-demand</label>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-sm-1">
					<label for="serviceStartFromLow">Service Start mellem</label>
				</div>
				
				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="serviceStartFromLow" name="serviceStartFromLow" path="serviceStartFromLow"/>
				</div>
				
				<div class="form-group col-sm-1">
					<label for="serviceStartFromHigh">og</label>
				</div>

				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="serviceStartFromHigh" name="serviceStartFromHigh" path="serviceStartFromHigh"/>
				</div>
			</div>			
			<div class="row">
				<div class="form-group col-sm-1">
					<label for="serviceStopToLow">Service Stop mellem</label>
				</div>
				
				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="serviceStopToLow" name="serviceStopToLow" path="serviceStopToLow"/>
				</div>

				<div class="form-group col-sm-1">
					<label for="serviceStopToHigh">og</label>
				</div>
				
				<div class="form-group col-sm-3">
					<form:input type="text" class="form-control" id="serviceStopToHigh" name="serviceStopToHigh" path="serviceStopToHigh"/>
				</div>
			</div>			
			<div class="row">
				
				<div class="form-group col-sm-1">
					<label for="status">Availability Status</label>
				</div>
				
				<div class="form-group col-sm-3">				
					<form:select class="form-control" path="status" >
 						<form:option value="Approved">Approved</form:option>
 						<form:option value="Deprecated">Deprecated</form:option>
 						<form:option value="Submitted">Submitted</form:option>
 						<form:option value=""></form:option>
    				</form:select>
				</div>
				

				
				<div class="form-group col-sm-8">				
					<button type="submit" class="btn btn-primary">Søg</button>
					<button type="reset" class="btn">Nulstil</button>
				</div>
				
			</div>

			<hr />
<div class="panel-heading">
	<a href="getLogEntry?logEntryId=<c:out value="${requestId}"/>">Download seneste request (Søgning)</a> <a href="getLogEntry?logEntryId=<c:out value="${responseId}"/>">Download seneste response (Søgning)</a>
</div>

	</form:form>
	</div>
</div>

<c:if test="${notification != null}">
	<div class="alert alert-success" role="alert">
		<c:out value="${notification}" />
	</div>
</c:if>

<c:if test="${not empty searchResult}">
	<table cellspacing="0" class="table table-striped table-bordered table-hover table-sm" id="searchResultsTable">
		<thead>
			<tr>
				<th>Unique ID </th>
				<th>Repository ID</th>
				<th>Document Type</th>
				<th>Service Start</th>
				<th>Service Stop</th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="document" items="${searchResult}">
				<tr style="font-size:12px;">
					<td style="word-break:break-all;"><c:out value="${document.getUniqueId()}" /></td>
					<td><c:out value="${document.getRepositoryUniqueId()}" /></td>
					<td><c:out value="${document.getDocumentType()}" /></td>
					<td data-order="${document.getStartSort()}"><c:out value="${document.getServiceStartTime()}" /></td>
					<td data-order="${document.getStopSort()}"><c:out value="${document.getServiceStopTime()}" /></td>
					<c:url value="/showDocument" var="url">
  						<c:param name="externalDocumentId" value="${document.getUniqueId()}" />
  						<c:param name="repositoryId" value="${document.getRepositoryUniqueId()}" />
  						<c:param name="patientId" value="${document.getPatientId()}" />
  						<c:param name="backendCode" value="${document.getBackendCode()}" />
  						<c:if test="${document.getHomeCommunityId() != null && !document.getHomeCommunityId().trim().isEmpty()}"> <c:param name="homecommunityid" value="${document.getHomeCommunityId()}"/>  </c:if>
					</c:url>

					<c:url value="/upload" var="urlUpdate">
  						<c:param name="externalDocumentId" value="${document.getUniqueId()}" />
  						<c:param name="repositoryId" value="${document.getRepositoryUniqueId()}" />
  						<c:param name="entryUuid" value="${document.getEntryUuid()}" />
					</c:url>
                    <td>
                        <form method="post" action="${url}" target="_blank">
                           <input type="hidden" name="encodedMetadata" value="${document.getDocumentEntryJsonFormatBase64Encoded()}" />
                           <a href="${url}" onClick="this.parentNode.submit(); return false;">Vis</a>
                         </form>
                    </td>
					
					<td>
						<c:if test="${document.isAllowUpdate() == true}">
							<a id="documentLinkUpdate" href="${urlUpdate}">Ret</a>
						</c:if>
					</td>

					<c:url value="/deprecate" var="urlDeprecate">
  						<c:param name="externalDocumentId" value="${document.getUniqueId()}" />
  						<c:param name="repositoryId" value="${document.getRepositoryUniqueId()}" />
  						<c:param name="patientId" value="${document.getPatientId()}" />
					</c:url>
					<td>
						<c:if test="${document.isAllowDeprecate() == true}">
							<a id="documentLinkDeprecate" href="${urlDeprecate}">Deprecate</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</c:if>

<c:if test="${searchErrorsBottom != null}">
	<div class="alert alert-warning" role="alert">
		<c:forEach var="error" items="${searchErrorsBottom}">
				<p>
					<c:out value="${error}" />
				</p>
			</c:forEach>
	</div>
</c:if>

</div>


<jsp:include page="footer.jsp" />
