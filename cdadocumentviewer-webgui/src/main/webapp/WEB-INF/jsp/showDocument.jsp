<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp"/>

<c:set var="docEntry" value="${docEntry}" />
<c:set var="document" value="${document}" />

<div class="container">
<div class="panel panel-default">
	
	<div class="panel-heading"><c:out value="Dokument: ${docEntry.uniqueId }"></c:out></div>
	
	<div class="panel-body">
	
	<c:if test="${notification != null}">
	<div class="alert alert-success" role="alert"><c:out value="${notification}" /></div>
	</c:if>

	<c:if test="${error != null}">
	<div class="alert alert-warning" role="alert"><c:out value="${error}" /></div>
	</c:if>

	<br/>
	<br/>	

	<div class="panel panel-default">
	<div class="panel-heading">DocumentEntry Metadata</div>
	<div class="panel-body">
	
	<c:if test="${docEntryErrors != null}">
	<div class="alert alert-warning" role="alert">
		<c:forEach var="error" items="${docEntryErrors}">
				<p>
					<c:out value="${error}" />
				</p>
			</c:forEach>
	</div>
	</c:if>
	
	<c:if test="${docEntry != null}">
	<table class="table table-bordered">
				<thead>
					<tr>
						<td>Variabel</td>
						<td>Værdi</td>
					</tr>
				</thead>
				
				<tbody>
				
					<c:forEach var="author" items="${docEntry.authors}">
					    <c:choose>
					    <c:when test="${author.authorInstitution.size() > 0}">
                            <c:forEach var="authorInstitution" items="${author.authorInstitution}" varStatus="status">
                                <tr>
                                    <c:choose>
                                        <c:when test="${status.first}">
                                            <td>AuthorInstitution</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td></td>
                                        </c:otherwise>
                                    </c:choose>

                                    <td>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td><b>code:</b></td>
                                                    <td>${authorInstitution.idNumber}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>name:</b></td>
                                                    <td>${authorInstitution.organizationName}</td>
                                                </tr>
                                                <tr>
                                                    <td><b>codeScheme:</b></td>
                                                    <td>${authorInstitution.assigningAuthority_universalId}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <tr>
                                <td>authorInstitution</td>
                            </tr>
                        </c:otherwise>
                        </c:choose>
						
						<tr>
							<td>AuthorPerson</td>
							<td>
								<table>
									<tbody>
										<tr>
											<td><b>name:</b></td>
											<td>${author.authorPerson.givenName} ${author.authorPerson.secondAndFurtherGivenNames} ${author.authorPerson.familyName}</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>		
						
					</c:forEach>
					
					<t:attribute name="AvailabilityStatus" value="${docEntry.availabilityStatus}" />

					<t:codeViewer code="${docEntry.classCode}" name="ClassCode" />
					
					<t:attribute name="Comments" value="${docEntry.comments}" />
				
					<t:codelistViewer name="ConfidentialityCode" codelist="${docEntry.confidentialityCodes}" />
					
					<t:attribute name="CreationTime" value="${docEntry.creationTime}" />
					
					<t:attribute name="DocumentAvailability" value="${docEntry.documentAvailability}" />
					
					<t:attribute name="EntryUuid" value="${docEntry.entryUuid}" />

					<t:codelistViewer name="EventCode" codelist="${docEntry.eventCodeList}" />
					
					<t:attribute name="ExtraMetadata" value="${docEntry.extraMetadata}" />

					<t:codeViewer code="${docEntry.formatCode}" name="FormatCode" />
					
					<t:attribute name="Hash" value="${docEntry.hash}" />

					<t:codeViewer code="${docEntry.healthcareFacilityTypeCode}" name="HealthcareFacilityTypeCode" />

					<t:attribute name="HomeCommunityId" value="${docEntry.homeCommunityId}" />
					
					<t:attribute name="LanguageCode" value="${docEntry.languageCode}" />

					<tr>
							<td>LegalAuthenticator</td>
							<td>
								<table>
									<tbody>
										<tr>
											<td><b>name:</b></td>
											<td>${docEntry.legalAuthenticator_givenName} ${docEntry.legalAuthenticator_secondAndFurtherGivenNames} ${docEntry.legalAuthenticator_familyName}</td>
										</tr>
									</tbody>
								</table>
							</td>
					</tr>		

					
					<t:attribute name="LogicalUuid" value="${docEntry.logicalUuid}" />

					<t:attribute name="MimeType" value="${docEntry.mimeType}" />
					
					<t:attribute name="ObjectType" value="${docEntry.typeUuid}" />

                    <tr>
                        <td>PatientId</td>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <td><b>code:</b></td>
                                        <td>${docEntry.patientId.id}</td>
                                    </tr>
                                    <tr>
                                        <td><b>codeScheme:</b></td>
                                        <td>${docEntry.patientId.assigningAuthority_universalId}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

					
					<t:codeViewer code="${docEntry.practiceSettingCode}" name="PracticeSettingCode" />

					<c:choose>
						<c:when test="${docEntry.referenceIdList.size() > 0}">
							<c:forEach var="referenceId" items="${docEntry.referenceIdList}" varStatus="status">
								<tr>
									<c:choose>
										<c:when test="${status.first}">
											<td>ReferenceidList</td>
										</c:when>
										<c:otherwise>
											<td></td>
										</c:otherwise>
									</c:choose>
			
									<td>
										<table>
											<tbody>
												<tr>
													<td><b>Id:</b></td>
													<td>${referenceId.id}</td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</c:forEach>					
						</c:when>
						<c:otherwise>
							<tr>
								<td>ReferenceidList</td>
							</tr>
						</c:otherwise>
					</c:choose>
					
					<t:attribute name="RepositoryUniqueId" value="${docEntry.repositoryUniqueId}" />
					
					<t:attribute name="ServiceStartTime" value="${docEntry.serviceStartTime}" />
					
					<t:attribute name="ServiceStopTime" value="${docEntry.serviceStopTime}" />
					
					<t:attribute name="Size" value="${docEntry.size}" />
	
					<tr>
						<td>SourcePatientId</td>
						<td>
							<table>
								<tbody>
									<tr>
										<td><b>code:</b></td>
										<td>${docEntry.sourcePatientId.id}</td>
									</tr>
									<tr>
										<td><b>codeScheme:</b></td>
										<td>${docEntry.sourcePatientId.assigningAuthority_universalId}</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>

					<tr>
						<td>SourcePatientInfo</td>
						<td>
							<table>
								<tbody>
									<tr>
										<td><b>name:</b></td>
										<td>${docEntry.sourcePatientInfo.givenName} ${docEntry.sourcePatientInfo.secondAndFurtherGivenNames} ${docEntry.sourcePatientInfo.familyName}</td>
									</tr>									
									<tr>
										<td><b>gender:</b></td>
										<td>${docEntry.sourcePatientInfo.gender}</td>
									</tr>									
									<tr>
										<td><b>birthTime:</b></td>
										<td>${docEntry.sourcePatientInfo.dateOfBirth}</td>
									</tr>
								</tbody>
							</table>					
						</td>
					</tr>
					
					<t:attribute name="Title" value="${docEntry.title}" />
					
					<t:attribute name="Type" value="${docEntry.type}" />
					
					<t:codeViewer code="${docEntry.typeCode}" name="TypeCode" />
					
					<t:attribute name="UniqueId" value="${docEntry.uniqueId}" />
					
					<t:attribute name="Uri" value="${docEntry.uri}" />
					
					<t:attribute name="Version" value="${docEntry.version}" />
					
				</tbody>
				
			</table>
			</c:if>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Document</div>
	<div class="panel-body">
		<pre lang="xml">
			<c:if test="${document != null}">
				<c:out value="${document}" escapeXml="true" />
			</c:if>			
		</pre>
	</div>
	<c:if test="${documentFetchErrors != null}">
		<div class="alert alert-warning" role="alert">
			<c:forEach var="error" items="${documentFetchErrors}">
					<p>
						<c:out value="${error}" />
					</p>
			</c:forEach>
		</div>
	</c:if>
	
</div>

<br/>
<a href="getLogEntry?logEntryId=<c:out value="${documentFetchRequestId}"/>" title="Download SOAP request">Download request (hent dokument)</a> <a href="getLogEntry?logEntryId=<c:out value="${documentFetchResponseId}"/>" title="Download SOAP response">Download response (hent dokument)</a>			

<br/>
<c:url value="/getDocumentRaw" var="urlGetDocumentRaw">
		<c:param name="externalDocumentId" value="${externalDocumentId}" />
		<c:param name="repositoryUniqueId" value="${repositoryId}" />
		<c:param name="patientId" value="${patientId}" />
		<c:param name="backendCode" value="${backendCode}" />
		<c:if test="${homecommunityid != null && !homecommunityid.trim().isEmpty()}"> <c:param name="homecommunityid" value="${homecommunityid}"/>  </c:if>
</c:url>
<a class="btn btn-default" target='_blank' href="${urlGetDocumentRaw}" title="Download CDA dokumentet direkte fra repository">Hent Dokument</a>
<br/>

<c:url value="/getDocument" var="url">
		<c:param name="externalDocumentId" value="${externalDocumentId}" />
		<c:param name="repositoryUniqueId" value="${repositoryId}" />
		<c:param name="patientId" value="${patientId}" />
		<c:param name="backendCode" value="${backendCode}" />
		<c:if test="${homecommunityid != null && !homecommunityid.trim().isEmpty()}"> <c:param name="homecommunityid" value="${homecommunityid}"/>  </c:if>
</c:url>
<a id="documentLink" href="${url}" title="Download transformeret version af CDA dokumentet fra repository">Hent Dokument og transformer</a>


</div>
</div>
</div>


<jsp:include page="footer.jsp" />