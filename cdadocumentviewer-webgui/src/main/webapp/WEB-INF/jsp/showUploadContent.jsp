<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp" />

<c:if test="${errorList != null}">
	<div class="alert alert-warning" role="alert">
		<c:forEach var="errorElm" items="${errorList}">
				<p>
					<c:out value="${errorElm}" />
				</p>
		</c:forEach>
	</div>
</c:if>


<c:set var="documentMetadata" value="${fileMetadata}" />

<div class="panel-group">
	<h2>Metadata</h2>
	<p>Input til metadata og visning af genereret metadata pba. fil</p>

	<div class="panel panel-default">
		<div class="panel-heading">Information om upload (${uploadOperation})</div>
		<div class="panel-body">
			<form:form>
			<div class="row">
				<div class="form-group col-sm-2">
					<label for=repository>Repository</label>
					<input type="text" class="form-control" id="repository" name="repository" readonly = "readonly" value="${valueRepository}">
				</div>
				<div class="form-group col-sm-2">
					<label for=repository>HomeCommunityId</label>
					<input type="text" class="form-control" id="homecommunityid" name="homecommunityid" readonly = "readonly" value="${valueHomecommunityid}">
				</div>
			</div>
			</form:form>
		</div>


	<div style="padding-top: 25px"></div>


			
		<div class="panel-heading">Angiv valgfri metadata</div>
		<div class="panel-body">
			<form:form modelAttribute="metadataInput" action="uploadDocument" method="POST">
				
			<div class="row">
				<div class="form-group col-sm-2">
					<label for="availabilityStatus">availabilityStatus</label>
					<form:select class="form-control" path="availabilityStatus">
						<c:forEach var="availabilityStatusSelect" items="${availabilityStatusList}">
							<form:option value="${availabilityStatusSelect.code}"><c:out value="${availabilityStatusSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>

				<div class="form-group col-sm-2">
						<label for="classCode" title="${tooltipClassCode}">ClassCode &#9432;</label>
						<input type="text" class="form-control" id="classCode" name="classCode" readonly = "readonly" value="${documentMetadata.classCode.code} - ${documentMetadata.classCode.name}">
				</div>
			   				
<!-- 			Either show dropdown or protected field, controlled from what is sent to screen  -->
			   <c:if test="${formatCodeList != null}">	
				<div class="form-group col-sm-2">
					<label for="formatCode" title="${tooltipFormatCode}">FormatCode &#9432;</label>
					<form:select class="form-control" path="formatCode">
						<c:forEach var="formatCodeSelect" items="${formatCodeList}"	>
							<form:option value="${formatCodeSelect.code}"><c:out value="${formatCodeSelect.code} - ${formatCodeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>
			   </c:if>	
			   <c:if test="${documentMetadata.formatCode != null}">
				<div class="form-group col-sm-2">
						<label for="formatCode">FormatCode</label>
						<input type="text" class="form-control" id="formatCode" name="formatCode" readonly = "readonly" value="${documentMetadata.formatCode.code}">
				</div>
			   </c:if>
			
				<div class="form-group col-sm-2">
					<label for="healthcareFacilityTypeCode" title="${tooltipHealthcareFacilityTypeCode}">HealthcareFacilityTypeCode &#9432;</label>
					<form:select class="form-control" path="healthcareFacilityTypeCode">
						<c:forEach var="healthcareFacilityTypeCodeSelect" items="${healthcareFacilityTypeCodeList}"	>
							<option value="${healthcareFacilityTypeCodeSelect.code}" ${healthcareFacilityTypeCodeSelect.code == selectedNA ? 'selected="selected"' : ''}>${healthcareFacilityTypeCodeSelect.code} - ${healthcareFacilityTypeCodeSelect.name}</option>
						</c:forEach>
    				</form:select>
				</div>				

			</div>

			<div class="row">
			
				<div class="form-group col-sm-2">
					<label for="objectType">ObjectType</label>
					<form:select class="form-control" path="objectType">
						<c:forEach var="objectTypeSelect" items="${objectTypeList}"	>
							<form:option value="${objectTypeSelect.code}"><c:out value="${objectTypeSelect.code} - ${objectTypeSelect.name}" /></form:option>
						</c:forEach>
    				</form:select>
				</div>
				<div class="form-group col-sm-2">
					<label for="practiceSettingCode" title="${tooltipPracticeSettingCode}">PracticeSetting &#9432;</label>
					<form:select class="form-control" path="practiceSettingCode">
						<c:forEach var="practiceSettingCodeSelect" items="${practiceSettingCodeList}">
						<option value="${practiceSettingCodeSelect.code}" ${practiceSettingCodeSelect.code == selectedNA ? 'selected="selected"' : ''}>${practiceSettingCodeSelect.code} - ${practiceSettingCodeSelect.name}</option>
						</c:forEach>
    				</form:select>
				</div>			

				<div class="form-group col-sm-2">
					<label for="submissionTime">SubmissionTime</label>
					<input type="text" class="form-control" id="submissionTime" name="submissionTime" readonly = "readonly" value="Faktisk tid for upload anvendes">
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Upload</button>
			</form:form>
		</div>
	</div>
	<div style="padding-top: 25px"></div>

	<div class="panel panel-default">
		<div class="panel-heading">Metadata genereret udfra fil</div>
		<div class="panel-body">
		
			<table class="table table-bordered" style="width:75%">
				<thead>
					<tr>
						<th style="width:15%">Variabel</th>
						<th>Værdi</th>
					</tr>
				</thead>
				
				<tbody>

					<tr>							
						<td>AuthorInstitution</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">Code:</th>
									<th style="width:200px">CodeScheme:</th>
									<th>Name:</th>
								</tr>
								<tr>
									<td>${documentMetadata.organisation.code}</td>
									<td>${documentMetadata.organisation.codingScheme}</td>
									<td>${documentMetadata.organisation.name}</td>
								</tr>

							</table>
						</td>
					</tr>
					<tr>
						<td>AuthorPerson</td>
						<td>
						<table>
							<tr>
								<th style="width:200px">FamilyName:</th>
								<th style="width:200px">GivenName:</th>
								<th>SecondAndFurtherGivenNames:</th>
							</tr>
							<tr>
								<td>${documentMetadata.authorPerson.familyName}</td>
								<td>${documentMetadata.authorPerson.givenName}</td>
								<td>${documentMetadata.authorPerson.secondAndFurtherGivenNames}</td>
							</tr>
							</table>
						</td>
					</tr>		
					<tr>							
						<td>ConfidentialityCode</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">Code:</th>
									<th style="width:200px">CodeScheme:</th>
									<th>Name:</th>
								</tr>
								<tr>
									<td>${documentMetadata.confidentialityCode.code}</td>
									<td>${documentMetadata.confidentialityCode.codingScheme}</td>
									<td>${documentMetadata.confidentialityCode.name}</td>
								</tr>

							</table>
						</td>
					</tr>

					<tr>
						<td>CreationTime (UTC)</td>
						<td>${documentMetadata.reportTimeStringUTC}</td>
					</tr>					

					<tr>
						<td>EventCodeList</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">Code:</th>
									<th style="width:200px">CodeScheme:</th>
									<th>Name:</th>
								</tr>
								<c:forEach var="item" items="${documentMetadata.eventCodes}">
									<tr>
										<td>
											<c:out value="${item.code}     "></c:out>
										</td>
										<td>
											<c:out value="${item.codingScheme} "></c:out>			
										</td>
										<td>
											<c:out value="${item.name}     "></c:out>			
										</td>
									</tr>
								</c:forEach>
							</table>
						
						</td>
					</tr>
					<tr>
						<td>LanguageCode</td>
						<td>${documentMetadata.languageCode}</td>
					</tr>					
					<tr>
						<td>LegalAuthenticator</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">FamilyName:</th>
									<th style="width:200px">GivenName:</th>
									<th>SecondAndFurtherGivenNames:</th>
								</tr>
								<tr>
									<td>${documentMetadata.legalAuthenticator.familyName}</td>
									<td>${documentMetadata.legalAuthenticator.givenName}</td>
									<td>${documentMetadata.legalAuthenticator.secondAndFurtherGivenNames}</td>
								</tr>

							</table>
						</td>
					</tr>		
					<tr>							
						<td>PatientId</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">Code:</th>
									<th style="width:200px">CodeScheme:</th>
									<th>Name:</th>
								</tr>
								<tr>
									<td>${documentMetadata.patientId.code}</td>
									<td>${documentMetadata.patientId.codingScheme}</td>
								</tr>
							</table>
						</td>
					</tr>


					<tr>
						<td>ServiceStartTime (UTC)</td>
						<td>${documentMetadata.serviceStartTimeStringUTC}</td>
					</tr>
					<tr>
						<td>ServiceStopTime (UTC)</td>
						<td>${documentMetadata.serviceStopTimeStringUTC}</td>
					</tr>
					<tr>							
						<td>SourcePatientId</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">Code:</th>
									<th style="width:200px">CodeScheme:</th>
									<th>Name:</th>
								</tr>
								<tr>
									<td>${documentMetadata.sourcePatientId.code}</td>
									<td>${documentMetadata.sourcePatientId.codingScheme}</td>
								</tr>

							</table>
						</td>
					</tr>
					<tr>							
						<td>SourcePatientInfo</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">FamilyName:</th>
									<th style="width:200px">GivenName:</th>
									<th style="width:250px">SecondaryAndFurtherNames:</th>
									<th style="width:100px">Gender:</th>
									<th style="width:300px">DateOfBirth:</th>
								</tr>
								<tr>
									<td>${documentMetadata.sourcePatientInfoPerson.familyName}</td>
									<td>${documentMetadata.sourcePatientInfoPerson.givenName}</td>
									<td>${documentMetadata.sourcePatientInfoPerson.secondAndFurtherGivenNames}</td>
									<td>${documentMetadata.sourcePatientInfoGender}</td>
									<td>${documentMetadata.sourcePatientInfoDateOfBirthString}</td>
								</tr>

							</table>
						</td>
					</tr>
					<tr>
						<td>Title</td>
						<td>${documentMetadata.title}</td>
					</tr>

					<tr>							
						<td>TypeCode</td>
						<td>
							<table>
								<tr>
									<th style="width:200px">Code:</th>
									<th style="width:200px">CodeScheme:</th>
									<th>Name:</th>
								</tr>
								<tr>
									<td>${documentMetadata.typeCode.code}</td>
									<td>${documentMetadata.typeCode.codingScheme}</td>
									<td>${documentMetadata.typeCode.name}</td>
								</tr>
							</table>
						</td>
					</tr>

					<tr>							
						<td>UniqueId</td>
						<td>
							<table>
								<tr>
									<td>${documentMetadata.uniqueId}</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div style="padding-top: 30px"></div>

<div class="panel panel-default">
	<div class="panel-heading">Filindhold</div>
	<div class="panel-body">
		<pre>
			<c:out value="${fileContent}" escapeXml="true" />
		</pre>
	</div>
	
</div>

<jsp:include page="footer.jsp" />