<%@taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>CDA Document Viewer</title>
 	<link type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="css/dataTables.bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="css/jquery-ui.css" rel="stylesheet">
 	<script type="text/javascript" src="js/jquery.min.js" ></script>
 	<script type="text/javascript" src="js/jquery.dataTables.min.js" ></script>
 	<script type="text/javascript" src="js/dataTables.bootstrap.min.js" ></script>
 	<script type="text/javascript" src="js/jquery-ui.js" ></script>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand">CDA Document Viewer</div>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="newsearch">Søg</a></li>
      <li><a href="upload">Upload</a></li>
      <li><a href="about">Om...</a></li>
    </ul>
  </div>
</nav>
	