<%@taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp" />

<div class="container">

<div class="panel panel-default">
	<div class="panel-heading">Om CDA Document Viewer</div>
	<div class="panel-body">

<table class="table table-bordered">
  <tbody>
    <tr>
      <td>STS url</td>
      <td><c:out value="${stsendpoint}" /></td>
    </tr>
  </tbody>
</table>

<table class="table table-bordered">
  <tbody>
    <tr>
      <td>Backends</td>
      <td>
            <c:if test="${backendList != null}">
            <table class="table">
                <tbody>
                    <c:forEach var="iti18Elm" items="${backendList}">
                    <table class="table table-bordered">
                        <tbody>
                            <tr><td>Backend</td><td>${iti18Elm.name}</td></tr>
                            <tr><td>Kode</td><td>${iti18Elm.backendCode}</td></tr>
                            <tr><td>ITI-18 url</td><td>${iti18Elm.endpoint}, dgws=${iti18Elm.dgws}</td></tr>
                            <tr><td>ITI-43 url</td><td>${iti18Elm.endpointIti43}, dgws=${iti18Elm.dgwsIti43}</td></tr>
                        </tbody>
                    </table>
                   </c:forEach>
                </tbody>
            </table>
        </c:if>
      </td>
    </tr>
  </tbody>
</table>

<table class="table table-bordered">
  <tbody>
    <tr>
      <td>Repositories</td>
      <td>
      		<c:if test="${repoList != null}">
			<table class="table">
				<tbody>
					<c:forEach var="iti41Elm" items="${repoList}">
                    <table class="table table-bordered">
                        <tbody>
                            <tr><td>Repository</td><td>${iti41Elm.name}</td></tr>
                            <tr><td>RepositoryUniqueId</td><td>${iti41Elm.repositoryuniqueid}</td></tr>
                            <tr><td>HomeCommunityId</td><td>${iti41Elm.homeCommunityId}</td></tr>
                            <tr><td>ITI-41 url</td><td>${iti41Elm.endpoint}, dgws=${iti41Elm.dgws}, uploadAllowed=${iti41Elm.uploadallowed}, updateAllowed=${iti41Elm.updateallowed}</td></tr>
                            <tr><td>ITI-57 url</td><td>${iti41Elm.endpointIti57}, dgws=${iti41Elm.dgwsIti57}, deprecateAllowed=${iti41Elm.deprecateallowed}</td></tr>
                            <tr><td>Backend kode</td><td>${iti41Elm.backendCode}</td></tr>
                            <tr><td>DokumentTyper</td><td>${iti41Elm.validDocTypes}</td></tr>
                        </tbody>
                    </table>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
      </td>
    </tr>
  </tbody>
</table>



</div>
</div>
</div>


<jsp:include page="footer.jsp" />