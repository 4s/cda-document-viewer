<%@taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page language="java" pageEncoding="UTF-8" contentType="text/html;charset=UTF-8"%>

<jsp:include page="header.jsp" />
<div class="container">
	<c:if test="${errorList != null}">
		<div class="alert alert-warning" role="alert">
			<c:forEach var="errorElm" items="${errorList}">
					<p>
						<c:out value="${errorElm}" />
					</p>
			</c:forEach>
		</div>
	</c:if>

	<div class="panel panel-default">
		<div class="panel-heading">Upload dokument (${uploadOperation})</div>
		<div class="panel-body">
			<form:form modelAttribute="request" action="showUpload" enctype="multipart/form-data" method="POST">
				
				<div class="form-group">
					<label for="file">Vælg fil</label>
					<input class="form-control" type="file" id="cdaFile" name="cdaFile" style="padding-bottom:40px">
				</div>
			<c:if test="${selectedRepositoryName == null}">
				<div class="form-group">
					<label for="repository">Vælg repository</label>
					<form:select class="form-control" path="repositoryEndpoint">
						<c:forEach var="repositorySelect" items="${repositoryList}">
								<form:option value="${repositorySelect.code}"><c:out value="${repositorySelect.name}" /></form:option>
						</c:forEach>
			 		</form:select>
				</div>
			</c:if>
			<c:if test="${selectedRepositoryName != null}">
				<div class="form-group">
					<label for="repository">Repository</label>
					<input type="text" class="form-control" id="repositoryName" name="repositoryName" readonly = "readonly" value="${selectedRepositoryName}"/>
				</div>
			
			</c:if>	
				<button type="submit" class="btn btn-primary">Preview Upload</button>
		
			</form:form>
		</div>
	</div>
</div>

<jsp:include page="footer.jsp" />