<!DOCTYPE html>
<%@tag description="Code" pageEncoding="UTF-8"%>
<%@attribute name="code" type="dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata.CodeMetadata" %>
<%@attribute name="name" type="java.lang.String" %>
 
<tr>
	<td>${name}</td>
	<td>
		<table>
			<tbody>
				<tr>
					<td><b>code:</b></td>
					<td>${code.code}</td>
				</tr>
				<tr>
					<td><b>name:</b></td>
					<td>${code.displayName}</td>
				</tr>
				<tr>
					<td><b>codeScheme:</b></td>
					<td>${code.schemeName}</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>