<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@tag description="CodeList" pageEncoding="UTF-8"%>
<%@attribute name="codelist" type="java.util.List" %>
<%@attribute name="name" type="java.lang.String" %>

<c:forEach var="code" items="${codelist}" varStatus="status">
	<tr>
		<c:choose>
			<c:when test="${status.first}">
				<td>${name}</td>
			</c:when>
			<c:otherwise>
				<td></td>
			</c:otherwise>
		</c:choose>
		<td>
			<table>
				<tbody>
					<tr>
						<td><b>code:</b></td>
						<td>${code.code}</td>
					</tr>
					<tr>
						<td><b>name:</b></td>
						<td>${code.displayName.value}</td>
					</tr>
					<tr>
						<td><b>codeScheme:</b></td>
						<td>${code.schemeName}</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</c:forEach>