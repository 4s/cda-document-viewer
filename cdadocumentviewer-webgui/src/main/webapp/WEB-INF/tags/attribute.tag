<!DOCTYPE html>
<%@tag description="Attribute" pageEncoding="UTF-8"%>
<%@attribute name="value"  %>
<%@attribute name="name" %>
 
<tr>
	<td>${name}</td>
	<td>${value}</td>
</tr>