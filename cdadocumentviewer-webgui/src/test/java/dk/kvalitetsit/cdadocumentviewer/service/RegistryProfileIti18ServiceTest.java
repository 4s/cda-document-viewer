package dk.kvalitetsit.cdadocumentviewer.service;

import dk.kvalitetsit.cdadocumentviewer.configuration.RegistryProfileIti18Configuration;
import dk.kvalitetsit.cdadocumentviewer.dto.RegistryProfileIti18Dto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.BeanFactory;

import java.util.List;

import static org.junit.Assert.*;

public class RegistryProfileIti18ServiceTest {

    BeanFactory beanFactory = Mockito.mock(BeanFactory.class);
    RegistryProfileIti18Configuration registryProfileIti18Configuration;
    String code1 = "DDSDDS";
    String code2 = "SFSKSFSK";
    String endpoint1 = "http://test1-cnsp.ekstern-test.nspop.dk:8080/ddsregistry";
    String endpoint2 = "http://test1-cnsp.ekstern-test.nspop.dk:8080/sfsk/iti18";
    String endpoint3 = "http://test1-cnsp.ekstern-test.nspop.dk:8080/ddsrepository";
    String endpoint4 = "http://test1-cnsp.ekstern-test.nspop.dk:8080/sfsk/iti43";
    String name1 = "DDS";
    String name2 = "SFSK";

    @Before
    public void prepareTest() {
        registryProfileIti18Configuration = new RegistryProfileIti18Configuration();

        RegistryProfileIti18Configuration.Reg reg = new RegistryProfileIti18Configuration.Reg(code1, name1, endpoint1, true, endpoint3, true);
        registryProfileIti18Configuration.addReg(reg);

        reg = new RegistryProfileIti18Configuration.Reg(code2, name2, endpoint2, true, endpoint4, true);
        registryProfileIti18Configuration.addReg(reg);

    }

    @Test
    public void testCreateRegistryProfileIti18Service() {

        // Given

        // When
        RegistryProfileIti18Service registryProfileIti18Service = new RegistryProfileIti18Service(beanFactory, registryProfileIti18Configuration);

        // Then
        assertNotNull(registryProfileIti18Service);

    }

    @Test
    public void testRegistryProfileIti18ServiceDropdown() {

        // Given
        RegistryProfileIti18Service registryProfileIti18Service = new RegistryProfileIti18Service(beanFactory, registryProfileIti18Configuration);

        // When
        DropDownListService dropDownListService = registryProfileIti18Service.getRegistryAsDropDownListForSearch();

        // Then
        assertNotNull(dropDownListService);
        assertNotNull(dropDownListService.getDropDownListValues());
        assertEquals(2, dropDownListService.getDropDownListValues().size());
        assertNotNull(dropDownListService.getDropDownListValues().get(0));
        assertNotNull(dropDownListService.getDropDownListValues().get(0).getCode());
        assertEquals(code1, dropDownListService.getDropDownListValues().get(0).getCode());
        assertEquals(name1, dropDownListService.getDropDownListValues().get(0).getName());
        assertEquals(code2, dropDownListService.getDropDownListValues().get(1).getCode());
        assertEquals(name2, dropDownListService.getDropDownListValues().get(1).getName());

    }
    @Test
    public void testRegistryProfileIti18ServiceGetRegistryFromBackendFound() {

        // Given
        RegistryProfileIti18Service registryProfileIti18Service = new RegistryProfileIti18Service(beanFactory, registryProfileIti18Configuration);

        // When
        RegistryProfileIti18Dto registryProfileIti18Dto = registryProfileIti18Service.getRegistryFromBackend(code2);


        // Then
        assertNotNull(registryProfileIti18Dto);
        assertEquals(endpoint2, registryProfileIti18Dto.getEndpoint());

    }

    @Test
    public void testRegistryProfileIti18ServiceGetRegistryFromBackendNotFound() {

        // Given
        RegistryProfileIti18Service registryProfileIti18Service = new RegistryProfileIti18Service(beanFactory, registryProfileIti18Configuration);

        // When
        RegistryProfileIti18Dto registryProfileIti18Dto = registryProfileIti18Service.getRegistryFromBackend("code3");


        // Then
        assertNull(registryProfileIti18Dto);

    }

    @Test
    public void testRegistryProfileIti18ServiceGetRegistryProfileList() {

        // Given
        RegistryProfileIti18Service registryProfileIti18Service = new RegistryProfileIti18Service(beanFactory, registryProfileIti18Configuration);

        // When
        List<RegistryProfileIti18Dto> registryProfileIti18DtoList = registryProfileIti18Service.getRegistryProfileList();


        // Then
        assertNotNull(registryProfileIti18DtoList);
        assertEquals(2, registryProfileIti18DtoList.size());

    }
}
