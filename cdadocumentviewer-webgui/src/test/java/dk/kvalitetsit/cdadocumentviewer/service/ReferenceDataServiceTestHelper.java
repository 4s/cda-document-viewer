package dk.kvalitetsit.cdadocumentviewer.service;

public class ReferenceDataServiceTestHelper extends ReferenceDataService {

	public static ReferenceDataService getSimpleReferenceData() {
		
		ReferenceDataServiceSetters referenceDataService = new ReferenceDataServiceSetters();
		referenceDataService.setMapClassCodes("53576-5;74468-0;");
		referenceDataService.setMapTypeCodes("001;004;");
		referenceDataService.setDefaultClassCode("001");
		referenceDataService.setClassCodeCodes("001;002;003;004;");
		referenceDataService.setClassCodeNames("Clinical report;Personal information about;Planning;Questionnaire;");
		referenceDataService.setClassCodeScheme("1.2.208.184.100.9");
		return referenceDataService;
	}
			
}

class ReferenceDataServiceSetters extends ReferenceDataService {

	
	public void setMapTypeCodes(String mapTypeCodes) {
		this.mapTypeCodes = mapTypeCodes;
	}

	public void setMapClassCodes(String mapClassCodes) {
		this.mapClassCodes = mapClassCodes;
	}

	public void setClassCodeScheme(String classCodeScheme) {
		this.classCodeScheme = classCodeScheme;
	}

	public void setClassCodeCodes(String classCodeCodes) {
		this.classCodeCodes = classCodeCodes;
	}

	public void setClassCodeNames(String classCodeNames) {
		this.classCodeNames = classCodeNames;
	}

	public void setDefaultClassCode(String defaultClassCode) {
		this.defaultClassCode = defaultClassCode;
	}

}


