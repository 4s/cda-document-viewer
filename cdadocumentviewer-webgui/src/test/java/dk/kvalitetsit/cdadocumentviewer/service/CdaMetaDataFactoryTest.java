package dk.kvalitetsit.cdadocumentviewer.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

import dk.kvalitetsit.cdadocumentviewer.dto.DocumentMetadata;

public class CdaMetaDataFactoryTest {

	@Test
	public void testcreateFromCdaRegistrationRequest_creationTime_starttime_stoptime_other_timezone() throws IOException {
		//Given
		ReferenceDataService referenceDataService = ReferenceDataServiceTestHelper.getSimpleReferenceData();
		
		String document = new String(Files.readAllBytes(Paths.get("src/test/resources/DK-APD_Example_1_anden_tidszone.xml")),StandardCharsets.UTF_8);
		CdaMetaDataFactory cdaMetaDataFactory = new CdaMetaDataFactory(referenceDataService);
		
		//When
		DocumentMetadata documentMetadata = cdaMetaDataFactory.createFromCdaRegistrationRequest(null, document);
		
		//Then
		//creationTime
		assertNotNull(documentMetadata.getReportTimeStringUTC());
		assertEquals("20170113060000", documentMetadata.getReportTimeStringUTC());

		//serviceStartTime+serviceStopTime
		assertNotNull(documentMetadata.getServiceStartTimeStringUTC());
		assertEquals("20170530070000", documentMetadata.getServiceStartTimeStringUTC());

		assertNotNull(documentMetadata.getServiceStopTimeStringUTC());
		assertEquals("20170530080000", documentMetadata.getServiceStopTimeStringUTC());
	}
	
	@Test
	public void testcreateFromCdaRegistrationRequest_uniqueIdisCorrect() throws IOException {
		//Given
		ReferenceDataService referenceDataService = ReferenceDataServiceTestHelper.getSimpleReferenceData();

		String document = new String(Files.readAllBytes(Paths.get("src/test/resources/DK-APD_Example_1_anden_tidszone.xml")),StandardCharsets.UTF_8);
		CdaMetaDataFactory cdaMetaDataFactory = new CdaMetaDataFactory(referenceDataService);

		//When
		DocumentMetadata documentMetadata = cdaMetaDataFactory.createFromCdaRegistrationRequest(null, document);

		//Then
		//creationTime
		assertNotNull(documentMetadata.getUniqueId());
		assertEquals("1.2.208.184^aa2386d0-79ea-11e3-981f-0800200c9a66", documentMetadata.getUniqueId());

	}

}
