package dk.kvalitetsit.cdadocumentviewer.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class DropDownListServiceTest {

	@Test
	public void testCreateDropDownListService() {
		
		//Given
		String codes = "code1;code2;code3";
		String names = "name1;name2;name3";
		
		//When
		DropDownListService dropDownListService = new DropDownListService(codes, names, null);
		
		//Then
		assertNotNull(dropDownListService);
		assertEquals(3, dropDownListService.getDropDownListValues().size());
		assertEquals("name1", dropDownListService.getNameFromCode("code1"));
	}

	@Test
	public void testCreateDropDownListServiceWhenEmpty() {

		//Given
		String codes = "";
		String names = "";

		//When
		DropDownListService dropDownListService = new DropDownListService(codes, names, null);

		//Then
		assertNotNull(dropDownListService);
		assertEquals(0, dropDownListService.getDropDownListValues().size());

	}
}
