package dk.kvalitetsit.cdadocumentviewer.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.BeanFactory;

import dk.kvalitetsit.cdadocumentviewer.configuration.RepositoryProfileIti41Configuration;
import dk.kvalitetsit.cdadocumentviewer.configuration.RepositoryProfileIti41Configuration.Repo;

public class RepositoryProfileIti41ServiceTest {
	
	BeanFactory beanFactory = Mockito.mock(BeanFactory.class);
	RepositoryProfileIti41Configuration repositoryProfileIti41Configuration;
	String endpoint1 = "http://test1-cnsp.ekstern-test.nspop.dk:8080/drs/proxy";
	String endpoint2 = "http://kih.test.xdsrepositoryb.medcom.dk:8020/axis2/services/xdsrepositoryb";
	String endpoint3 = "https://test1-cnsp.ekstern-test.nspop.dk:8443/dros/iti41";
	String endpoint4 = "https://test1-cnsp.ekstern-test.nspop.dk:8443/dros/iti57";
	String name1 = "NSP-DRS";
	String name2 = "KIH";
	String name3 = "DROS";
	String repositoryuniqueid1 = "id1";
	String repositoryuniqueid2 = "id2";
	String repositoryuniqueid3 = "id3";
	String homeCommunityId1 = "hid1";
	String homeCommunityId2 = "hid2";
	String homeCommunityId3 = "hid3";
	String validDocType2_1 = "74465-6";
	String validDocType2_2 = "39289-4";
	String validDocType2_3 = "39289-3";
	
	
	@Before
	public void prepareTest() {
		repositoryProfileIti41Configuration = new RepositoryProfileIti41Configuration();
		
		Repo repo = new Repo(name1, endpoint1, true, endpoint4, true, "code", repositoryuniqueid1, homeCommunityId1, true, true, true);
		repositoryProfileIti41Configuration.addRepo(repo);
		
		repo = new Repo(name2, endpoint2, false, endpoint4, true, repositoryuniqueid2, "code", homeCommunityId2, true, false, false);
		repo.addValidDoctype(validDocType2_1);
		repo.addValidDoctype(validDocType2_2);
		repositoryProfileIti41Configuration.addRepo(repo);
		
		repo = new Repo(name3, endpoint3, false, endpoint4, true, repositoryuniqueid3, "code", homeCommunityId3, false, false, false);
		repo.addValidDoctype(validDocType2_1);
		repo.addValidDoctype(validDocType2_2);
		repositoryProfileIti41Configuration.addRepo(repo);

	}
	
	@Test
	public void testCreateRepositoryProfileIti41Service() {
		
		// Given

		// When
		RepositoryProfileIti41Service repositoryProfileIti41Service = new RepositoryProfileIti41Service(beanFactory, repositoryProfileIti41Configuration);
		
		// Then
		assertNotNull(repositoryProfileIti41Service);
		assertNotNull(repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload());
		assertNotNull(repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues());
		assertEquals(2, repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().size());		
		assertNotNull(repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().get(0));
		assertNotNull(repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().get(0).getCode());
		assertEquals(endpoint2, repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().get(0).getCode());
		assertEquals(name2, repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().get(0).getName());
		assertEquals(endpoint1, repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().get(1).getCode());
		assertEquals(name1, repositoryProfileIti41Service.getRepositoryAsDropDownListForUpload().getDropDownListValues().get(1).getName());

	}
	
}