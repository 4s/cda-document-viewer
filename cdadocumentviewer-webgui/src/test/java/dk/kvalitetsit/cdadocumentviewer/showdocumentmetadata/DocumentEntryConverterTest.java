package dk.kvalitetsit.cdadocumentviewer.showdocumentmetadata;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.joda.time.DateTime;
import org.junit.Test;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssigningAuthority;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Author;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AvailabilityStatus;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Code;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentAvailability;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntry;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.DocumentEntryType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Identifiable;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.LocalizedString;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Organization;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.PatientInfo;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Person;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.ReferenceId;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.Version;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.XpnName;

public class DocumentEntryConverterTest {
	
	private DocumentEntryConverter subject = new DocumentEntryConverter();
	
	private static final Code CLASS_CODE = new Code("001", new LocalizedString("Klinisk rapport"), "1.2.208.184.100.9");
	private static final Code CONFIDENTIALITY_CODE =  new Code("N", new LocalizedString("N"), "2.16.840.1.113883.5.25");
	private static final Code FORMAT_CODE_AFTALEDOKUMENT = new Code("urn:ad:dk:medcom:appointmentsummary:full", new LocalizedString("DK Appointment Summary Document schema"), "1.2.208.184.100.10");
	private static final Code HEALTHCAREFACILITYTYPE_CODE = new Code("22232009", new LocalizedString("hospital"), "2.16.840.1.113883.6.96");
	private static final Code PRACTICE_SETTING_CODE = new Code("408443003", new LocalizedString("almen medicin"), "2.16.840.1.113883.6.96");
	private static final Code TYPE_CODE_AFTALEDOKUMENT = new Code("39289-4", new LocalizedString("Dato og tidspunkt for møde mellem patient og sundhedsperson"), "2.16.840.1.113883.6.1");
	private static final ReferenceId REFERENCE_ID_1 = new ReferenceId("foo", new AssigningAuthority("1.2.208.176.1.2"), "urn:ihe:iti:xds:2013:uniqueId", new AssigningAuthority("1.2.208.176.1.1"));
	private static final ReferenceId REFERENCE_ID_2 = new ReferenceId("baz", new AssigningAuthority("2.16.840.1.113883.6.1"), "urn:ihe:iti:xds:2013:accession", null);
	private static final Code EVENT_CODE_1 = new Code("EC1", new LocalizedString("foo1"), "bar1");
	private static final Code EVENT_CODE_2 = new Code("EC2", new LocalizedString("foo2"), "bar2");
	
	@Test
	public void testFilledInValues() throws IOException {
		
		//given
		DocumentEntry documentEntry = createDocumentEntry();
		
		//when
		DocumentMetadata unparsedData  = subject.createShowDocumentMetadata(documentEntry);
		String documentEntryAsString = subject.getJsonBase64EncodedFromDocumentEntry(unparsedData);
		DocumentMetadata parsedData  = subject.getShowDocumentMetadataFromJsonBase64Encoded(documentEntryAsString);
		
		//then
		assertEquals(unparsedData.toString(), parsedData.toString());
		assertEquals("1960-04-14", parsedData.getSourcePatientInfo().getDateOfBirth());

	}
	
	@Test
	public void testNullValues() throws IOException {
		
		//given
		DocumentEntry documentEntry = new DocumentEntry();
		
		//when
		subject.createShowDocumentMetadata(documentEntry);
		
		//then
		//no null pointer
		
	}
	
	private DocumentEntry createDocumentEntry() {
		
		DocumentEntry documentEntry = new DocumentEntry();
		
		documentEntry.getAuthors().add(createAuthor("Anna", "Louise", "Hansen"));
		documentEntry.getAuthors().add(createAuthor("Per", "Arne", "Larsen"));
		
		documentEntry.setAvailabilityStatus(AvailabilityStatus.APPROVED);
		documentEntry.setClassCode(CLASS_CODE);
		documentEntry.setComments(new LocalizedString("Comments på dokument"));
		documentEntry.getConfidentialityCodes().add(CONFIDENTIALITY_CODE);
		documentEntry.setCreationTime(DateTime.parse("2021-10-11T13:00:00.000000+01:00"));
		documentEntry.setDocumentAvailability(DocumentAvailability.ONLINE);
		documentEntry.setEntryUuid(generateUUIDwithPrefix());
		documentEntry.getEventCodeList().add(EVENT_CODE_1);
		documentEntry.getEventCodeList().add(EVENT_CODE_2);
		documentEntry.setExtraMetadata(null);
		documentEntry.setFormatCode(FORMAT_CODE_AFTALEDOKUMENT);
		documentEntry.setHash("51abb9636078defbf888d8457a7c76f85c8f114c");
		documentEntry.setHealthcareFacilityTypeCode(HEALTHCAREFACILITYTYPE_CODE);
		documentEntry.setHomeCommunityId("2.2.222.222.2.2");
		documentEntry.setLanguageCode("da-DK");
		
		documentEntry.setLegalAuthenticator(createPerson("Tanja", null, "Jensen"));
		documentEntry.setLogicalUuid(generateUUIDwithPrefix());
		documentEntry.setMimeType("text/xml");
		
		documentEntry.setType(DocumentEntryType.STABLE);
		
		documentEntry.setPracticeSettingCode(PRACTICE_SETTING_CODE);
		
		AssigningAuthority assigningAuthority = new AssigningAuthority("1.2.208.176.1.2");
		documentEntry.setPatientId(new Identifiable("1234567890", assigningAuthority));
		
		
		documentEntry.getReferenceIdList().add(REFERENCE_ID_1);
		documentEntry.getReferenceIdList().add(REFERENCE_ID_2);
		
		documentEntry.setRepositoryUniqueId("1.1.111.111.1.1");
		documentEntry.setServiceStartTime(DateTime.parse("2021-10-11T13:00:00.000000+01:00"));
		documentEntry.setServiceStopTime(DateTime.parse("2021-10-11T14:00:00.000000+01:00"));
		documentEntry.setSize(1000L);
		
		documentEntry.setSourcePatientId(new Identifiable("1234567891", assigningAuthority));
        PatientInfo sourcePatientInfo = new PatientInfo();
        sourcePatientInfo.setDateOfBirth(DateTime.parse("1960-04-14T00:00:00.000000+01:00"));
        sourcePatientInfo.setName(createName("Søren", "Peter", "Andersen", null));
        sourcePatientInfo.setGender("M");
        documentEntry.setSourcePatientInfo(sourcePatientInfo);

		
		documentEntry.setTitle(new LocalizedString("Titel på dokument"));
		documentEntry.setType(DocumentEntryType.STABLE);
		
		documentEntry.setTypeCode(TYPE_CODE_AFTALEDOKUMENT);
		documentEntry.setUniqueId(generateUUID());
		documentEntry.setUri("uri");
		documentEntry.setVersion(new Version(Integer.toString(1)));
		
		return documentEntry;
		

	}
	
	private Author createAuthor(String given, String second, String familiy) {
		Author author = new Author();
		author.setAuthorPerson(createPerson(given, second, familiy));
		
		Organization org = new Organization("Testafdeling");
		org.setIdNumber("123456789");
		org.setAssigningAuthority(new AssigningAuthority("1.2.208.176.1.1"));
		return author;
	}
	
	private Person createPerson(String given, String second, String familiy) {
		Person person = new Person();
		person.setName(createName(given, second, familiy, null));
		return person;
	}
	
	private XpnName createName(String given, String second, String familiy, String prefix) {
        XpnName xpnName = new XpnName();
        xpnName.setGivenName(given);
        xpnName.setSecondAndFurtherGivenNames(second);
        xpnName.setFamilyName(familiy);
        xpnName.setPrefix(prefix);
        return xpnName;
	}


	public String generateUUIDwithPrefix() {
		return "urn:uuid:" + generateUUID();

	}
	public String generateUUID() {
		java.util.UUID uuid = java.util.UUID.randomUUID();
		return uuid.toString();

	}
	
}
