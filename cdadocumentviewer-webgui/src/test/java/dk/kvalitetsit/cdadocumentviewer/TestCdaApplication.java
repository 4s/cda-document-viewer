package dk.kvalitetsit.cdadocumentviewer;
import org.springframework.boot.SpringApplication;
import org.testcontainers.containers.BindMode;
import org.testcontainers.containers.GenericContainer;

public class TestCdaApplication extends Application {

	private static final int REDIS_PORT = 6379;

 	
	public static void main(String[] args) {

		GenericContainer<?> redis = new GenericContainer("redis:3.0.6")
				.withExposedPorts(REDIS_PORT)
				.withCommand("redis-server /usr/local/etc/redis/redis.conf")
				.withClasspathResourceMapping("redis.conf", "/usr/local/etc/redis/redis.conf", BindMode.READ_ONLY);
		redis.start();
		
		Integer mappedRedisPort = redis.getMappedPort(REDIS_PORT);
		System.setProperty("spring.redis.host", "localhost");
		System.setProperty("spring.redis.port", mappedRedisPort.toString());
		
		SpringApplication.run(TestCdaApplication.class, args);
	}
}
