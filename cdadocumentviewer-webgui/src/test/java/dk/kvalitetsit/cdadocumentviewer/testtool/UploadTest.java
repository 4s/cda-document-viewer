package dk.kvalitetsit.cdadocumentviewer.testtool;

import dk.kvalitetsit.cdadocumentviewer.log.LogEntry;
import dk.kvalitetsit.cdadocumentviewer.log.LogEntryService;
import dk.kvalitetsit.cdadocumentviewer.service.SearchProfile;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLFactory30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.EbXMLProvideAndRegisterDocumentSetRequest30;
import org.openehealth.ipf.commons.ihe.xds.core.ebxml.ebxml30.ProvideAndRegisterDocumentSetRequestType;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.AssigningAuthority;
import org.openehealth.ipf.commons.ihe.xds.core.metadata.ReferenceId;
import org.openehealth.ipf.commons.ihe.xds.core.requests.ProvideAndRegisterDocumentSet;
import org.openehealth.ipf.commons.ihe.xds.core.stub.ebrs30.rs.RegistryResponseType;
import org.openehealth.ipf.commons.ihe.xds.core.transform.requests.ProvideAndRegisterDocumentSetTransformer;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;


import static org.junit.Assert.*;
@Ignore
public class UploadTest {
    // denne klasse anvendes til at lave test data, man kan hente i en kørende cda viewer. Sådan at man 100 % ved hvad data der er uploaded.
    // Udgangspunktet er et tidligere request, som kan "beriges" yderligere.

    private String endpoint = "https://test1-cnsp.ekstern-test.nspop.dk:8443/dros/iti41";

    @Test
    public void test2() throws IOException, SOAPException, JAXBException {

        //given
        String fileName = "/iti41request/request1.xml";

        ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet = getProvideAndRegisterDocumentSet(fileName);
        provideAndRegisterDocumentSet = setNewIdValues(provideAndRegisterDocumentSet);


        String CPR_OID = "1.2.208.176.1.2";
        String SOR_OID = "1.2.208.176.1.1";

        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().
                getReferenceIdList().add(new ReferenceId("qux1", new AssigningAuthority(CPR_OID), "urn:ihe:iti:xds:2013:uniqueId", new AssigningAuthority(SOR_OID)));
        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().
                getReferenceIdList().add(new ReferenceId("qux2", new AssigningAuthority(CPR_OID), "urn:ihe:iti:xds:2013:uniqueId", new AssigningAuthority(SOR_OID)));

        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().setUri("https://www.medcom.dk");

        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().getPatientId();
        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().getSourcePatientId();


        ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetRequestType = getProvideAndRegisterDocumentSetRequestType(provideAndRegisterDocumentSet);
        Iti41PortType iti41Client = new Iti41UploadClient(endpoint, getSearchProfileMock(), getLogEntryService());

        //when
        RegistryResponseType registryResponse = iti41Client.documentRepositoryProvideAndRegisterDocumentSetB(provideAndRegisterDocumentSetRequestType);

        //then
        assertNotNull(registryResponse);
        System.out.println("Patientid: " + provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().getPatientId());
        System.out.println("Uniqueid: " + provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().getUniqueId());

    }

    private ProvideAndRegisterDocumentSet setNewIdValues(ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet) {
        String submissionId = generateUUIDAdjusted();
        provideAndRegisterDocumentSet.getSubmissionSet().setEntryUuid(submissionId);
        provideAndRegisterDocumentSet.getSubmissionSet().setUniqueId(submissionId);
        provideAndRegisterDocumentSet.getSubmissionSet().setSourceId(submissionId);

        String documentEntryId = generateUUIDwithPrefix();
        String documentEntryUniqueId = generateUUIDAdjusted();
        assertTrue(provideAndRegisterDocumentSet.getDocuments().size() < 2);
        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().setEntryUuid(documentEntryId);
        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().setUniqueId(documentEntryUniqueId);
        provideAndRegisterDocumentSet.getDocuments().get(0).getDocumentEntry().setLogicalUuid(documentEntryId);

        assertTrue(provideAndRegisterDocumentSet.getAssociations().size() < 2);
        provideAndRegisterDocumentSet.getAssociations().get(0).setEntryUuid(generateUUID());
        provideAndRegisterDocumentSet.getAssociations().get(0).setSourceUuid(submissionId);
        provideAndRegisterDocumentSet.getAssociations().get(0).setTargetUuid(documentEntryId);

        return provideAndRegisterDocumentSet;

    }

    private String generateUUID() {
        java.util.UUID uuid = java.util.UUID.randomUUID();
        return uuid.toString();
    }
    public String generateUUIDwithPrefix() {
        java.util.UUID uuid = java.util.UUID.randomUUID();
        return "urn:uuid:" + uuid.toString();
    }

    private String generateUUIDAdjusted() {
        java.util.UUID uuid = java.util.UUID.randomUUID();
        return Math.abs(uuid.getLeastSignificantBits()) + "." + Math.abs(uuid.getMostSignificantBits())+"."+ Calendar.getInstance().getTimeInMillis();
    }
    private LogEntryService getLogEntryService() {
        LogEntryService logEntryServiceMock = Mockito.mock(LogEntryService.class);
        LogEntry logEntry = new LogEntry();
        logEntry.setId("id");
        Mockito.when(logEntryServiceMock.saveLog(Mockito.anyString(), Mockito.anyString())).thenReturn(logEntry);
        return logEntryServiceMock;
    }
    private SearchProfile getSearchProfileMock() {
        SearchProfile searchProfileMock = Mockito.mock(SearchProfile.class);
        return searchProfileMock;
    }

    private ProvideAndRegisterDocumentSetRequestType getProvideAndRegisterDocumentSetRequestType(ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet) {
        ProvideAndRegisterDocumentSetTransformer transformer = new ProvideAndRegisterDocumentSetTransformer(new EbXMLFactory30());
        EbXMLProvideAndRegisterDocumentSetRequest30 ebXMLRequest = (EbXMLProvideAndRegisterDocumentSetRequest30) transformer.toEbXML(provideAndRegisterDocumentSet);
        return ebXMLRequest.getInternal();
    }
    private ProvideAndRegisterDocumentSet getProvideAndRegisterDocumentSet(String fileName) throws SOAPException, JAXBException, IOException {
        InputStream inputStream = UploadTest.class.getResourceAsStream(fileName);
        ProvideAndRegisterDocumentSetRequestType provideAndRegisterDocumentSetInput = parseNspSoapResponse(inputStream, ProvideAndRegisterDocumentSetRequestType.class);

        ProvideAndRegisterDocumentSetTransformer provideAndRegisterDocumentSetTransformer = new ProvideAndRegisterDocumentSetTransformer(new EbXMLFactory30());
        EbXMLProvideAndRegisterDocumentSetRequest30 ebXMLProvideAndRegisterDocumentSetRequest30 = new EbXMLProvideAndRegisterDocumentSetRequest30(provideAndRegisterDocumentSetInput);
        ProvideAndRegisterDocumentSet provideAndRegisterDocumentSet = provideAndRegisterDocumentSetTransformer.fromEbXML(ebXMLProvideAndRegisterDocumentSetRequest30);
        assertNotNull(provideAndRegisterDocumentSet);
        assertNotNull(provideAndRegisterDocumentSet.getSubmissionSet());
        Assert.assertEquals(1, provideAndRegisterDocumentSet.getDocuments().size());
        Assert.assertEquals(1, provideAndRegisterDocumentSet.getAssociations().size());
        Assert.assertEquals(0, provideAndRegisterDocumentSet.getFolders().size());

        return provideAndRegisterDocumentSet;
    }
    private ProvideAndRegisterDocumentSetRequestType parseNspSoapResponse(InputStream responseAdjusted, Class<ProvideAndRegisterDocumentSetRequestType> c) throws SOAPException, IOException, JAXBException {
        final Document message = MessageFactory.newInstance().createMessage(null, responseAdjusted).getSOAPBody().extractContentAsDocument();
        return JAXBContext.newInstance(c).createUnmarshaller().unmarshal(message, c).getValue();
    }
}
