package dk.kvalitetsit.cdadocumentviewer.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.mockito.Mockito;
import org.openehealth.ipf.commons.ihe.xds.iti41.Iti41PortType;
import org.openehealth.ipf.commons.ihe.xds.iti57.Iti57PortType;

public class RepositoryProfileIti41DtoTest {
	
	@Test
	public void testCreateNewRepositoryProfileIti41Dto() {
		
		// Given
				
		// When
		RepositoryProfileIti41Dto repositoryProfileIti41Dto = new RepositoryProfileIti41Dto();
		
		// Then
		assertNotNull(repositoryProfileIti41Dto);
		assertNotNull(repositoryProfileIti41Dto.getValidDocTypes());
		assertEquals(0,  repositoryProfileIti41Dto.getValidDocTypes().size());
		
	}
	
	@Test
	public void testGetterAndSettersRepositoryProfileIti41Dto() {
		
		// Given
		RepositoryProfileIti41Dto repositoryProfileIti41Dto = new RepositoryProfileIti41Dto();
		Iti41PortType iti41PortType = Mockito.mock(Iti41PortType.class);
		Iti57PortType iti57PortType = Mockito.mock(Iti57PortType.class);
		String endpoint = "TestEndpoint";
		String endpoint2 = "TestEndpoint2";
		String name = "RepositoryName";
		String repositoryuniqueid = "repositoryuniqueid";
		String homeCommunityId  = "homeCommunityId "; 
				
		// When
		repositoryProfileIti41Dto.setDgws(true);
		repositoryProfileIti41Dto.setEndpoint(endpoint);
		repositoryProfileIti41Dto.setDgwsIti57(true);
		repositoryProfileIti41Dto.setEndpointIti57(endpoint2);
		repositoryProfileIti41Dto.setBackendCode("code");
		repositoryProfileIti41Dto.setRepositoryuniqueid(repositoryuniqueid);
		repositoryProfileIti41Dto.setHomeCommunityId(homeCommunityId);
		repositoryProfileIti41Dto.setUploadallowed(true);
		repositoryProfileIti41Dto.setUpdateallowed(true);
		repositoryProfileIti41Dto.setDeprecateallowed(true);
		repositoryProfileIti41Dto.setName(name);
		repositoryProfileIti41Dto.addDocType("DocType01");
		repositoryProfileIti41Dto.addDocType("DocType02");
		repositoryProfileIti41Dto.setIti41PortType(iti41PortType);
		repositoryProfileIti41Dto.setIti57PortType(iti57PortType);

		// Then
		assertNotNull(repositoryProfileIti41Dto);
		assertNotNull(repositoryProfileIti41Dto.isDgws());
		assertEquals(true,  repositoryProfileIti41Dto.isDgws());
		assertNotNull(repositoryProfileIti41Dto.getEndpoint());
		assertEquals(endpoint,  repositoryProfileIti41Dto.getEndpoint());
		assertNotNull(repositoryProfileIti41Dto.isDgwsIti57());
		assertEquals(true,  repositoryProfileIti41Dto.isDgwsIti57());
		assertNotNull(repositoryProfileIti41Dto.getEndpointIti57());
		assertEquals(endpoint2,  repositoryProfileIti41Dto.getEndpointIti57());
		assertNotNull(repositoryProfileIti41Dto.isDgwsIti57());
		assertNotNull(repositoryProfileIti41Dto.getBackendCode());
		assertEquals("code",  repositoryProfileIti41Dto.getBackendCode());
		assertNotNull(repositoryProfileIti41Dto.getRepositoryuniqueid());
		assertEquals(repositoryuniqueid,  repositoryProfileIti41Dto.getRepositoryuniqueid());
		assertNotNull(repositoryProfileIti41Dto.getHomeCommunityId());
		assertEquals(homeCommunityId,  repositoryProfileIti41Dto.getHomeCommunityId());
		assertNotNull(repositoryProfileIti41Dto.isUploadallowed());
		assertEquals(true,  repositoryProfileIti41Dto.isUploadallowed());
		assertNotNull(repositoryProfileIti41Dto.isUpdateallowed());
		assertEquals(true,  repositoryProfileIti41Dto.isUpdateallowed());
		assertNotNull(repositoryProfileIti41Dto.isdeprecateallowed());
		assertEquals(true,  repositoryProfileIti41Dto.isdeprecateallowed());
		assertNotNull(repositoryProfileIti41Dto.getName());
		assertEquals(name,  repositoryProfileIti41Dto.getName());
		assertNotNull(repositoryProfileIti41Dto.getValidDocTypes());
		assertEquals(2,  repositoryProfileIti41Dto.getValidDocTypes().size());
		assertNotNull(repositoryProfileIti41Dto.getIti41PortType());
		assertNotNull(repositoryProfileIti41Dto.getIti57PortType());
	}

}
