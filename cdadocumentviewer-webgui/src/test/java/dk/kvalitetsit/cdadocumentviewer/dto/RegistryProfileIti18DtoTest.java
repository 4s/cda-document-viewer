package dk.kvalitetsit.cdadocumentviewer.dto;

import org.junit.Test;
import org.mockito.Mockito;
import org.openehealth.ipf.commons.ihe.xds.iti18.Iti18PortType;
import org.openehealth.ipf.commons.ihe.xds.iti43.Iti43PortType;

import static org.junit.Assert.*;

public class RegistryProfileIti18DtoTest {

    @Test
    public void testCreateNewRegistryProfileIti18Dto() {

        // Given

        // When
        RegistryProfileIti18Dto registryProfileIti18Dto = new RegistryProfileIti18Dto();

        // Then
        assertNotNull(registryProfileIti18Dto);

    }

    @Test
    public void testGetterAndSettersRegistryProfileIti18Dto() {

        // Given
        RegistryProfileIti18Dto registryProfileIti18Dto = new RegistryProfileIti18Dto();
        Iti18PortType iti18PortType = Mockito.mock(Iti18PortType.class);
        Iti43PortType iti43PortType = Mockito.mock(Iti43PortType.class);
        String endpoint = "TestEndpoint";
        String endpoint2 = "TestEndpoint2";
        String name = "RegistryName";


        // When
        registryProfileIti18Dto.setDgws(true);
        registryProfileIti18Dto.setEndpoint(endpoint);
        registryProfileIti18Dto.setDgwsIti43(true);
        registryProfileIti18Dto.setEndpointIti43(endpoint2);
        registryProfileIti18Dto.setBackendCode("code");

        registryProfileIti18Dto.setName(name);
        registryProfileIti18Dto.setIti18PortType(iti18PortType);
        registryProfileIti18Dto.setIti43PortType(iti43PortType);

        registryProfileIti18Dto.setTypeCodeCodes("typeCodeCodes");
        registryProfileIti18Dto.setTypeCodeNames("typeCodeNames");
        registryProfileIti18Dto.setTypeCodeSchemes("typeCodeSchemes");
        registryProfileIti18Dto.setEventCodeSchemeCodes("eventCodeSchemeCodes");
        registryProfileIti18Dto.setEventCodeSchemeNames("eventCodeSchemeNames");
        registryProfileIti18Dto.setFormatCodeCodes("formatCodeCodes");
        registryProfileIti18Dto.setFormatCodeNames("formatCodeNames");
        registryProfileIti18Dto.setHealthcareFacilityTypeCodeCodes("healthcareFacilityTypeCodeCodes");
        registryProfileIti18Dto.setHealthcareFacilityTypeCodeNames("healthcareFacilityTypeCodeNames");
        registryProfileIti18Dto.setPracticeSettingCodeCodes("practiceSettingCodeCodes");
        registryProfileIti18Dto.setPracticeSettingCodeNames("practiceSettingCodeNames");

        registryProfileIti18Dto.setDefaultTypeStable(true);
        registryProfileIti18Dto.setDefaultTypeOnDemand(false);


        // Then
        assertNotNull(registryProfileIti18Dto);
        assertEquals(true,  registryProfileIti18Dto.isDgws());
        assertEquals(endpoint,  registryProfileIti18Dto.getEndpoint());
        assertEquals(true,  registryProfileIti18Dto.isDgwsIti43());
        assertEquals(endpoint2,  registryProfileIti18Dto.getEndpointIti43());
        assertEquals(true,  registryProfileIti18Dto.isDgwsIti43());
        assertEquals("code",  registryProfileIti18Dto.getBackendCode());
        assertEquals(name,  registryProfileIti18Dto.getName());
        assertNotNull(registryProfileIti18Dto.getIti18PortType());
        assertNotNull(registryProfileIti18Dto.getIti43PortType());

        assertEquals("typeCodeCodes",  registryProfileIti18Dto.getTypeCodeCodes());
        assertEquals("typeCodeNames",  registryProfileIti18Dto.getTypeCodeNames());
        assertEquals("typeCodeSchemes",  registryProfileIti18Dto.getTypeCodeSchemes());
        assertEquals("eventCodeSchemeCodes",  registryProfileIti18Dto.getEventCodeSchemeCodes());
        assertEquals("eventCodeSchemeNames",  registryProfileIti18Dto.getEventCodeSchemeNames());
        assertEquals("formatCodeCodes",  registryProfileIti18Dto.getFormatCodeCodes());
        assertEquals("formatCodeNames",  registryProfileIti18Dto.getFormatCodeNames());
        assertEquals("healthcareFacilityTypeCodeCodes",  registryProfileIti18Dto.getHealthcareFacilityTypeCodeCodes());
        assertEquals("healthcareFacilityTypeCodeNames",  registryProfileIti18Dto.getHealthcareFacilityTypeCodeNames());
        assertEquals("practiceSettingCodeCodes",  registryProfileIti18Dto.getPracticeSettingCodeCodes());
        assertEquals("practiceSettingCodeNames",  registryProfileIti18Dto.getPracticeSettingCodeNames());

        assertTrue(registryProfileIti18Dto.isDefaultTypeStable());
        assertFalse(registryProfileIti18Dto.isDefaultTypeOnDemand());


    }

}
