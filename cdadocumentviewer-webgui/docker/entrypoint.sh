#! /bin/ash
if [ "$CONTAINER_TIMEZONE" = "" ]
then
   echo "Using default timezone"
else
	
	TZFILE="/usr/share/zoneinfo/$CONTAINER_TIMEZONE"
	if [ ! -e "$TZFILE" ]
	then 
    	echo "requested timezone $CONTAINER_TIMEZONE doesn't exist"
	else
		cp /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime
		echo "$CONTAINER_TIMEZONE" > /etc/timezone
		echo "using timezone $CONTAINER_TIMEZONE"
	fi
fi

if [[ -z $SERVER_PORT ]]; then
  echo "Default SERVER_PORT = 8080"
  export SERVER_PORT=8080
fi

if [[ -z $LOG_LEVEL ]]; then
  echo "Default LOG_LEVEL = INFO"
  export LOG_LEVEL=INFO
fi

if [[ -z $SPRING_REDIS_HOST ]]; then
  echo "Default SPRING_REDIS_HOST = localhost"
  export SPRING_REDIS_HOST=localhost
fi

if [[ -z $SPRING_REDIS_PORT ]]; then
  echo "Default SPRING_REDIS_PORT = 6379"
  export SPRING_REDIS_PORT=6379
fi

if [[ -z $CORRELATION_ID ]]; then
  echo "Default CORRELATION_ID = correlation-id"
  export CORRELATION_ID=correlation-id
fi

if [[ -z $SERVICE_ID ]]; then
  echo "Default SERVICE_ID = cdadocumentviewer-webgui"
  export SERVICE_ID=cdadocumentviewer-webgui
fi

if [[ -z $ITI_41_DGWS_ENABLED ]]; then
  export ITI_41_DGWS_ENABLED=false
fi

envsubst < /cdadocumentviewer/template/application.properties > /app/application.properties
envsubst < /cdadocumentviewer/template/logback.xml > /app/logback-spring.xml

echo "Jvm options is $JVM_OPTS"

java $JVM_OPTS -jar cdadocumentviewer-webgui.war
