# CDA Document Viewer

Dette dokument beskriver applikationen "CDA Document Viewer", en web-app  beregnet til at fremsøge, vise og uploade CDA dokumenter via Dokument-delings-servicen (herefter DDS). 

# Beskrivelse af CDA Document viewer´s brugergrænseflade

CDA Document viewer starter på søgesiden

## Søgesiden

![alt text](/screenshots/screenshot_search.png)

Her er det øverst muligt at indtaste en eller flere søge-parametre til fremsøgning af CDA-dokumenter:

- Patient ID
- Unique ID (dette er et filter)
- Type Code
- Format Code
- Healthcare Facility Type Code
- Event Code (code og scheme)
- Practice Setting Code
- Dokument type (man kan vælge stable og/eller on-demand, bemærk at hvis man ikke vælger nogen fremsøges som default stable dokumenter)
- Service Start (angives som interval mellem to tidspunkter, bemærk det er lovligt at have et opad eller nedadtil ubegrænset interval)
- Service Stop (angives som interval mellem to tidspunkter, bemærk det er lovligt at have et opad eller nedadtil ubegrænset interval)
- Availability Status

Filter-parametre filtreres der på, efter søgningen er udført. Dvs. parametren er ikke en del af søgningen. Derfor vil fejlbeskeder og download af request og response ikke tage højde for denne parameter.

Oprindelig var det kun muligt at søge vha. dokumentdelingsservicen. I dag kan man vælge mellem flere backend i listen "Søg i". Når man vælger en backend, vil søge parametrene blive justeret, så de passer til den valgte backend.  

Pt er det kun muligt at søge som læge Charles Babbage". Man kan angive om søgning skal være med eller uden værdispring.

Efter indtastning af en eller flere parametre trykkes der "Søg" for at foretage en søgning op imod dokumentdelings-servicen. Hvis søgningen returnerer en fejl eller advarsel (f.eks ufuldstændigt resultat pga samtykkeregler og/eller et system ikke er tilgængeligt), vises denne over og nedenunder søgeparametrene i en orange kasse. Søgeresultaterne vises i en tabel nedenunder søgeparametrene.

I søgeresultat kan man for hvert dokument udføre en række operationer ved at trykke på det respektive link:

- Vis: her kommer man til dokumentvisningssiden og kan se dokumentet og dets meta data
- Ret: her startes et dokument upload via upload siden. Det nye upload dokument erstatter det dokument, man retter.
- Deprecate: her deprecates dokumnentet

For "Ret" og "Deprecate" gælder det, at det kun er tilgængelig, hvis det er konfigureret for det pågældende dokuments repository.

## Dokumentvisningssiden

![alt text](/screenshots/screenshot_documentview.png)

Under overskriften "DocumentEntry Metadata" vil der blive vist en tabel indeholdende de attributer, der findes på DocumentEntry objektet i XDS´en. Under disse vises selve dokument-XML´en, og der er allernederst på siden en knap "Hent Dokument" til at downloade dokument-XML´en som en fil.

Evt. fejlmeddelser vises i orange kasser.

Man kan altid kan påbegynde en ny søgning ved at trykke "Søg" i den øverste bjælke. Eller gå tilbage til fanen man lavede den oprindelige søgning i.

## Upload siden
Upload af et dokument består af flere trin, som startes ved at trykke på linket "Upload" i den øverste bjælke (Opret) eller ved at rette et ekisterende dokument fra søgesidens "ret" link (Ret).

### Valg af dokument og repository

I dette trin får man mulighed for at vælge det dokument, der skal uploades. 
Hvis der er tale om opret af nyt dokument, kan man vælge det repository, der skal uploades til. Ellers er repository givet af det dokument, man erstatter, og kan ikke ændres.
Herefter trykkes på "Preview Upload".
Dokumenttypen på det valgte dokument valideres mod de tilladte dokumenttyper for det valgte repository. Matcher de ikke, må man vælge et andet dokument eller repository.

![alt text](/screenshots/screenshot_upload.jpg)

### Review og angivelse af metadata

I dette trin kan man reviewe de metadata, der er blevet trukket ud af dokumentet. Selve dokumentindholdet kan ses nederst på siden. Man har mulighed for at angive yderligere metadata som healthcarefacilitycode, object type og practicesetting. Herefter trykkes på "upload"

Følgende metadata håndteres specielt:

- Format code tages fra dokumentets metadata, hvis det findes som sådan. Ellers kan en fra en liste vælges
- Class code bestemmes af dokumentets Type code ud fra en konfigureret mapningstabel. Alternativt anvendes en konfigureret default værdi
- Submission Time bestemmes af faktisk tid for upload

![alt text](/screenshots/screenshot_uploadpreview.png)

### Kvittering for upload

Nu er dokumentet uploaded og man får en kvittering med dokumentets id. Der er mulighed for hurtig fremsøgning af dokumentet ved at trykke på "Fremsøg".

![alt text](/screenshots/screenshot_uploadresult.png)

Lykkedes uploaden ikke får man istedet en fejlliste.

![alt text](/screenshots/screenshot_uploaderror.jpg)

## Deprecate siden
Deprecate af et dokument kan gøres ved, at man fra søgesiden vælger linket "deprecate" ud for et dokument. Herefter vises resulatet af deprecate, som en kvittering for at deprecate gik godt.

![alt text](/screenshots/screenshot_deprecateok.png)

Eller alternativt som en fejlbesked, hvis det ikke lykkedes.

## "Om..." siden

Denne side tilgås ved at trykke på Linket "Om..." i den øverste bjælke. På denne side kan man se info om hvilket miljø som CDA Document Viewer er sat til at køre op imod - herunder:

- **ITI-18 url:** Hvilket (backend) endpoint der anvendes til ITI-18 forespørgelser 
- **ITI-43 url:** Hvilket (backend) endpoint der anvendes til ITI-43 forespørgsler
- **ITI-57 url:** Hvilket (dros) endpoint der anvendes til ITI-57 opdateringer 
- **STS url:** Hvilken STS der bruges til at trække ID-kort fra i forbindelse med DGWS-kald
- **ITI-41 url:** Hvilket endpoint der kan anvendes til ITI-41 registreringer

# Afvikling af CDA Document viewer 

CDA Document Viewer bygges som et docker image. Det kan konfigureres ved opstart vha miljøvariable:

| Miljøvariabel                   | Beskrivelse                                                              |
|---------------------------------|--------------------------------------------------------------------------|
| CONTEXT                         | Kontekst-rod for applikationen. Forsiden på webapp vil kunne nås på http://SERVERNAVN:SERVER_PORT/CONTEXT                                                 |
| SERVER_PORT                     | Server-port for applikationen. Denne port skal altså mappes (eller det er denne port en eventuel proxy skal nå applikationen på). Forsiden på webapp vil kunne nås på http://SERVERNAVN:SERVER_PORT/CONTEXT                                                        |
| LOG_LEVEL                       | Log4j-niveau for applikationen. Bør være INFO, og DEBUG i fejlfindings-situationer              |
| **Vedr. fremsøgning**                         |
| FORMAT_CODE_SCHEME                 | Ved fremsøgning af dokumenter, kan der blive angivet en format code, denne konfigurationsvariabel bestemmer scheme for denne code, f.eks. 1.2.208.184.100.10 |
| HEALTHCAREFACILITYTYPE_CODE_SCHEME | Samme som ovenstående, men her f.eks. 2.16.840.1.113883.6.96 |
| PRACTICESETTING_CODE_SCHEME | Samme som ovenstående, men her f.eks. 2.16.840.1.113883.6.96 |
|                                 | For event codes  findes der flere forskellige schemes, som kan vælges fra en liste. Disse konfigureres som en liste af koder og navne i nedenstående variable, hver adskilles med ; |
| event_code_scheme_codes         | Eks: 1.2.208.176.2.1;1.2.208.176.2.4; |
| event_code_scheme_names         | Eks: NPU;Episode of care label; |
| Ekstra metadata:                | Ved søgning kan man angive en række ekstra metadata, som er tilgængelig vha. valg i en liste. Disse konfigureres som en liste af koder og navne i nedenstående variable. Hver element adskilles med ; . Udover Type_CODE_ bruges også variablene fra upload: FORMAT_CODE_, HEALTHCAREFACILITYTYPE_CODE_ og PRACTICESETTING_CODE_ |
| type_code_codes                 | Eks: 53576-5;74468-0;74465-6;11502-2;56446-8; |
| type_code_names                 | Eks: Personal health monitoring report Document;Questionnaire Form Definition Document;Questionnaire Response Document;LABORATORY REPORT.TOTAL;Appointment Summary Document; |
| type_code_schemes               | Eks: 2.16.840.1.113883.6.1;2.16.840.1.113883.6.1;2.16.840.1.113883.6.1;2.16.840.1.113883.6.1;2.16.840.1.113883.6.1; |
| **Vedr. upload**                | |
| CLASS_CODE_SCHEME                 | Ved upload af dokumenter, kan der angives en class code. Denne konfigurationsvariabel bestemmer scheme for denne code, f.eks. 1.2.208.184.100.9  |
|                                   | Scheme codes listet under "vedr. fremsøgning" anvendes også i forbindelse med upload  |
| Ekstra metadata:                  | Ved upload kan man angive en række ekstra metadata, som er tilgængelig vha. valg i en liste. Disse konfigureres som en liste af koder og navne i nedenstående variable. Hver element adskilles med ;  |
| AVAILABILITYSTATUS_CODES          | Eks: APPROVED;  |
| AVAILABILITYSTATUS_NAMES          | Eks: Approved; |
| FORMAT_CODE_CODES                 | Eks: urn:ad:dk:medcom:phmr:full;urn:ad:dk:medcom:qfdd:full;urn:ad:dk:medcom:qrd:full;urn:ad:dk:medcom:appointmentsummary:full;urn:ad:dk:medcom:labreports:svareksponeringsservice;  |
| FORMAT_CODE_NAMES                 | Eks: DK PHMR schema;DK QFDD schema;DK QRD schema;DK Appointment Summary Document schema;Laboratoriesvar (samling af svar);  |
| CLASS_CODE_CODES                  | Eks: 001;  |
| CLASS_CODE_NAMES                  | Eks: Clinical report;  |
| HEALTHCAREFACILITYTYPE_CODE_CODES | Eks: N/A;264372000;20078004;554221000005108;554031000005103;546821000005103;547011000005103;546811000005109;550621000005101;22232009;550631000005103;550641000005106;550651000005108;394761003;550661000005105;42665001;554211000005102;550711000005101;550671000005100;554061000005105;264361005;554041000005106;554021000005101;550681000005102;550691000005104;550701000005104;554231000005106;554051000005108;  |
| HEALTHCAREFACILITYTYPE_CODE_NAMES | Eks: N/A;apotek;behandlingscenter for stofmisbrugere;bosted;diætistklinik;ergoterapiklinik;fysioterapiklinik;genoptræningscenter (snomed: rehabiliteringscenter);hjemmesygepleje;hospital;jordemoderklinik;kiropraktor klinik;lægelaboratorium;lægepraksis (snomed:  almen lægepraksis);lægevagt;plejehjem;præhospitals enhed (snomed:  præhospitalsenhed);psykologisk rådgivningsklinik;speciallægepraksis;statsautoriseret fodterapeut (snomed:  fodterapeutklinik);sundhedscenter;sundhedsforvaltning;sundhedspleje;tandlægepraksis;tandpleje klinik;tandteknisk klinik;vaccinationsklinik;zoneterapiklinik;  |
| PRACTICESETTING_CODE_CODES        | Eks: N/A;408443003;394577000;394821009;394588006;394582007;394914008;394583002;394811001;394585009;408472002;394803006;394807007;419192003;394579002;408463005;394609007;551411000005104;394596001;394600006;394601005;394580004;421661004;408454008;394809005;394592004;418112009;394805004;394584008;394589003;394610002;394591006;394812008;394594003;394801008;394604002;394915009;394611003;394587001;394537008;394810000;554011000005107;394581000;394605001;394603008;408448007;394612005;  |
| PRACTICESETTING_CODE_NAMES        | Eks: N/A;almen medicin;anæstesiologi;arbejdsmedicin;børne- og ungdomspsykiatri;dermato-venerologi;diagnostisk radiologi;endokrinologi;geriatri;gynækologi og obstetrik;hepatologi;hæmatologi;infektionsmedicin;intern medicin;kardiologi;karkirurgi;kirurgi;kirurgisk gastroenterologi;klinisk biokemi;klinisk farmakologi;klinisk fysiologi og nuklearmedicin (snomed:  klinisk fysiologi);klinisk genetik;klinisk immunologi;klinisk mikrobiologi;klinisk neurofysiologi;klinisk onkologi;lungesygdomme;medicinsk allergologi;medicinsk gastroenterologi;nefrologi;neurokirurgi;neurologi;odontologi (snomed: odontologiske specialer);oftalmologi;ortopædisk kirurgi;oto-rhino-laryngologi;patologisk anatomi og cytologi;plastikkirurgi;psykiatri;pædiatri;reumatologi;Retsmedicin;samfundsmedicin;tand-, mund- og kæbekirurgi;thoraxkirurgi;tropemedicin;urologi;  |
| OBJECT_TYPE_CODES                 | Eks: STABLE;  |
| OBJECT_TYPE_NAMES                 | Eks: Stable;  |
| map_type_class_type_codes   | Dette er mapning mellem class codes og type codes. Type code indgangen. Eksempel: 53576-5;74468-0;74465-6;11502-2;56446-8; Dvs. Når type code er 53576-5, sættes class code til 001   |
| map_type_class_class_codes   |   Dette er mapning mellem class codes og type codes. class code indgangen. Eksempel: 001;001;001;001;001; Dvs. Når type code er 53576-5, sættes class code til 001  |
| default_class_code=001   | Dette er den class code, der anvendes ved upload, når der ikke kan findes en mapning fra et type code  |
| Backends:                                                | Ved søgning kan man vælge hvilken backend man ønsker at foretage søgningen imod. Disse konfigueres i nedenstående variable, et sæt per backend, hvor løbenummer adskiller dem. Det er muligt at angive et konkret sæt af koder til de forskellige lister i søge skærmen. Angiver man ikke nogen vil de default værdierne bliver anvendt. |
| xds_iti18_registry_reg_0_backendcode                     | Eks: DDS                                                                                                                     |
| xds_iti18_registry_reg_0_name                            | Eks: DDS - Dokumentdelingsservice                                                                                            |
| xds_iti18_registry_reg_0_endpoint                        | Den URL som ITI-18 snitfladen kan nås på.                                                                                    |
| xds_iti18_registry_reg_0_dgwsenabled                     | En boolean (true eller false) som fortæller, om ITI_18_ENDPOINT kører den gode webservice eller ej                           |
| xds_iti18_registry_reg_0_endpointiti43                   | Den URL som ITI-43 snitfladen kan nås på                                                                                     |
| xds_iti18_registry_reg_0_dgwsenablediti43                | En boolean (true eller false) som fortæller, om ITI_43_ENDPOINT kører den gode webservice eller ej                           |
| xds_iti18_registry_reg_0_defaulttypestable               | En boolean (true or false) som angiver om dokumentttypen stable skal være krydset af som søgekriterie når backend vælges     |
| xds_iti18_registry_reg_0_defaulttypeondemand             | En boolean (true or false) som angiver om dokumentttypen on-demand skal være krydset af som søgekriterie når backend vælges  |
| Koder                                                    | Disse angives hvis man ønsker at ændre default listerne specificeret ovenfor. De skal altid angives i sæt, dvs en codes og en names (typecode har også schemes). Indsætter man en tom, bliver listen tom |
| xds_iti18_registry_reg_1_eventcodeschemenames            |  |
| xds_iti18_registry_reg_1_eventcodeschemecodes            |  |
| xds_iti18_registry_reg_1_formatcodecodes                 |  |
| xds_iti18_registry_reg_1_formatcodenames                 |  |
| xds_iti18_registry_reg_1_healthcarefacilitytypecodecodes |  |
| xds_iti18_registry_reg_1_healthcarefacilitytypecodenames |  |
| xds_iti18_registry_reg_1_practicesettingcodecodes        |  |
| xds_iti18_registry_reg_1_practicesettingcodenames        |  |
| xds_iti18_registry_reg_1_typecodecodes                   |  |
| xds_iti18_registry_reg_1_typecodenames                   |  |
| xds_iti18_registry_reg_1_typecodeschemes                 |  |
| Repositories:                     | Ved upload kan man angive hvilket repository der skal uploades til. Disse konfigureres i nedenstående variable, et sæt per repository, hvor løbenummer adskiller dem. Man angiver navn, endpoint, om den gode webservice skal benyttes eller ej, unique id samt om man må opdatere et dokument herfra. Der er også mulighed for at angive hvilke dokument typer, som er gyldige for det enkelte repository. Det er også muligt at angive hvilken endpoint for et givet registry man skal deprecate imod for et givet repositoryid |
| xds_iti41_repos_repo_0_name       | Eks: NSP-DRS (dette er logiske navne, som brugeren ser når der skal vælges repository) |
| xds_iti41_repos_repo_0_endpoint   | Eks: https://test2-cnsp.ekstern-test.nspop.dk:8443/dros_plind/iti41 |
| xds_iti41_repos_repo_0_dgwsenabled       | Eks: true |
| xds_iti41_repos_repo_0_endpointiti57     | Eks: https://test2-cnsp.ekstern-test.nspop.dk:8443/dros_plind/iti57 |
| xds_iti41_repos_repo_0_dgwsenablediti57  | Eks: true |
| xds_iti41_repos_repo_0_backendcode | Eks: DDS. Angiv en backendcode, der findes i xds_iti18_registry tabellen ovenfor. Den anvendes til at finde korrekt endpoint i forbindelse med opslag ved deprecate og fremsøgning efter upload  |
| xds_iti41_repos_repo_0_repositoryuniqueid | Eks: 1.2.208.176.43210.8.10.11 |
| xds_iti41_repos_repo_0_homecommunityid | Det homecommunityid der anvendes ved upload (opret/ret). Eks: 1.2.208.176.43210.8.20 |
| xds_iti41_repos_repo_0_uploadallowed | Eks: true  (angiver at det er tilladt at oprette et dokument). Må kun sættes til true for dem, som kan nåes af iti41 endpointet |
| xds_iti41_repos_repo_0_updateallowed | Eks: true  (angiver at det er tilladt at opdatere et dokument). Må kun sættes til true for dem, som kan nåes af iti41 endpointet |
| xds_iti41_repos_repo_0_deprecateallowed | Eks: true  (angiver at det er tilladt at deprecate et dokument). Må kun sætte til true for dem, som kan nåes via iti57 endpointet |
| xds_iti41_repos_repo_0_validdoctypes_0 | Eks: 74465-6 |
| xds_iti41_repos_repo_0_validdoctypes_1 | Eks: 74465-4 |
| SPRING_SERVLET_MULTIPART_MAX    | Angiver hvor stor en fil der kan uploades. Kan sættes til fast værdi f.eks. 50MB eller ubegrænset med værdien -1 |
| **Vedr. default Moces**                      |
| MOCES_HSUID_USER_TYPE           | Denne og de følgende parametre bruges ved udfyldelse af HSUID-headeren. Denne er brugertypen, f.eks. "nsi:HealthcareProfessional" |
| MOCES_HSUID_SYSTEM_OWNER        | Systemejer-navnet (i HSUID header) |
| MOCES_HSUID_SYSTEM_NAME         | System-navnet (i HSUID header) |
| MOCES_HSUID_SYSTEM_VERSION      | System-version (i HSUID header) |
| MOCES_HSUID_ORG_NAME            | Organisationsnavn (i HSUID header) |
| MOCES_HSUID_ISSUER              | Navnet på issuer (i HSUID header) |
| moces_active_users | En liste af hvilke brugere der skal være til rådighed. Eks: 1;2;3;4; |


# Byg af CDA Document viewer 
CDa document vieweren bygges med maven (mvn clean install). For at bygge er det nødvendigt at have Docker installeret og kørende.

# Kørsel af CDA Document viewer
Man kan starte vieweren op med docker-compose (docker-compose -f compose/development/docker-compose.yml up). Det er nødvendigt at have Docker compose installeret. Vieweren kan herefter nås på http://localhost:8083/

# Release notes
Indført fra version 1.1.0 og dækker brugerrettet funktionalitet

| Release | Feature/fix                                                           |
|---------|-----------------------------------------------------------------------|
| 1.1.0   | - Ret og depcrete af dokument af dokumenter. Tillades per repository, der er konfigureret. Dokumentet skal have status "approved".  |
|         | - Ensretning af beskeder og fejlbeskeder. Fejlbeskeder har rød baggrund. Almindelig beskeder grønne |
|         | - For søgning på dokumenter vises fejlbeskeder både før og efter indtastningsfelterne. Området med indtastning fylder hele skærmen, så brugeren kan overse en fejlsked ellers  |
|         | - For upload, hvis formatCode kan trækkes ud af dokumentets metadata, kan brugeren ikke selv vælge det (Stammer oprindelig fra documentationOf, dokumentets version)  |
|         | - For upload, classCode kan ikke længere vælges, men mappes ud fra typeCode værdien |
|         | - Den anvendte builder/parser (4s-cda-builder) er blevet opgraderet til version 2.1.0 |
|         | - About siden er blevet udvidet med de konfigurede iti57 og iti41 endpoints |
|         |  |
| 1.1.1   | - For opret, ret og depcrete af dokument kan man nu efterfølgende downloade request og response i en tekst fil  |
| 1.1.2   | - Det er ikke længere muligt at anvende FOCES certifikat mod DDS'en. Det er derfor fjernet som mulighed ved dokument søgning  |
| 1.2.1   | - Det er nu muligt at angive i en miljø variabel, hvor længe et ID kort trukket fra STS anvendes (sts.idcard.expiry.hours). Id kortet eget udløbstidspunkt tjekkes fortsat først  |
|         | - Unique id for et dokument er nu på formen: root^extension (ex: 1.2.208.184^90c1c008-ec8e-43af-9a02-13e7bb2aee8b). Før var det extension^root.  |
| 1.3.1   | - Ved søgning på eventcodes, skal nu angives hvilken scheme, der skal anvendes i søgningen. Man vælger fra en dropdown liste |
| 1.3.2   | - Typecode dropdown ved søgning indeholdt forkerte værdier istedet for type codes |
| 1.3.3   | - Extension is no longer required in document Id |
| 1.3.4   | - It is now possible to download the document from the repository "as is" (without any transformation) |
| 1.4.1   | - When creating ITI-41 registration information for authorInstitution, it now uses getOrganizationIdentity from CDAMetadata, which matches was is present in the representedOrganization XML tag. (It will only create a authorInstition if representedOrganization is present and including an id tag.) |
| 1.4.2   | - Ændringer til lokale properties (classcode, formatcode og iti57 endpoint |
|         | - Status Approved er nu default som Availability Status ved søgning  |
|         | - homeCommunityId tilføjet ved upload, ret og deprecate. Faktisk homeCommunityId anvendt kan ses i forbindelse med preview af upload (Metadata) |
| 1.5.0   | - Ved upload der gik godt, vises eventuelle advarsler nu under kvittering for upload. Før blev de vist som uploaden gik i fejl |
|         | - Det er nu muligt at deprecate dokumenter uafhængigt af, om man kan uploade til det repository der er tale om |
| 1.6.0   | - Opdatering til builder parser version 6.3.4 |
|         | - Mulighed for valg af værdispring ved søgning (default i gammel version var at den altid søgte med værdispring) |
|         | - Mulighed for upload af eget MOCES certifikat fjernet |
|         | - Ændring af logik til kald af backend servicene. Hvilket kommer til udtryk i hvordan "download af seneste request" layout ser ud |
|         | - Mulighed for at vælge forskellige bruger profiler ved søgning. Pt konfigueres Læge Charles Babbage |
| 1.7.0   | - Der søges nu altid med findDocuments unanset om uniqueId er indtastet eller ej. Dette har konsekvens for søgetid og response xml og fejlbeskeder. Listen med dokumenter filtreres efterfølgende på uniqueId hvis udfyldt. |
|         | - Listen med fejl i forbindelse med søgning findes 2 gange. Den nederste er flyttet fra før søge resultat til efter og den øverste justeret ved udfyldelse af uniqueId |
| 1.7.1   | - For on-demand dokumenter vises der nu "< not applicable >" for hash og size i metadata listen |
|         | - I metadata listen vises label for EventCode og ReferenceidList nu, selvom der ikke er data  |
|         | - Det er nu muligt at angive et ITI-57 endpoint per repositoryuniqueid. Hvis flere med samme id kan kun et være aktivt med deprecateallowed |
| 1.7.2   | - Når man vælger "vis" linket i søgeresultatet åbnes dokumentet i en ny fane |
| 1.7.3   | - Fejlrettelse: "vis" link viste altid det øverste dokument fra søge resultat |
| 1.7.4   | - PatientId er sat på metadata feltlisten ved visning af dokumentet. Name er fjernet som felt for SourcePatientId, da det ikke findes som metadata |
|         | - I metadata listen vises label for authorInstitution nu, selvom der ikke er data |
| 1.7.5   | - I metadata listen vises prefix ikke længere for AuthorPerson |
|         | - I metadata listen vises SourcePatientInfo birthTime nu uden tidspunkt |
| 1.8.0   | - Det er nu muligt at foretage søgning i flere backends. Man vælger hvilken backend man vil søge i vha. dropdown "Søg i" |
| 1.8.1   | - Da typecodes kan have forskellige schemes, er det nu muligt at konfigurere disse for hver kode. Værdien bliver vist i søgekriterierne, når man vælger en typecode |

